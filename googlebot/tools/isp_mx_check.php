<?php

require('../config.php');

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('ignore_repeated_errors', false);
ini_set('error_reporting', E_ALL);

require_once '/opt/data/www/vendor/autoload.php';
use GeoIp2\Database\Reader;

$reader = new Reader(LIBS.'geoip2/GeoIP2-ISP.mmdb');

include(LIBS.'functions.php');
include(LIBS.'database.php');

$db = new database();
$db->connect();

//echo gip('188.125.73.108');die;
/*try {
$record = $reader->isp(trim('188.125.73.108'));
} catch(Exception $e){
    echo ($e);
}
die;*/

$s = '349766';

$i = 0;
$handle = fopen(ROOT.'tools/relay_domain.txt', "rb");
while(!feof($handle)) {
	$str = fgets($handle);
	$str=trim($str);
	if(empty($str)) continue;
	
	if ($s > $i) { $i ++; continue; }
	
	if (!preg_match('/\./isu',$str)) { continue; }
	if (preg_match('/\"|\=|\!|\)|\(|\^|\@|\#|\%|\&|\?|\*|\+|\_|\~/isu',$str)) { continue; }
	
	$_ip = gethostbyname(trim($str));
	
	$geo = (!empty($_ip) ? gip($_ip) : '');
	
	if (empty($geo) || trim($geo) == "") { continue; }
	
	if (preg_match('/192\.168\.|127\.0\.0\.|10\.0\.0\.|1\.0\.0\./isu',$_ip)) { continue; }
	
	if (!empty($_ip) && $_ip != trim($str)) {
		
		try {
			$record = $reader->isp(trim($_ip));
		} catch(Exception $e){
    		echo $e."<br>\n";
		}
		
		if (preg_match('/JustHost|BlueHost|Unified Layer/isu',$record->isp)) {
			
			write_file_a(ROOT."tools/relay_domain_results.txt",$str.'|'.$record->isp."\n");
			@chmod(ROOT."tools/relay_domain_results.txt", 0777);
		}
		else {
			getmxrr(trim($str),$mx_domain);
			
			if (!empty($mx_domain) && preg_match('/JustHost|BlueHost|Unified Layer/isu',implode(" ",$mx_domain))) {
				
				write_file_a(ROOT."tools/relay_domain_results.txt",$str.'|'.implode(", ",$mx_domain)."\n");
				@chmod(ROOT."tools/relay_domain_results.txt", 0777);
			}
			unset($mx_domain);
		}
	}
	else {
		getmxrr(trim($str),$mx_domain);
		
		if (!empty($mx_domain) && preg_match('/JustHost|BlueHost|Unified Layer/isu',implode(" ",$mx_domain))) {
			
			write_file_a(ROOT."tools/relay_domain_results.txt",$str.'|'.implode(", ",$mx_domain)."\n");
			@chmod(ROOT."tools/relay_domain_results.txt", 0777);
		}
		unset($mx_domain);
	}
	
	write_file_w(ROOT."tools/relay_domain_count.txt",$i.'|'.$str."\n");
	@chmod(ROOT."tools/relay_domain_count.txt", 0777);
	//echo trim($str).", ";
	//break;
	
	$i ++;
}
fclose($handle);
unset($handle);

$db->close();

?>