<?php
require(rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/config.php');

//error_reporting(1);
//ini_set('display_errors', true);

include(LIBS.'functions.php');
include(LIBS.'database.php');

$db = new database();
$db->connect();

if (file_exists(LOGS."bots_search.proc")) { if (filemtime(LOGS."bots_search.proc") < time() - 60*60*3) { unlink(LOGS."bots_search.proc"); } }
if (file_exists(LOGS."bots_search.proc")) { exit("Bots Search Process Started ..."); }

write_file_w(LOGS."bots_search.proc",time());

/*
die;
*/

// Сколько прошлых дней выбрать
define('PREVIUS_DATE', '10');

// Юзаем все даты
$datesearch = '1';

// Расшипляем дату старта и конечную
$date=date("j_n_Y");
$explp = explode("_", $date);
$prevdate=mktime(0,0,0,$explp[1],$explp[0],$explp[2]);
$prevdate=strtotime('-'.PREVIUS_DATE.' day', $prevdate);

// Выводим список дат по введенному периоду
$date_array = array();
$date_array = getInterval(date("j_n_Y",$prevdate), $date, 'j_n_Y');

$bfile = array();
$table_n_array = array();

if ($datesearch == '1') {
foreach ($date_array as $_table_name_array_) {
if (file_exists(LOGS.'bots_cashe/'.$_table_name_array_)) {
$table_n_array[] = $_table_name_array_;
$bhandle = fopen(LOGS.'bots_cashe/'.$_table_name_array_, "rb");
while(!feof($bhandle)) {
  $bstr = fgets($bhandle);
  $bstr=trim($bstr);
  if(empty($bstr)) continue;
  $str_exp = explode('|',$bstr);
  $bfile["".trim($str_exp[0]).""] = '1';
  unset($str_exp);
}
fclose($bhandle);
unset($bhandle);
}
}
}
else {
if (file_exists(LOGS.'bots_cashe/traffic_'.date("j_n_Y"))) {
$table_n_array[] = 'traffic_'.date("j_n_Y");
$bhandle = fopen(LOGS.'bots_cashe/traffic_'.date("j_n_Y"), "rb");
while(!feof($bhandle)) {
  $bstr = fgets($bhandle);
  $bstr=trim($bstr);
  if(empty($bstr)) continue;
  $str_exp = explode('|',$bstr);
  $bfile["".trim($str_exp[0]).""] = '1';
  unset($str_exp);
}
fclose($bhandle);
unset($bhandle);
}
}

//d($table_n_array);

//d($bfile);

$afile = array();
if ($datesearch == '1') {
foreach ($date_array as $_table_name_array_) {
if (file_exists(LOGS.'accept_cashe/'.$_table_name_array_)) {
$ahandle = fopen(LOGS.'accept_cashe/'.$_table_name_array_, "rb");
while(!feof($ahandle)) {
  $astr = fgets($ahandle);
  $astr=trim($astr);
  if(empty($astr)) continue;
  $str_exp = explode('|',$astr);
  if (preg_match('/192\.168\.|127\.0\.0\.|10\.0\.0\.|10\.1\.10\./isu',trim($str_exp[0]))) { continue; }
  if (!empty($bfile["".trim($str_exp[0]).""]) && !in_array(trim($str_exp[1]),$bot_agent)) {
  	$afile[] = $astr;
  }
  unset($str_exp);
}
fclose($ahandle);
unset($ahandle);
unlink(LOGS.'accept_cashe/'.$_table_name_array_);
}
}
}
else {
if (file_exists(LOGS.'accept_cashe/traffic_'.date("j_n_Y"))) {
$ahandle = fopen(LOGS.'accept_cashe/traffic_'.date("j_n_Y"), "rb");
while(!feof($ahandle)) {
  $astr = fgets($ahandle);
  $astr=trim($astr);
  if(empty($astr)) continue;
  $str_exp = explode('|',$astr);
  if (preg_match('/192\.168\.|127\.0\.0\.|10\.0\.0\.|10\.1\.10\./isu',trim($str_exp[0]))) { continue; }
  	if (!empty($bfile["".trim($str_exp[0]).""]) && !in_array(trim($str_exp[1]),$bot_agent)) {
 	 	$afile[] = $astr;
 	 }
  }
  unset($str_exp);
}
fclose($ahandle);
unset($ahandle);
unlink(LOGS.'accept_cashe/traffic_'.date("j_n_Y"));
}

if (!file_exists(LOGS.'unknow_traffic/traffic_'.date("j_n_Y"))) {

write_file_a(LOGS.'unknow_traffic/traffic_'.date("j_n_Y"),'');

@chmod(LOGS.'unknow_traffic/traffic_'.date("j_n_Y"), 0777);
}
write_file_a(LOGS.'unknow_traffic/traffic_'.date("j_n_Y"),implode("\n",$afile));

//d($afile);

// Удаление старых файлов истории линк чекера
$check_proc_array = glob(LOGS."check_cashe/check_*");
foreach ($check_proc_array as $filename) {
    if (file_exists($filename)) {
    if (filemtime($filename) < (time() - 60*60*24*3)) {
    if (file_exists($filename)) { unlink($filename); }
    }
    }
}
unset($check_proc_array);

// Удаление старых файлов истории непринятого трафика
$break_proc_array = glob(LOGS."break_cashe/traffic_*");
foreach ($break_proc_array as $filename) {
    if (file_exists($filename)) {
    if (filemtime($filename) < (time() - 60*60*24*30)) {
    if (file_exists($filename)) { unlink($filename); }
    }
    }
}
unset($break_proc_array);

// Удаление старых файлов истории бот трафика
$bots_proc_array = glob(LOGS."bots_cashe/traffic_*");
foreach ($bots_proc_array as $filename) {
    if (file_exists($filename)) {
    if (filemtime($filename) < (time() - 60*60*24*30)) {
    if (file_exists($filename)) { unlink($filename); }
    }
    }
}
unset($bots_proc_array);

// Удаление старых файлов истории подозрительного трафика
$unknow_proc_array = glob(LOGS."unknow_traffic/traffic_*");
foreach ($unknow_proc_array as $filename) {
    if (file_exists($filename)) {
    if (filemtime($filename) < (time() - 60*60*24*30)) {
    if (file_exists($filename)) { unlink($filename); }
    }
    }
}
unset($unknow_proc_array);

if (file_exists(LOGS."bots_search.proc")) { unlink(LOGS."bots_search.proc"); }

echo "All Done!";

unset($afile);
unset($bfile);
unset($bots_uniq);
unset($date_array);
unset($table_n_array);

$db->close();
?>