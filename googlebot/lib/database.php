<?php
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
class database
{
	var $host,
		$user,
		$pass,
		$DB_NAME,
		$db_link=false,
		$DB_PREFIX=false,
		$link=array(),
		$debug=true,
		$use_cache=true,
		$charset_name,
		$charset,
		$collate,
		$DB_CHECK_PING,
		$DB_TIMEOUT;
	function connect($host=false,$user=false,$pass=false,$db=false,$prefix=false,$debug=-1,$charset_name=false,$charset=false,$collate=false)
	{
		$this->host=$host=$host!==false?$host:DB_HOST;
		$this->user=$user=$user!==false?$user:DB_USER;
		$this->pass=$pass=$pass!==false?$pass:DB_PASS;
		$this->DB_NAME=$db=$db!==false?$db:DB_NAME;
		$this->DB_PREFIX=$prefix=$prefix!==false?$prefix:DB_PREFIX;
		$this->debug=$debug=$debug!==-1?$debug:DB_DEBUG;
		$this->charset_name=$charset_name=$charset_name!==false?$charset_name:DB_CHARSET_NAME;
		$this->charset=$charset=$charset!==false?$charset:DB_CHARSET;
		$this->collate=$collate=$collate!==false?$collate:DB_COLLATE;
		$this->DB_TIMEOUT=(int)_constant('DB_TIMEOUT', 0);
		$this->DB_TIMEOUT=$this->DB_TIMEOUT?$this->DB_TIMEOUT:300;
		$this->DB_CHECK_PING=_constant('DB_CHECK_PING');
		ini_set('mysql.trace_mode', false);
		ini_set('mysql.connect_timeout', $this->DB_TIMEOUT);
		//if($this->db_link=mysql_pconnect($host,$user,$pass,MYSQL_CLIENT_INTERACTIVE))
		if($this->db_link=mysql_connect($host,$user,$pass,true,MYSQL_CLIENT_INTERACTIVE))
		{
			mysql_query('SET SESSION interactive_timeout='.$this->DB_TIMEOUT,$this->db_link);
			if(!($this->link['default']=mysql_query('USE `'.$db. '`', $this->db_link)))
			{
				trigger_error ('Could not use database: '. mysql_error(), E_USER_ERROR);
			}
			mysql_query("/*!40101 SET NAMES '{$charset}' */",$this->db_link);
		}
		else
		{
			trigger_error ('Could not connect to database: '. mysql_error(), E_USER_ERROR);
		}
	}
	function query($query,$id='default',$ping_true=true)
	{
		$query = str_replace('#__', $this->DB_PREFIX, $query);
		if($this->DB_CHECK_PING && $ping_true)
		{
                 // if(!mysql_ping($this->db_link))
			if(!mysql_query('SELECT 1',$this->db_link))
			{
				mysql_close($this->db_link);
				$this->connect($this->host,$this->user,$this->pass,$this->DB_NAME,$this->DB_PREFIX,$this->debug,$this->charset_name,$this->charset,$this->collate);
			}
		}                                                      
		if(!($this->link[$id]=mysql_query($query,$this->db_link)))
		{
			$this->show_error($query);
		}
		return $this->link[$id];
	}
	function insert($query,$id='default')
	{
		$this->query($query,$id);
		return (int)(mysql_insert_id($this->db_link));
	}
	function count($id='default')
	{
		return (int)(mysql_num_rows($this->link[$id]));
	}
	function count_rows($id='default')
	{
		$count = $this->select_row('SELECT FOUND_ROWS() as count_rows', $id, false);
		return (int)$count['count_rows'];
	}
	function fetch($id='default')
	{
		return mysql_fetch_assoc($this->link[$id]);
	}
	function select($query,$id='default',$column=false)
	{
		$this->query($query,$id);
		return $column?$this->get_super_array($column,$id):$this->get_array($id);
	}
	function select_row($query,$id='default',$ping_true=true)
	{
		$this->query($query,$id,$ping_true);
		return $this->count($id)?mysql_fetch_assoc($this->link[$id]):array();
	}
	function affected($id='default')
	{
		return (int)(mysql_affected_rows($this->db_link));
	}
	function get_super_array($column,$id='default')
	{
		
		$return=array();
		while($array=$this->fetch($id))
		{
			$return[$array[$column]]=$array;
		}
		return $return;
	}
	function get_array($id='default')
	{
		$return=array();
		while($array=$this->fetch($id))
		{
			$return[]=$array;
		}
		return $return;
	}
	function data_seek($index,$id='default')
	{
		return mysql_data_seek($this->link[$id],$index);
	}
	function show_error($query)
	{
		if($this->debug)
		{
			$query=preg_replace('/([0-9a-f]){32}/','********************************',$query);
			trigger_error (mysql_error().". Error # ". mysql_errno(). ". QUERY: {$query}", E_USER_ERROR);
		}
	}
	function close()
	{
		mysql_close($this->db_link);
	}
	function free($id='default')
	{
		mysql_free_result($this->link[$id]);
	}
	function sets_sql_list(&$arr,$params)
	{
		$params_new=array_flip($params);
		$params=array_keys($params_new);
		foreach($params as $v)
		{
			$params_new[$v]="`{$v}`='".mysql_real_escape_string((string)getvar($v,$arr))."'";
		}
		return implode(',',$params_new);
	}
	
	function escapes($value)
	{
		return mysql_real_escape_string($value, $this->db_link);
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
class FDateTime {
	
	private $timestamp;
	
	public function __construct($timestamp) {
		$this->timestamp = $timestamp;
	}
	
	public function __toString() {
		return $this->toSql();
	}
	
	public function toSql() {
		return date('Y-m-d H:i:s', $this->timestamp);
	}
	
	public static function parseSql($datetime) {
		if(preg_match('/([\d]+)[^\d]([\d]+)[^\d]([\d]+) ([\d]+)[^\d]([\d]+)[^\d]([\d]+)/', $datetime, $r)) {
			return new FDateTime(mktime($r[4], $r[5], $r[6], $r[2], $r[3], $r[1]));
		}
		else {
			return null;
		}
	}
	
	public function timestamp() {
		return $this->timestamp;
	}
	
	public static function now() {
		return new FDateTime(time());
	}
	
	public function diffTo(FDateTime $to) {
		return $to->timestamp() - $this->timestamp();
	}
	
	public function diffFrom(FDateTime $from) {
		return $this->timestamp() - $from->timestamp();
	}
	
	public function add($s, $m = 0, $h = 0, $d = 0, $M = 0, $y = 0) {
		$timestamp = mktime(
			date('H', $this->timestamp) + $h,
			date('i', $this->timestamp) + $m,
			date('s', $this->timestamp) + $s,
			date('m', $this->timestamp) + $M,
			date('d', $this->timestamp) + $d,
			date('Y', $this->timestamp) + $y);
		return new FDateTime($timestamp);
	}
	
}
?>