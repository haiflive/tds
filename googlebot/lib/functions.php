<?php

// Рандомизация
function make_seed() {
    list($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
}
srand(make_seed());

function getInterval($start, $end, $format='Y-m-d') {
   $start = str_replace('_','-',$start);
   $end = str_replace('_','-',$end);
   return array_map(create_function('$item', 'return "traffic_".date("'.$format.'", $item);'),range(strtotime($start), strtotime($end), 60*60*24));
}

// Вывод массива для приятного отображение при отладке
function html_quote($string, $charset=false) {
return str_replace(array('&', '<', '>', '"', '\''), array('&amp;', '&lt;', '&gt;', '&quot;', '&#039;'), $string);
}
function html_quote_array(&$array) {
if(is_array($array)) {
foreach($array as $k=>$v) {
$array[$k] = html_quote($v);
}
}
return $array;
}

function natrsort(&$array)
{
natsort($array);
$array = array_reverse($array);
}

function convert($size) {
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function strips(&$el) {
  if (is_array($el))
    foreach($el as $k=>$v)
      strips($el[$k]);
  else $el = stripslashes($el);
} 
if (get_magic_quotes_gpc()) {
  strips($_GET);
  strips($_POST);
  strips($_COOKIE);
  strips($_REQUEST);
  if (isset($_SERVER['PHP_AUTH_USER'])) strips($_SERVER['PHP_AUTH_USER']);
  if (isset($_SERVER['PHP_AUTH_PW'])) strips($_SERVER['PHP_AUTH_PW']);
}

// Определяет размера файла
function formatSizeUnits($bytes) {
if ($bytes >= 1073741824) {
$bytes = number_format($bytes / 1073741824, 2) . ' GB';
}
elseif ($bytes >= 1048576) {
$bytes = number_format($bytes / 1048576, 2) . ' MB';
}
elseif ($bytes >= 1024) {
$bytes = number_format($bytes / 1024, 2) . ' KB';
}
elseif ($bytes > 1) {
$bytes = $bytes . ' bytes';
}
elseif ($bytes == 1) {
$bytes = $bytes . ' byte';
}
else {
$bytes = '0 bytes';
}
return $bytes;
}

// Функция транслита
function text_translit($str) {
$translit = array(
            " "=>"_","А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
            "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
            "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
            "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
            );
return strtr($str,$translit);
}

// Работа с временем
function get_time() {
$t = microtime();
$t = explode(' ', $t);
$t = (float)$t[1] + (float)$t[0];
return $t;
}
define('BEGIN_TIME', get_time());
function get_timed() {
return str_replace('.', ',', (string)(round(get_time()-BEGIN_TIME,4)));
}

// Вывод массива в удобном формате
function d($var)
{
echo '<pre style="font-family:tahoma;font-size:13px;color:#000000;border:1px dashed #B1B1B1;padding:15px;">';
echo html_quote(print_r($var, true));
echo '<br/>';
echo '<b style="font-family:courier new;font-size:13px;color:#000000;">'. get_timed() .' seconds </b>';
echo '<br/>';
echo '</pre>';
}

function sort_it ($a, $b) {
$a = $a['value'];
$b = $b['value'];
if ($a == $b) {
return 0;
}
return ($b < $a ? 0 - 1 : 1);
}

// Генерируем случайные числа, буквы, символы
function generate_allbig($len) {
$res = '';
$useChars = 'abcdefghkmnpqrstuvwxyzABCDEFGHKMNPQRSTUVWXYZ1234567890-+_=!@#$^*().';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные числа, буквы, символы
function generate_all($len) {
$res = '';
$useChars = 'abcdefghkmnpqrstuvwxyz1234567890-+_=!@#$^*().';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные числа и буквы
function generatebig($len) {
$res = '';
$useChars = 'abcdefghkmnpqrstuvwxyzABCDEFGHKMNPQRSTUVWXYZ1234567890';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные числа и буквы
function generate($len) {
$res = '';
$useChars = 'abcdefghkmnpqrstuvwxyz1234567890';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные буквы
function generate_chrbig($len) {
$res = '';
$useChars = 'abcdefghkmnpqrstuvwxyzABCDEFGHKMNPQRSTUVWXYZ';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные буквы
function generate_big($len) {
$res = '';
$useChars = 'ABCDEFGHKMNPQRSTUVWXYZ';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные буквы
function generate_chr($len) {
$res = '';
$useChars = 'abcdefghkmnpqrstuvwxyz';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные числа
function generate_dig ($len) {
$res = '';
$useChars = '1234567890';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Генерируем случайные числа
function generate_sim ($len) {
$res = '';
$useChars = '-+_=!@#$^*().';
$useChars .= $useChars;
for ($i = 0; $i < $len; $i++ ) {
$res .= $useChars[mt_rand ( 0, strlen ( $useChars)-1)];
}
return $res;
}

// Данный к подключеню базам SQL
$config = array(
	'DB_HOST' => DB_HOST_SET,
	'DB_USER' => DB_USER_SET,
	'DB_PASS' => DB_PASS_SET,
	'DB_NAME' => DB_NAME_SET,
	'DB_PREFIX' => DB_PREFIX_SET,
	'DB_DEBUG' => DEV_MODE,
	'DB_CHARSET_NAME' => 'UTF-8',
	'DB_CHARSET' => 'utf8',
	'DB_COLLATE' => 'utf8_general_ci',
	'DB_TIMEOUT' => 30,
	'DB_CHECK_PING' => true
);

// Экранирование
function PHP_slashes($string,$type='add') {
if ($type == 'add') { if (get_magic_quotes_gpc()) { return $string; }
else { if (function_exists('addslashes')) { return addslashes($string); }
else { return mysql_real_escape_string($string); }
}
}
else if ($type == 'strip') { return stripslashes($string); }
else { die('error in PHP_slashes (mixed,add | strip)'); }
}

// Массив констант
function define_constants(&$array) {
foreach($array as $k=>$v) { @define($k, $v); }
}
function _constant($c, $d=false) {
return defined($c)? constant($c): $d; 
}
define_constants($config);

// Функция сбора защита от SQL ijection
function unis ($str){
    $str = str_replace("'","", $str);
    $str = str_replace('#',"", $str);
    $str = str_replace('"','', $str);
    $str = str_replace("/*","", $str);
    $str = str_replace("*/","", $str);
    if (get_magic_quotes_gpc()) { $str = stripslashes($str); }
    $str = mysql_real_escape_string($str);
    $str = trim($str);
    $str = htmlspecialchars($str);
    return $str;
}

// Функция сбора защита от SQL ijection
function unis_sql ($str){
    $str = str_replace("'","", $str);
    $str = str_replace('#',"", $str);
    $str = str_replace('"','', $str);
    $str = str_replace("/*","", $str);
    $str = str_replace("*/","", $str);
    if (get_magic_quotes_gpc()) { $str = stripslashes($str); }
    //$str = mysql_real_escape_string($str);
    $str = trim($str);
    //$str = htmlspecialchars($str);
    return $str;
}

// Функция сбора защита от SQL ijection
function unis_lite ($str){
    $str = trim($str);
    return $str;
}

// Дописиваем в файл
function write_file_a($path,$body) {
$file=fopen($path,"a+");
fputs($file,$body);
fclose($file);
}

// Пишем в файл с нуля
function write_file_w($path,$body) {
$file=fopen($path,"w+");
fputs($file,$body);
fclose($file);
}

// Дописиваем в файл c flock
function write_file_flock_a($path,$body) {
$file=fopen($path,"a+");
flock($file, LOCK_EX);
fputs($file,$body);
flock($file, LOCK_UN);
fclose($file);
}

// Пишем в файл с нуля c flock
function write_file_flock_w($path,$body) {
$file=fopen($path,"w+");
flock($file, LOCK_EX);
fputs($file,$body);
flock($file, LOCK_UN);
fclose($file);
}

// Дата по стандартам RFC +0300
function RFCDate() {
    $tz = date('Z');
    $tzs = ($tz < 0) ? '-' : '+';
    $tz = abs($tz);
    $tz = (int)($tz/3600)*100 + ($tz%3600)/60;
    $result = sprintf("%s %s%04d", date('D, j M Y H:i:s'), $tzs, $tz);
    return $result;
}

// Дата по стандартам RFC
function RFCBDate() {
    return date('D, j M Y H:i:s');
}

function gip($server){
require_once(LIBS.'geoip/geoip.inc');
$gi = geoip_open(LIBS.'geoip/GeoIP.dat',GEOIP_STANDARD);
$server = gethostbyname($server);
$country = geoip_country_code_by_addr($gi, $server);
geoip_close($gi);
return $country;
}

function gip_f($server){
require_once(LIBS.'geoip/geoip.inc');
$gi = geoip_open(LIBS.'geoip/GeoIP.dat',GEOIP_STANDARD);
$server = gethostbyname($server);
$country = geoip_country_name_by_addr($gi, $server);
geoip_close($gi);
return $country;
}

function gip_name($name){
global $db;
$sqlc = mysql_query("SELECT `name` FROM `".DB_PREFIX_SET."countries` WHERE `zone2`='".strtoupper($name)."' LIMIT 1");
$name = mysql_fetch_array($sqlc, MYSQL_NUM);
return trim($name[0]);
}

function grizli($start,$end,$source,$chars="0") {
if(DEV_MODE){ write_file_a(LOGS."DEBUG.txt",'GRIZLI(in) $start='.$start.' $end='.$end.' $source='.$source.' $chars='.$chars."\n"); }
if(empty($chars)) { $chars = strlen($start); }
if(empty($source)) { $source = ''; }
$startgr = strpos($source, $start);
$out1 = substr($source, $startgr + $chars);
$endgr = strpos($out1, $end);
$out = substr($source, $startgr + $chars, $endgr);
if(DEV_MODE){ write_file_a(LOGS."DEBUG.txt",'GRIZLI(out) $out='.$out."\n"); }
return $out;
}

function write_string($what,$where){
if(DEV_MODE){ write_file_a(LOGS."DEBUG.txt",'write_string(in) $what='.$what.' $where='.$where."\n"); }
$write_file = fopen($where,"a+");
fputs($write_file, "\n".$what);
fclose($write_file);
}

function cutStr($str,$scount,$cutParam){
if(strlen($str) > $scount){
$str = substr($str,0,$scount).$cutParam;
}
return $str;
}

function count_f($filedir){
if(DEV_MODE){ write_file_a(LOGS."DEBUG.txt",'count_f(in) $filedir='.$filedir."\n"); }
$phdbdir = opendir ($filedir);
$pfilecount = 0;
while ($file = readdir($phdbdir))
{if (($file != ".") && ($file != "..")&& ($file != "Thumbs.db")) {
$pfilecount = $pfilecount + 1;
}
}
closedir($phdbdir);
if(DEV_MODE){ write_file_a(LOGS."DEBUG.txt",'count_f(out) $pfilecount='.$pfilecount."\n"); }
return $pfilecount;
}

function show_f($filep,$f_number){
if(DEV_MODE){ write_file_a(LOGS."DEBUG.txt",'show_f(in) $filep='.$filep.' $f_number='.$f_number."\n"); }
$phdbdir = opendir($filep);
$chekpfilecount = 0;
while ($file = readdir ($phdbdir))
{if (($file != ".") && ($file != "..") && ($file != "Thumbs.db")) {
$chekpfilecount ++;
if ($f_number == $chekpfilecount){
$photofile = $file;
}}}
closedir($phdbdir);
$postfile=$filep.$photofile;
if(DEV_MODE){ write_file_a(LOGS."DEBUG.txt",'show_f(out) $postfile='.$postfile."\n"); }
return $postfile;
}

function dest_show($did) {
global $db;
$sqlc = mysql_query("SELECT `ndest` FROM `".DB_PREFIX_SET."dest_now` WHERE `ndid` = '".$did."' LIMIT 0,1");
$check = mysql_fetch_array($sqlc, MYSQL_NUM);
return $check[0];
}

function dest_switch_this($did,$content) {
global $db;
if($content){
mysql_query("DELETE FROM `".DB_PREFIX_SET."dest_now` WHERE `ndid` = '".$did."'");
mysql_query("INSERT INTO `".DB_PREFIX_SET."dest_now` (`ndid`, `ndest`) VALUES ('".$did."', '".$content."')");
}
return $content;
}

function dest_switch_url($did,$url){
global $db;
$content=@file_get_contents($url);
if($content){
mysql_query("DELETE FROM `".DB_PREFIX_SET."dest_now` WHERE `ndid` = '".$did."'");
mysql_query("INSERT INTO `".DB_PREFIX_SET."dest_now` (`ndid`, `ndest`) VALUES ('".$did."', '".$content."')");
}
return $content;
}

function dest_switch_db($did){
global $db;
$sqlc = mysql_query("SELECT `dest` FROM `".DB_PREFIX_SET."dests` WHERE `did` = '".$did."' LIMIT 0,1");
$dest = mysql_fetch_array($sqlc, MYSQL_NUM);
if($dest[0]){
mysql_query("DELETE FROM `".DB_PREFIX_SET."dests` WHERE `did` = '".$did."' AND `dest` = '".$dest[0]."' LIMIT 0,1");
mysql_query("DELETE FROM `".DB_PREFIX_SET."dest_now` WHERE `ndid` = '".$did."'");
mysql_query("INSERT INTO `".DB_PREFIX_SET."dest_now` (`ndid`, `ndest`) VALUES ('".$did."', '".$dest[0]."')");
}
return $dest[0];
}

function dest_get_rand_db($did){
global $db;
$sqlc = mysql_query("SELECT `dest` FROM `".DB_PREFIX_SET."dests` WHERE `did` = '".$did."' ORDER BY RAND() LIMIT 1");
$dest = mysql_fetch_array($sqlc, MYSQL_NUM);
if($dest[0]){
mysql_query("DELETE FROM `dest_now` WHERE `ndid` = '".$did."'");
mysql_query("INSERT INTO `".DB_PREFIX_SET."dest_now` (`ndid`, `ndest`) VALUES ('".$did."', '".$dest[0]."')");
}
return $dest[0];
}

function dest_get_rand($did){
global $db;
$sqlc = mysql_query("SELECT `dest` FROM `".DB_PREFIX_SET."dests` WHERE `did` = '".$did."' ORDER BY RAND() LIMIT 1");
$dest = mysql_fetch_array($sqlc, MYSQL_NUM);
return $dest[0];
}

function check_unique_stat_($table='XX',$sql_tablename='') {
global $db,$table_list;
$datashow = array();
if (!empty($sql_tablename)) { $sqltablename=$sql_tablename; } else  { $sqltablename=DB_PREFIX_SET."traffic_".date("j_n_Y"); }
$sql = mysql_query("select distinct `ip`,geo from `".$sqltablename."` WHERE `timestamp` > '0' AND `geo` = '".$table."' ORDER by geo");
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[1];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = $rowArray[0];
}
return $datashow;
}

function check_hits_stat_($table='XX',$sql_tablename='') {
global $db,$table_list;
$datashow = array();
if (!empty($sql_tablename)) { $sqltablename=$sql_tablename; } else  { $sqltablename=DB_PREFIX_SET."traffic_".date("j_n_Y"); }
$sql = mysql_query("select geo from `".$sqltablename."` WHERE `timestamp` > '0' AND `geo` = '".$table."' ORDER by geo");
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[0];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
	   	   
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = $rowArray[0];
}
return $datashow;
}

function _check_hits_stat_($table_section='',$table_name_array=array()) {
$hitsstat_ = array();
$_hits_stat_ = array();
$hits_stat_ = array();
foreach ($table_name_array as $sql_tablename) {
if (file_exists(LOGS.'cashe/'.$sql_tablename.'_hits_check')) {
$_hits_stat_file = file(LOGS.'cashe/'.$sql_tablename.'_hits_check');
$_hits_stat_[] = unserialize(trim($_hits_stat_file[0])); unset($_hits_stat_file);
}
else {
$_hits_stat_[] = check_hits_stat_($table_section,$sql_tablename);
}
}
foreach ($_hits_stat_ as $stater) {
foreach ($stater as $kays => $_stater) {
$hits_stat_[$kays][] = $_stater;
}
}
foreach ($hits_stat_ as $_kays => $stater_) {
$hitsstat_[''.$_kays.''] = array(str_replace('_',' ',$_kays),array_sum($stater_));
}
uasort($hitsstat_, "cmp");
//natrsort($hitsstat_);
unset($hits_stat_);
unset($_hits_stat_);
return $hitsstat_;
}

function _check_unique_stat_($table_section='',$table_name_array=array()) {
$hitsstat_ = array();
$_hits_stat_ = array();
$hits_stat_ = array();
foreach ($table_name_array as $sql_tablename) {
if (file_exists(LOGS.'cashe/'.$sql_tablename.'_unique_check')) {
$_hits_stat_file = file(LOGS.'cashe/'.$sql_tablename.'_unique_check');
$_hits_stat_[] = unserialize(trim($_hits_stat_file[0])); unset($_hits_stat_file);
}
else {
$_hits_stat_[] = check_unique_stat_($table_section,$sql_tablename);
}
}
foreach ($_hits_stat_ as $stater) {
foreach ($stater as $kays => $_stater) {
$hits_stat_[$kays][] = $_stater;
}
}
foreach ($hits_stat_ as $_kays => $stater_) {
$hitsstat_[''.$_kays.''] = array(str_replace('_',' ',$_kays),array_sum($stater_));
}
uasort($hitsstat_, "cmp");
//natrsort($hitsstat_);
unset($hits_stat_);
unset($_hits_stat_);
return $hitsstat_;
}

function check_unique_stat($table_name_array,$table='XX',$datesearch=''){
global $db,$table_list;
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select distinct `ip`,geo from `".$sql_tablename."` WHERE `timestamp` > '0' AND `geo` = '".$table."')"; }
} else { $sql_array[] = "select distinct `ip`,geo from `".$table_name_array[0]."` WHERE `timestamp` > '0' AND `geo` = '".$table."'"; }
$sql = mysql_query("".implode(' UNION ',$sql_array)." ORDER by geo");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select distinct `ip`,geo from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' AND `geo` = '".$table."' ORDER by geo");
} else { return array(); }
}
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[1];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
}
}
return $datashow;
}

function check_hits_stat($table_name_array,$table='XX',$datesearch=''){
global $db,$table_list;
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select geo from `".$sql_tablename."` WHERE `timestamp` > '0' AND `geo` = '".$table."')"; }
} else { $sql_array[] = "select geo from `".$table_name_array[0]."` WHERE `timestamp` > '0' AND `geo` = '".$table."'"; }
$sql = mysql_query("".implode(' UNION ALL ',$sql_array)." ORDER by geo");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select geo from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' AND `geo` = '".$table."' ORDER by geo");
} else { return array(); }
}
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[0];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
	   	   
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
}
}
return $datashow;
}

function db_unique_stat($table_name_array,$dbcolum,$datesearch=''){
global $db,$table_list;
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select ".($dbcolum == "ip" ? "distinct `ip`" : "distinct `ip`,".$dbcolum."")." from `".$sql_tablename."` WHERE `timestamp` > '0' AND `geo` != 'XX')"; }
} else { $sql_array[] = "select distinct `ip`,".$dbcolum." from `".$table_name_array[0]."` WHERE `timestamp` > '0' AND `geo` != 'XX'"; }
$sql = mysql_query("".implode(' UNION ',$sql_array)." ORDER by ".$dbcolum."");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select distinct `ip`,".$dbcolum." from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' AND `geo` != 'XX' ORDER by ".$dbcolum."");
} else { return array(); }
}
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = ($dbcolum == "ip" ? $listex[0] : $listex[1]);
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
	   	   /*
		   if($othercnt<9){
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
}else{
$othernum=$othernum+$rowArray[0];
}
$othercnt=$othercnt+1;
*/
}
/*
if(!empty($othernum)){
$datashow['others'] = array('Others',$othernum);
}
*/
}
return $datashow;
}

function db_hits_stat($table_name_array,$dbcolum,$datesearch=''){
global $db,$table_list;
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select ".$dbcolum." from `".$sql_tablename."` WHERE `timestamp` > '0' AND `geo` != 'XX')"; }
} else { $sql_array[] = "select ".$dbcolum." from `".$table_name_array[0]."` WHERE `timestamp` > '0' AND `geo` != 'XX'"; }
$sql = mysql_query("".implode(' UNION ALL ',$sql_array)." ORDER by ".$dbcolum."");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select ".$dbcolum." from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' AND `geo` != 'XX' ORDER by ".$dbcolum."");
} else { return array(); }
}
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[0];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
	   	   
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
	   	   /*
		   if($othercnt<9){
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
}else{
$othernum=$othernum+$rowArray[0];
}
$othercnt=$othercnt+1;
*/
}
/*
if(!empty($othernum)){
$datashow['others'] = array('Others',$othernum);
}
*/
}
return $datashow;
}

function db_count_stat($table_name_array,$datesearch=''){
global $db,$table_list;
$data_count = '';
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select `timestamp`,`ip`,`target`,`referer`,`domain`,`geo`,`os`,`agent`,`cookie`,`proxytype`,`esdid`,`devices` from `".$sql_tablename."` WHERE `timestamp` > '0')"; }
} else { $sql_array[] = "select `timestamp`,`ip`,`target`,`referer`,`domain`,`geo`,`os`,`agent`,`cookie`,`proxytype`,`esdid`,`devices` from `".$table_name_array[0]."` WHERE `timestamp` > '0'"; }
$sql = mysql_query("".implode(' UNION ALL ',$sql_array)." ORDER by `timestamp` desc");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select `timestamp`,`ip`,`target`,`referer`,`domain`,`geo`,`os`,`agent`,`cookie`,`proxytype`,`esdid`,`devices` from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' ORDER by `timestamp` desc");
} else { return '0'; }
}
$data_count = mysql_num_rows($sql); mysql_free_result($sql);
}
return $data_count;
}

function db_logs_stat($start_from='1',$table_name_array,$datesearch=''){
global $db,$table_list;
$sql = '';
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select `timestamp`,`ip`,`target`,`referer`,`domain`,`geo`,`os`,`agent`,`cookie`,`proxytype`,`esdid`,`devices` from `".$sql_tablename."` WHERE `timestamp` > '0')"; }
} else { $sql_array[] = "select `timestamp`,`ip`,`target`,`referer`,`domain`,`geo`,`os`,`agent`,`cookie`,`proxytype`,`esdid`,`devices` from `".$table_name_array[0]."` WHERE `timestamp` > '0'"; }
$sql = mysql_query("".implode(' UNION ALL ',$sql_array)." ORDER by `timestamp` desc LIMIT ".$start_from.", ".STAT_ALLDATA_COUNT."");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select `timestamp`,`ip`,`target`,`referer`,`domain`,`geo`,`os`,`agent`,`cookie`,`proxytype`,`esdid`,`devices` from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' ORDER by `timestamp` desc LIMIT ".$start_from.", ".STAT_ALLDATA_COUNT."");
} else { return ''; }
}
}
return $sql;
}

function cmp($a, $b) {
	if ($a[1] == $b[1]) {
		return 0;
	}
	return ($a[1] >= $b[1]) ? -1 : 1;
}

function db_unique_stat_($dbcolum,$sql_tablename='') {
global $db,$table_list;
$datashow = array();
if (!empty($sql_tablename)) { $sqltablename=$sql_tablename; } else  { $sqltablename=DB_PREFIX_SET."traffic_".date("j_n_Y"); }
$sql = mysql_query("select distinct `ip`,".$dbcolum." from `".$sqltablename."` WHERE `timestamp` > '0' AND `geo` != 'XX' ORDER by ".$dbcolum."");
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = ($dbcolum == "ip" ? $listex[0] : $listex[1]);
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = $rowArray[0];
}
return $datashow;
}

function db_hits_stat_($dbcolum,$sql_tablename='') {
global $db,$table_list;
$datashow = array();
if (!empty($sql_tablename)) { $sqltablename=$sql_tablename; } else  { $sqltablename=DB_PREFIX_SET."traffic_".date("j_n_Y"); }
$sql = mysql_query("select ".$dbcolum." from `".$sqltablename."` WHERE `timestamp` > '0' AND `geo` != 'XX' ORDER by ".$dbcolum."");
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[0];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = $rowArray[0];
}
return $datashow;
}

function _db_hits_stat_($table_section='',$table_name_array=array()) {
$hitsstat_ = array();
$_hits_stat_ = array();
$hits_stat_ = array();
foreach ($table_name_array as $sql_tablename) {
if (file_exists(LOGS.'cashe/'.$sql_tablename.'_hits_'.$table_section)) {
$_hits_stat_file = file(LOGS.'cashe/'.$sql_tablename.'_hits_'.$table_section);
$_hits_stat_[] = unserialize(trim($_hits_stat_file[0])); unset($_hits_stat_file);
}
else {
$_hits_stat_[] = db_hits_stat_($table_section,$sql_tablename);
}
}
foreach ($_hits_stat_ as $stater) {
foreach ($stater as $kays => $_stater) {
$hits_stat_[$kays][] = $_stater;
}
}
foreach ($hits_stat_ as $_kays => $stater_) {
$hitsstat_[''.$_kays.''] = array(str_replace('_',' ',$_kays),array_sum($stater_));
}
uasort($hitsstat_, "cmp");
//natrsort($hitsstat_);
unset($hits_stat_);
unset($_hits_stat_);
return $hitsstat_;
}

function _db_unique_stat_($table_section='',$table_name_array=array()) {
$hitsstat_ = array();
$_hits_stat_ = array();
$hits_stat_ = array();
foreach ($table_name_array as $sql_tablename) {
if (file_exists(LOGS.'cashe/'.$sql_tablename.'_unique_'.$table_section)) {
$_hits_stat_file = file(LOGS.'cashe/'.$sql_tablename.'_unique_'.$table_section);
$_hits_stat_[] = unserialize(trim($_hits_stat_file[0])); unset($_hits_stat_file);
}
else {
$_hits_stat_[] = db_unique_stat_($table_section,$sql_tablename);
}
}
foreach ($_hits_stat_ as $stater) {
foreach ($stater as $kays => $_stater) {
$hits_stat_[$kays][] = $_stater;
}
}
foreach ($hits_stat_ as $_kays => $stater_) {
$hitsstat_[''.$_kays.''] = array(str_replace('_',' ',$_kays),array_sum($stater_));
}
uasort($hitsstat_, "cmp");
//natrsort($hitsstat_);
unset($hits_stat_);
unset($_hits_stat_);
return $hitsstat_;
}

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////

function db_unique_stat_pc($table_name_array,$dbcolum,$datesearch=''){
global $db,$table_list,$pcid;
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select ".($dbcolum == "ip" ? "distinct `ip`" : "distinct `ip`,".$dbcolum."")." from `".$sql_tablename."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '').")"; }
} else { $sql_array[] = "select distinct `ip`,".$dbcolum." from `".$table_name_array[0]."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '').""; }
$sql = mysql_query("".implode(' UNION ',$sql_array)." ORDER by ".$dbcolum."");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select distinct `ip`,".$dbcolum." from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '')." ORDER by ".$dbcolum."");
} else { return array(); }
}
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = ($dbcolum == "ip" ? $listex[0] : $listex[1]);
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
	   	   /*
		   if($othercnt<9){
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
}else{
$othernum=$othernum+$rowArray[0];
}
$othercnt=$othercnt+1;
*/
}
/*
if(!empty($othernum)){
$datashow['others'] = array('Others',$othernum);
}
*/
}
return $datashow;
}

function db_hits_stat_pc($table_name_array,$dbcolum,$datesearch=''){
global $db,$table_list,$pcid;
$datashow = array();
if (!empty($table_name_array)) {
if ($datesearch == '1') {
if (count($table_name_array) > 1) {
foreach ($table_name_array as $sql_tablename) { $sql_array[] = "(select ".$dbcolum." from `".$sql_tablename."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '').")"; }
} else { $sql_array[] = "select ".$dbcolum." from `".$table_name_array[0]."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '').""; }
$sql = mysql_query("".implode(' UNION ALL ',$sql_array)." ORDER by ".$dbcolum."");
} else {
if (in_array(DB_PREFIX_SET."traffic_".date("j_n_Y"), $table_list)) {
$sql = mysql_query("select ".$dbcolum." from `".DB_PREFIX_SET."traffic_".date("j_n_Y")."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '')." ORDER by ".$dbcolum."");
} else { return array(); }
}
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[0];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
	   	   
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
	   	   /*
		   if($othercnt<9){
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = array(str_replace('_',' ',$rowArray[1]),$rowArray[0]);
}else{
$othernum=$othernum+$rowArray[0];
}
$othercnt=$othercnt+1;
*/
}
/*
if(!empty($othernum)){
$datashow['others'] = array('Others',$othernum);
}
*/
}
return $datashow;
}

function db_unique_stat_pc_($dbcolum,$sql_tablename='') {
global $db,$table_list,$pcid;
$datashow = array();
if (!empty($sql_tablename)) { $sqltablename=$sql_tablename; } else  { $sqltablename=DB_PREFIX_SET."traffic_".date("j_n_Y"); }
$sql = mysql_query("select distinct `ip`,".$dbcolum." from `".$sqltablename."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '')." ORDER by ".$dbcolum."");
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = ($dbcolum == "ip" ? $listex[0] : $listex[1]);
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = $rowArray[0];
}
return $datashow;
}

function db_hits_stat_pc_($dbcolum,$sql_tablename='') {
global $db,$table_list,$pcid;
$datashow = array();
if (!empty($sql_tablename)) { $sqltablename=$sql_tablename; } else  { $sqltablename=DB_PREFIX_SET."traffic_".date("j_n_Y"); }
$sql = mysql_query("select ".$dbcolum." from `".$sqltablename."` WHERE `timestamp` > '0' AND `geo` != 'XX' ".(!empty($pcid) ? " AND `cookie` = '".trim($pcid)."'" : '')." ORDER by ".$dbcolum."");
$count=0;
$refar = array();
$datashow = array();
while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
$ref = $listex[0];
$refe = explode('/', $ref);
if(!empty($refe[2])){$ref=$refe[2];}
$refar[$count]=$ref;
$count=$count+1;
} mysql_free_result($sql);
$prevout=0;
$count_o=0;
$count_m=0;
foreach($refar as $out) {
if($out==$prevout){
$count_o=$count_o+1;
}else{
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
}
$prevout=$out;
}
$unics[$count_m]=$count_o."_|_".$prevout;
$count_o=1;
$count_m=$count_m+1;
natsort($unics);
$unics=array_reverse($unics);
if($unics[0]==0){unset($unics[0]);} 
$othercnt=0;
$othernum=0;
foreach($unics as $unic) {
       $rowArray = explode('_|_',$unic);
	   	   if(empty($rowArray[0])){$rowArray[0]="NONE";} 
	   	   if(empty($rowArray[1])){$rowArray[1]="NONE";} 
$rowArray[1]=str_replace(';','',$rowArray[1]);
$datashow[''.$rowArray[1].''] = $rowArray[0];
}
return $datashow;
}

function pc_db_hits_stat_($table_section='',$table_name_array=array()) {
$hitsstat_ = array();
$_hits_stat_ = array();
$hits_stat_ = array();
foreach ($table_name_array as $sql_tablename) {
$_hits_stat_[] = db_hits_stat_pc_($table_section,$sql_tablename);
}
foreach ($_hits_stat_ as $stater) {
foreach ($stater as $kays => $_stater) {
$hits_stat_[$kays][] = $_stater;
}
}
foreach ($hits_stat_ as $_kays => $stater_) {
$hitsstat_[''.$_kays.''] = array(str_replace('_',' ',$_kays),array_sum($stater_));
}
uasort($hitsstat_, "cmp");
//natrsort($hitsstat_);
unset($hits_stat_);
unset($_hits_stat_);
return $hitsstat_;
}
function pc_db_unique_stat_($table_section='',$table_name_array=array()) {
$hitsstat_ = array();
$_hits_stat_ = array();
$hits_stat_ = array();
foreach ($table_name_array as $sql_tablename) {
$_hits_stat_[] = db_unique_stat_pc_($table_section,$sql_tablename);
}
foreach ($_hits_stat_ as $stater) {
foreach ($stater as $kays => $_stater) {
$hits_stat_[$kays][] = $_stater;
}
}
foreach ($hits_stat_ as $_kays => $stater_) {
$hitsstat_[''.$_kays.''] = array(str_replace('_',' ',$_kays),array_sum($stater_));
}
uasort($hitsstat_, "cmp");
//natrsort($hitsstat_);
unset($hits_stat_);
unset($_hits_stat_);
return $hitsstat_;
}

function _auth_request($server_api_url, $data = array(), $timeuot = '30', $PANEL_LOGIN='', $PANEL_PASSW='') {
    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_CRLF, true);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $PANEL_LOGIN.":".$PANEL_PASSW);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeuot);
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    curl_setopt($ch, CURLOPT_URL, $server_api_url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $results =curl_exec($ch);
    curl_close($ch);
    return $results;
}

function check_diapazon_ip($ip='',$ip_range='') {
	
	if (empty($ip)) { return false; }
	if (empty($ip_range)) { return false; }
	if (!preg_match('/\-/isu', trim($ip_range))) { return false; }
	
	if (!preg_match('/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/isu', trim($ip))) { return false; }
	
	$war = false;
	
	$mip = ip2long($ip);
	
	$ip_range_array = explode('|', trim($ip_range));
	
	foreach ($ip_range_array as $_ip_range) {
		
		if (!preg_match('/\-/isu', trim($_ip_range))) { continue; }
		
		$ip_range_exp = explode("-", trim($_ip_range));
		
		if (empty($ip_range_exp[0])) { continue; }
		if (empty($ip_range_exp[1])) { continue; }
		
		if (!preg_match('/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/isu', trim($ip_range_exp[0]))) { continue; }
		if (!preg_match('/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/isu', trim($ip_range_exp[1]))) { continue; }
		
		$ip_start = ip2long(trim($ip_range_exp[0]));
		$ip_end = ip2long(trim($ip_range_exp[1]));
		
		if ($mip >= $ip_start && $mip <= $ip_end) {
			$war = true;
			break;
		}
	} unset($ip_range_array);
	
	if ($war) {
		return true;
	}
	else {
		return false;
	}
}
