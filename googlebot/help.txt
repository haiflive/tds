Системные требования:
Минимальная конфигурация сервера: i7-2600 Quad-Core,16GB,2x3TB или Intel Xeon E3,16GB,2х1TB
Операционная система: Linux Debian 64 bit
Версия PHP: не ниже 5.5.0
Версия MySQL: не ниже 5.5.3

Установка:
1. 
Ставим htop для мониторинга сервера: http://htop.sourceforge.net/index.php?page=downloads

Ставим apt-get install curl libcurl3 libcurl3-dev php5-curl php5-mcrypt ( перезагружаем если нужно /etc/init.d/apache2 restart)

Ставим composer https://github.com/maxmind/GeoIP2-php, ставим в директорию сайта.

Вирубаем open_basedir и рестартим service apache2 restart

Провериить что бы php понимал <? кроме <?php , В файле php.ini нужно исправить значение параметра short_open_tag на On (short_open_tag = On)

3. Вирубить логи сервера.

4. Включить innodb_file_per_table (как это сделать смотрим ниже)

6. В ручную выставить прва 777 на файлы и директории:
dump/ (и всему что внути)
run/ (и всему что внути)
logs/ (и всему что внути)
utilites/ (и всему что внути)
lib/esd_sources/ (и всему что внути)
redirect.esd.config.php

7. Создаем MySQL бд на сервере и прописать их в файле config.php

8. Запускаем install.php

9. Заходим в настройки https://domain.com/admin.php и вносим начальные настройки админки.
Доступ к скрипту по умолчанию.
Логин: admin
Пароль: admin

10. Ниже добавить в крон (wget не юзать).

0 */1 * * * root cd /var/www/www-root/data/www/indigon.net/googlebot/;/usr/bin/php cron_cashe.php >/dev/null 2>&1
*/30 * * * * root cd /var/www/www-root/data/www/indigon.net/googlebot/;/usr/bin/php cron_redirect_esd.php >/dev/null 2>&1
0 */5 * * * root cd /var/www/www-root/data/www/indigon.net/googlebot/;/usr/bin/php cron_bots_search.php >/dev/null 2>&1
0 */1 * * * root cd /var/www/www-root/data/www/indigon.net/googlebot/;/usr/bin/php cron_id.php >/dev/null 2>&1

11. После обновление файла ip-new-list.dat необходимо растартнуть апач

Это пока все :)

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
В PHP выставить лимиты:
upload_max_filesize 528M
memory_limit 1024M
post_max_size 528M
max_execution_time 0

В MySQL мы будем использовать тип таблиц InnoDB и MyISAM,
по этому предвалительно убедитесть что InnoDB у Вас включен и оптимизирован.
Пример оптимизации в файле /etc/mysql/my.cnf

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
#########################################################################################
#
# The MySQL database server configuration file.
#
# You can copy this to one of:
# - "/etc/mysql/my.cnf" to set global options,
# - "~/.my.cnf" to set user-specific options.
# 
# One can use all long options that the program supports.
# Run program with --help to get a list of available options and with
# --print-defaults to see which it would actually understand and use.
#
# For explanations see
# http://dev.mysql.com/doc/mysql/en/server-system-variables.html

# This will be passed to all mysql clients
# It has been reported that passwords should be enclosed with ticks/quotes
# escpecially if they contain "#" chars...
# Remember to edit /etc/mysql/debian.cnf when changing the socket location.
[client]
port		= 3306
socket		= /var/run/mysqld/mysqld.sock

# Here is entries for some specific programs
# The following values assume you have at least 32M ram

# This was formally known as [safe_mysqld]. Both versions are currently parsed.
[mysqld_safe]
socket		= /var/run/mysqld/mysqld.sock
nice		= 0

[mysqld]
#
# * Basic Settings
#
user		= mysql
pid-file	= /var/run/mysqld/mysqld.pid
socket		= /var/run/mysqld/mysqld.sock
port		= 3306
basedir		= /usr
datadir		= /var/lib/mysql
tmpdir		= /tmp
lc-messages-dir	= /usr/share/mysql
skip-external-locking
#
# Instead of skip-networking the default is now to listen only on
# localhost which is more compatible and is not less secure.
bind-address		= 127.0.0.1
#
# * Fine Tuning
#
key_buffer              = 128M
max_allowed_packet      = 128M
thread_stack            = 192K
thread_cache_size       = 16
table_open_cache        = 512
key_buffer_size         = 2G
sort_buffer_size        = 16M
tmp_table_size        	= 64M
max_heap_table_size     = 4M
# This replaces the startup script and checks MyISAM tables if needed
# the first time they are touched
myisam-recover         = BACKUP
max_connections        = 1500
#table_cache            = 64
#thread_concurrency     = 10
#
# * Query Cache Configuration
#
query_cache_limit	= 16M
query_cache_size        = 64M
#
# * Logging and Replication
#
# Both location gets rotated by the cronjob.
# Be aware that this log type is a performance killer.
# As of 5.1 you can enable the log at runtime!
#general_log_file        = /var/log/mysql/mysql.log
#general_log             = 1
#
# Error logging goes to syslog due to /etc/mysql/conf.d/mysqld_safe_syslog.cnf.
#
# Here you can see queries with especially long duration
#log_slow_queries	= /var/log/mysql/mysql-slow.log
#long_query_time = 2
#log-queries-not-using-indexes
#
# The following can be used as easy to replay backup logs or for replication.
# note: if you are setting up a replication slave, see README.Debian about
#       other settings you may need to change.
#server-id		= 1
#log_bin			= /var/log/mysql/mysql-bin.log
expire_logs_days	= 10
max_binlog_size         = 100M
#binlog_do_db		= include_database_name
#binlog_ignore_db	= include_database_name
#
# * InnoDB
#
# InnoDB is enabled by default with a 10MB datafile in /var/lib/mysql/.
# Read the manual for more InnoDB related options. There are many!
#
# * Security Features
#
# Read the manual, too, if you want chroot!
# chroot = /var/lib/mysql/
#
# For generating SSL certificates I recommend the OpenSSL GUI "tinyca".
#
# ssl-ca=/etc/mysql/cacert.pem
# ssl-cert=/etc/mysql/server-cert.pem
# ssl-key=/etc/mysql/server-key.pem

innodb_flush_log_at_trx_commit	= 2
innodb_buffer_pool_size	        = 2G
innodb_additional_mem_pool_size	= 20M
innodb_log_buffer_size	        = 16M
innodb_file_per_table           = 1

[mysqldump]
quick
quote-names
max_allowed_packet	= 16M

[mysql]
#no-auto-rehash	# faster start of mysql but no tab completition

[isamchk]
key_buffer		= 16M

#
# * IMPORTANT: Additional settings that can override those from this file!
#   The files must end with '.cnf', otherwise they'll be ignored.
#
!includedir /etc/mysql/conf.d/
#########################################################################################
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

Как включить innodb_file_per_table:

Как включить MySQL innodb_file_per_table? 
Привожу рецепт по включению такой полезной функции как innodb_file_per_table.

Зачем это нужно?
MySQL по умолчанию все таблички innodb хранит в одном файле - когда их накапливается приличное количество - файл значительно разрастается.
Плюс не забывайте, что при удалении данных в innodb - размер файла не уменьшается - он растет только в большую сторону. 
Так что если данных в базе у вас много или идет активное удаление - рано или поздно вы задумаетесь о том, чтобы выполнить подобное разделение
Что нужно сделать?
1.Прежде всего - отрубаем нагрузку, выключаем связь сервер с внешним миром  - php и прочее беспокоить вас не должны - процесс не быстрый
2.Теперь тщательно делаем ПОЛНЫЙ бекап всех баз данных и конфига 
3.Удаляем все таблицы из БД 
4.Выключаем mysqld 
5.В /etc/my.cnf добавляем

innodb_file_per_table=1

6.удалаем cледующие файлы если есть
/var/lib/ibdata1 
/var/lib/ib_logfile0 
/var/lib/ib_logfile1 
или сколько там файлов ibdata - оставлять старые logfile нельзя!! 
7.запускаем mysqld 
8.вкатываем таблицы обратно 
9.проверяем наличие свежесозданных файликов *.ibd 
PROFIT!! 
Да, разумеется - операция опасная по сути, поэтому - бекап, бекап и еще раз бекап.

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

В общем измучился пока наладил все.

Теперь тдс работает с форматами типо:

http://dom.com/--RN1--/

http://dom.com/--RN1--/--RN2--/

http://dom.com/--RN1--/--RN2--.php

http://dom.com/--RN1--/--RN2--.asp

Где --RN1-- или --RN2-- генерируют рандомную строку из лат. букв.

В итогле линк на тдс будет иметь вид например http://dom.com/khcdszzapr/dshkfrfccx/

Думаю лучше не привязываться к реальним словам, а так же к каким то расширениям, по этому выбрал второй вариант.

Нововведение появиться в сл. партии. есд файл не трогал, все изменения в тдс сделал.

RewriteEngine on
RewriteCond %{REQUEST_URI} !^/googlebot/$
RewriteRule ^(.*)/(.*)$ /googlebot/search.post.php [L,QSA]

/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

На данный момент ESD работает только с передачей в URL какой либо переменной, сделано это для защиты отботов в шеллах, чтоб лишный раз не обращались к тдс.

Что бы убрать привязку к обьязательным переменним нужно отредактировать esd API api.esd.php в TDS убрав if(!empty($_GET)){

А так же если нужно работать без ID и параметров то нужно редактировать search.post.php (711 по 781 строки) и файлы /lib/esd_sources/* в переменных $URL_DAFAULT и $URL_NUM0

Удачи ;)