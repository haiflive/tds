<?php
session_start();

require(rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/config.php');

// LOGIN 1
if ((isset($_SESSION['PANEL_LOGIN']) && !empty($_SESSION['PANEL_LOGIN']) && $_SESSION['PANEL_LOGIN'] == PANEL_LOGIN)
&& (isset($_SESSION['PANEL_PASSW']) && !empty($_SESSION['PANEL_PASSW']) && $_SESSION['PANEL_PASSW'] == PANEL_PASSW)) {
    include(LIBS.'functions.php');
    include(LIBS.'database.php');
    include(LIBS.'snoopy.start.headers.php');

    $db = new database();
    $db->connect();

    /*
    die;
    */

    // Тут и так понятно думаю
    $date = (isset($_GET['date']) ? str_replace('-', '_', $_GET['date']) : '');
    if (empty($date)) {
        $date=date("j_n_Y");
    }
    $start_date = (isset($_GET['start_date']) ? str_replace('-', '_', $_GET['start_date']) : '');
    $datesearch = (isset($_GET['datesearch']) ? $_GET['datesearch'] : '');
    $showpage = (isset($_GET['showpage']) ? $_GET['showpage'] : '');
    $act = (isset($_REQUEST['act']) ? $_REQUEST['act'] : '');
    $new_macname = (isset($_REQUEST['new_macname']) ? $_REQUEST['new_macname'] : '');
    $del_macname = (isset($_REQUEST['del_macname']) ? $_REQUEST['del_macname'] : '');

    // Расшипляем дату старта и конечную
    $explp = explode("_", $date);
    $day = $explp[0];
    $month = $explp[1];
    $year = $explp[2];

    if (!empty($start_date)) {
        $start_explp = explode("_", $start_date);
        $start_day = $start_explp[0];
        $start_month = $start_explp[1];
        $start_year = $start_explp[2];
    } else {
        $start_date='1_'.$month.'_'.$year;
    }

    // какой то непонятный код из старого скрипта
    $redate=mktime(0, 0, 0, $month, $day, $year);
    $redate=strtotime('-1 day', $redate);
    $prevday_day=date('j', $redate);
    $prevday_month=date('n', $redate);
    $prevday_year=date('Y', $redate);
    $redate=mktime(0, 0, 0, $month, $day, $year);
    $redate=strtotime('+1 day', $redate);
    $futday_day=date('j', $redate);
    $futday_month=date('n', $redate);
    $futday_year=date('Y', $redate);
    $redate=mktime(0, 0, 0, $month, $day, $year);
    $redate=strtotime('-1 month', $redate);
    $prevmonth_day=date('j', $redate);
    $prevmonth_month=date('n', $redate);
    $prevmonth_year=date('Y', $redate);
    $redate=mktime(0, 0, 0, $month, $day, $year);
    $redate=strtotime('+1 month', $redate);
    $futmonth_day=date('j', $redate);
    $futmonth_month=date('n', $redate);
    $futmonth_year=date('Y', $redate);

    // Выводим именя всех таблиц начинающийся с traffic_
    $table_list = array();
    $res_list = mysql_query("SHOW TABLES");
    if (mysql_num_rows($res_list)) {
        while ($result_list = mysql_fetch_array($res_list)) {
            if (preg_match('/'.DB_PREFIX_SET.'traffic_(\d+)_(\d+)_(\d\d\d\d)/i', $result_list[0])) {
                $table_list[] = trim($result_list[0]);
            }
        }
    }
    mysql_free_result($res_list);

    // Выводим список дат по введенному периоду
    $date_array = getInterval($start_date, $date, 'j_n_Y');

    // Выводим имена таблиц которые совпали в указанном периоде
    $table_name_array = array();
    foreach ($table_list as $table_name) {
        if (in_array($table_name, $date_array)) {
            $table_name_array[] = $table_name;
        }
    }


    if ($act == 'cldom' && isset($_GET['cldom'])) {
        foreach ($table_list as $table_list_name) {
            mysql_query("DELETE FROM `".$table_list_name."` WHERE `domain` = '".urldecode($_GET['cldom'])."'");
        }
    }

    $uresults = array();

    $uresults[] = '<form name="editfrm" method="post" action="'.SITE_URL.'/esd_log.php?do=change" target="log"><input type="text" id="gggInput" name="console" value="console&gt;">';

    if (!empty($_POST['console'])) {
        if ($_POST['console']=='console>help') {
            $uresults[] = ']Accepted commands: ';
            $uresults[] = '<br>]add http://esdserver';
            $uresults[] = '<br>]Консоль в режиме разработке, позже появятся полезные фишки';
        }
        $cpo = explode(" ", $_POST['console']);
        //$cpo[0];
        if ($cpo[0]=='console>add') {
            if (!empty($cpo[1])) {
                if ($snoopy -> fetch($cpo[1])) {
                    $uresults[] = '<span style="background: #000000;">['.date("H:i:s").']</span>added esd '.$cpo[1];
                } else {
                    $uresults[] = '<span style="background: #000000;">['.date("H:i:s").']</span>not added esd '.$cpo[1];
                }
            } else {
                $uresults[] = '<span style="background: #000000;">['.date("H:i:s").']</span>not added esd';
            }
        }
        if ($cpo[0]=='console>change') {
            $formvars['amode'] = $cpo[1];
            $formvars['chfrom'] = $cpo[2];
            $formvars['chto'] = $cpo[4];
        }
        /*
        
        if($cpo[0]=='console>add'){}
        if($cpo[0]=='console>add'){}
        if($cpo[0]=='console>add'){}
        
        */

        $uresults[] = '</form></body>
</html>';

        include(ROOT.'theme/esd_log.php');

        die();
    }


    if (isset($_GET['console']) && $_GET['console']=='ajax_refresh') {
        mysql_query("TRUNCATE TABLE `".DB_PREFIX_SET."esd_tmp`");

        $uresults[] = '<script type="text/javascript" src="'.SITE_URL.'/theme/js/fullajax.js"></script>';

        $count=0;
        $tablename_array = array();
        $refar = array();

        foreach ($table_list as $table_list_name) {
            $tablename_array[] = "(SELECT `domain` FROM `".$table_list_name."` WHERE `timestamp` > '0')";
        }
        $sql = mysql_query("SELECT `domain` FROM (".implode(' UNION ALL ', $tablename_array).") t GROUP BY `domain`");

        while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
            $ref = $listex[0];
            $refar[$count]=$ref;
            $count++;
        }
        mysql_free_result($sql);

        $count_o=0;
        $count_m=0;
        $prevout=0;

        foreach ($refar as $out) {
            if ($out==$prevout) {
                $count_o++;
            } else {
                $unics[$count_m]=$count_o."_|_".$prevout;
                $count_o=1;
                $count_m++;
            }
            $prevout=$out;
        }

        $unics[$count_m]=$count_o."_|_".$prevout;
        $count_o=1;
        $count_m=$count_m+1;
        natsort($unics);
        $unics=array_reverse($unics);
        if ($unics[0]==0) {
            unset($unics[0]);
        }
        foreach ($unics as $unic) {
            $rowArray = explode('_|_', $unic);
            if ($rowArray[1]) {
                $uresults[] = '<br><include src="'.SITE_URL.'/ajax.th.php?u='.urlencode($rowArray[1]).'" ax:nohistory="1"></include>';
            }
        }
    }

    if (isset($_POST['amode']) && $_POST['amode']=='postonce') {
        $formvars['ptds'] = $_POST['tds'];
        $formvars['ptdsip'] = $_POST['tdsip'];
        $formvars['pkey'] = $_POST['key'];
        $formvars['pto'] = $_POST['reserve'];
        $formvars['pesdid'] = $_POST['esdid'];

        if ($snoopy->submit($_POST['esd'], $formvars)) {
            $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span>post ok: '.$_POST['esd'].' <b> &nbsp; DATA</b>[tds: '.$_POST['tds'].' tds ip: '.$_POST['tdsip'].' key: '.$_POST['key'].' reserve: '.$_POST['reserve'].' esd id: '.$_POST['esdid'].']';
        } else {
            $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span><b>post error</b>: '.$_POST['esd'].' <b> &nbsp; DATA</b>[tds: '.$_POST['tds'].' tds ip: '.$_POST['tdsip'].' key: '.$_POST['key'].' reserve: '.$_POST['reserve'].' esd id: '.$_POST['esdid'].']';
        }
    } else {
        if (isset($_GET['do']) && $_GET['do']=='change') {
            $count=0;
            $tablenamearray = array();

            foreach ($table_list as $table_list_name) {
                $tablenamearray[] = "(SELECT `domain` FROM `".$table_list_name."` WHERE `timestamp` > '0')";
            }
            $sql = mysql_query("".implode(' UNION ALL ', $tablenamearray)." ORDER by `domain` desc");

            while ($listex = mysql_fetch_array($sql, MYSQL_NUM)) {
                $ref = $listex[0];
                $refar[$count]=$ref;
                $count++;
            }
            mysql_free_result($sql);

            $count_o=0;
            $count_m=0;
            $prevout=0;

            foreach ($refar as $out) {
                if ($out==$prevout) {
                    $count_o++;
                } else {
                    $unics[$count_m]=$count_o."_|_".$prevout;
                    $count_o=1;
                    $count_m++;
                }
                $prevout=$out;
            }

            $unics[$count_m]=$count_o."_|_".$prevout;

            $count_o=1;
            $count_m=$count_m+1;
            natsort($unics);
            $unics=array_reverse($unics);
            if ($unics[0]==0) {
                unset($unics[0]);
            }

            foreach ($unics as $unic) {
                $rowArray = explode('_|_', $unic);
                if ($rowArray[1]) {
                    if ($snoopy -> fetch($rowArray[1]."?key=".DOMAINS_KEY."&mode=config")) {
                        $tds=grizli('name="ptds" value="', '"', $snoopy->results);
                        $tdsip=grizli('name="ptdsip" value="', '"', $snoopy->results);
                        $key=grizli('name="pkey" value="', '"', $snoopy->results);
                        $reserve=grizli('name="pto" value="', '"', $snoopy->results);
                        $esdid=grizli('name="pesdid" value="', '"', $snoopy->results);

                        if (isset($_POST['amode']) && $_POST['amode']=='tds') {
                            if (isset($_POST['chfrom']) && $_POST['chfrom']==$tds) {
                                $formvars['ptds'] = $_POST['chto'];
                                $formvars['ptdsip'] = $tdsip;
                                $formvars['pkey'] = $key;
                                $formvars['pto'] = $reserve;
                                $formvars['pesdid'] = $esdid;

                                if ($snoopy->submit($rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig", $formvars)) {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span>post ok: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                } else {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span><b>post error</b>: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                }
                            }
                        }
                        if (isset($_POST['amode']) && $_POST['amode']=='tdsip') {
                            if (isset($_POST['chfrom']) && $_POST['chfrom']==$tdsip) {
                                $formvars['ptds'] = $tds;
                                $formvars['ptdsip'] = $_POST['chto'];
                                $formvars['pkey'] = $key;
                                $formvars['pto'] = $reserve;
                                $formvars['pesdid'] = $esdid;
                                if ($snoopy->submit($rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig", $formvars)) {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span>post ok: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                } else {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span><b>post error</b>: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                }
                            }
                        }
                        if (isset($_POST['amode']) && $_POST['amode']=='key') {
                            if (isset($_POST['chfrom']) && $_POST['chfrom']==$key) {
                                $formvars['ptds'] = $tds;
                                $formvars['ptdsip'] = $tdsip;
                                $formvars['pkey'] = $_POST['chto'];
                                $formvars['pto'] = $reserve;
                                $formvars['pesdid'] = $esdid;
                                if ($snoopy->submit($rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig", $formvars)) {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span>post ok: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                } else {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span><b>post error</b>: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                }
                            }
                        }
                        if (isset($_POST['amode']) && $_POST['amode']=='reserve') {
                            if (isset($_POST['chfrom']) && $_POST['chfrom']==$reserve) {
                                $formvars['ptds'] = $tds;
                                $formvars['ptdsip'] = $tdsip;
                                $formvars['pkey'] = $key;
                                $formvars['pto'] = $_POST['chto'];
                                $formvars['pesdid'] = $esdid;
                                if ($snoopy->submit($rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig", $formvars)) {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span>post ok: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                } else {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span><b>post error</b>: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                }
                            }
                        }
                        if (isset($_POST['amode']) && $_POST['amode']=='esdid') {
                            if (isset($_POST['chfrom']) && $_POST['chfrom']==$esdid) {
                                $formvars['ptds'] = $tds;
                                $formvars['ptdsip'] = $tdsip;
                                $formvars['pkey'] = $key;
                                $formvars['pto'] = $reserve;
                                $formvars['pesdid'] = $_POST['chto'];
                                if ($snoopy->submit($rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig", $formvars)) {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span>post ok: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                } else {
                                    $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span><b>post error</b>: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=setconfig".' <b> &nbsp; DATA</b>[tds: '.$formvars['ptds'].' tds ip: '.$formvars['ptdsip'].' key: '.$formvars['pkey'].' reserve: '.$formvars['pto'].' esd id: '.$formvars['pesdid'].']';
                                }
                            }
                        }
                    } else {
                        $uresults[] = '<br>
<span style="background: #000000;">['.date("H:i:s").']</span><b>server is down</b>: '.$rowArray[1]."?key=".DOMAINS_KEY."&mode=config";
                    }
                }
            }
        }
    }

    $uresults[] = '</form>';

    // Получаем имя открытого файла
    $explpname = explode("/", $_SERVER['SCRIPT_NAME']);
    foreach ($explpname as $i => $value) {
        $file_name=$explpname[$i];
    }

    include(ROOT.'theme/esd_log.php');

    unset($table_name_array);
    unset($date_array);
    unset($table_list);

    $db->close();

// LOGIN 2
} else {
    if ((isset($_POST['login']) && !empty($_POST['login']) && $_POST['login'] == PANEL_LOGIN)
&& (isset($_POST['pass']) && !empty($_POST['pass']) && $_POST['pass'] == PANEL_PASSW)) {
        $_SESSION['PANEL_LOGIN'] = PANEL_LOGIN;
        $_SESSION['PANEL_PASSW'] = PANEL_PASSW;
        echo '<meta http-equiv="refresh" content="0">';
        die();
    }

    include(ROOT.'theme/login.php');
}
