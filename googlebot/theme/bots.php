<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <form class="form-inline" role="form" method="get" name="form">
      <input type="hidden" name="datesearch" value="1">
      <div class="form-group">
      <div id="sandbox-container">
    	<div class="input-daterange input-group" id="datepicker" data-date="<?=$date;?>" data-date-format="d-m-yyyy">
      <span class="input-group-addon" style="border-left:1px solid #CCCCCC;">Начало</span>
      <input type="text" class="input-sm form-control" name="start_date" value="<?=str_replace('_','-',$start_date);?>" />
      <span class="input-group-addon">Конец</span>
      <input type="text" class="input-sm form-control" name="date" value="<?=str_replace('_','-',$date);?>" />
      </div>
      </div>
      </div>
      <button type="button" class="btn btn-primary btn-sm" onClick='document.form.submit();'>Применить</button>
      <a style="float:right;margin-left:5px;" class="btn btn-default btn-sm" href="<?=SITE_URL;?>/cron_bots_search.php" target="_blank">Обновить данные</a>
      <a style="float:right;margin-left:5px;" class="btn btn-default btn-sm various_m" data-fancybox-type="iframe" href="<?=SITE_URL;?>/bots.php?get_isp=1" target="_blank">Показать ISP по IP</a>
      <a style="float:right;" class="btn btn-default btn-sm various_lg" data-fancybox-type="iframe" href="<?=SITE_URL;?>/bots.php?get_dom_block=1" target="_blank">Показать что блочит Domenik</a>
      </form>
</div>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
    <?php if (!empty($bots_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Функция</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$bots_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Не принятый траффик</strong>'; }?>
</div>


<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
    <?php if (!empty($unknow_bots_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Функция</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$unknow_bots_stat_array);?>
      </tbody>
    </table>
    <a class="btn btn-default btn-sm" href="<?=SITE_URL;?>/bots.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>get_unknow_stat=1&start_date=<?=str_replace('_','-',$start_date);?>&date=<?=str_replace('_','-',$date);?>" target="_blank">Отобразить данные по трафику подозрительных IP-ов за указанный периуд</a>
    	<?php } else { echo '<strong>Подозрительный траффик</strong>'; }?>
</div>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <span style="color:red;">*</span> Подозрительный ИП - это адрес откуда раньше заходили с палевным юзер агентом:<br><br>
      <pre style="font-size:11px;"><?php echo implode("\n",$bot_agent); ?></pre>
</div>


<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
    <?php if (!empty($blacklist_ip_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Функция</th>
          <th>Количество</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$blacklist_ip_array);?>
      </tbody>
    </table>
    <a class="btn btn-default btn-sm various_m" data-fancybox-type="iframe" href="<?=SITE_URL;?>/bots.php?get_blacklist_ip=1" target="_blank">Отобразить или добавить IP в Blacklist</a>
    	<?php } else { echo '<strong>Список IP в Blacklist</strong>'; }?>
</div>


<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      Серверное время: <?php echo date("d.m.Y H:i:s"); ?>
</div>
<? //print_r(timezone_abbreviations_list()); ?> 