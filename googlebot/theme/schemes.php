<!-- Main component for a primary marketing message or call to action -->
<div class="jumbotron" style="padding:18px;margin-bottom:20px;">

<script src="<?=SITE_URL;?>/theme/js/palitra.js"></script>

<table width="100%"  border="0">
<tr>
<td>
<div align="center"><b>Текущие схемы</b></div><br>

<form name="editfrm" method="post" action="schemes.php">
<table width='100%' border='0' class="table table-bordered">
<tr bgcolor='#f3f7fa' class='hl'> 
<th width='16'></th>
<th width='40'>№</th>
<th width='110'>IP</th>
<th>Рефер</th>
<th>EsdID</th>
<th>Домен</th>
<th width='24'>Гео</th>
<th width='100'>ISP</th>
<th width='120'>ОС</th>
<th width='100'>Агент</th>
<th>Девайс</th>
<th>Параметры</th>
<th width='45'>Прокс</th>
<th width='45'>Уник</th>
<th bgcolor="#f7efdf">Действие</th>
<th width='16'></th>
</tr>
<?php echo implode("\n",$schemes_results); ?>
<td colspan="15">
<div align="center">
<button type="button" class="btn btn-primary btn-sm" onClick='document.editfrm.submit();'>Сохранить</button>
</div>
</td>
</table>
<input type="hidden" name="act" value="edit">
</form>
<br>

</td>
</tr>

<tr>
<td>
<div align="center"><b>Добавить схему</b></div><br>
<form name="addfrm" method="post" action="schemes.php">
<table width='100%' border='0' class="table table-bordered">
<tr bgcolor='#f3f7fa' class='hl'> 
<th width='40'>№</th>
<th width='110'>IP</th>
<th>Рефер</th>
<th>EsdID</th>
<th>Домен</th>
<th width='24'>Гео</th>
<th width='100'>ISP</th>
<th>ОС</th>
<th>Агент</th>
<th>Девайс</th>
<th>Параметры</th>
<th width='45'>Прокс</th>
<th width='45'>Уник</th>
<th bgcolor="#f7efdf">Действие</th>
<th bgcolor="#f7efdf"></th>
</tr>
<tr bgcolor='#ffffff' class='hl'> 
<td><input type="text" id='gggInput' name="no" size="3"></td><td><input type="text" id='gggInput' name="ip" size="13"></td>
<td><input type="text" id='gggInput' name="ref" size="33"></td><td><input type="text" id='gggInput' name="esdid" size="33"></td>
<td><input type="text" id='gggInput' name="dom" size="33"></td><td><input type="text" id='gggInput' name="geo"  size="3"></td>
<td><input type="text" id='gggInput' name="isp"  size="33"></td>
<td>
	<select id='gggInput' name='os'>
	<option value='' selected></option>
	<option value='Windows_8'>Windows 8</option>
	<option value='Windows_7'>Windows 7</option>
	<option value='Windows_Vista'>Windows Vista</option>
	<option value='Windows_XP'>Windows XP</option>
	<option value='Windows_RT'>Windows RT</option>
	<option value='Windows_Phone'>Windows Phone</option>
	<option value='Windows_NT'>Windows NT</option>
	<option value='Windows_Mobile'>Windows Mobile</option>
	<option value='Windows_ME'>Windows ME</option>
	<option value='Windows_CE'>Windows CE</option>
	<option value='Windows_98'>Windows 98</option>
	<option value='Windows_95'>Windows 95</option>
	<option value='Windows_2003_Server'>Windows 2003 Server</option>
	<option value='Windows_2000'>Windows 2000</option>
	<option value='Windows_Other'>Windows Other</option>
	<option value='OS_X'>OS X</option>
	<option value='iOS'>iOS</option>
	<option value='BlackBerry'>BlackBerry</option>
	<option value='Android'>Android</option>
	<option value='OpenBSD'>OpenBSD</option>
	<option value='Mac_OS'>Mac OS</option>
	<option value='Linux'>Linux</option>
	<option value='Ubuntu'>Ubuntu</option>
	<option value='MacOS'>MacOS</option>
	<option value='FreeBSD'>FreeBSD</option>
	<option value='BeOS'>BeOS</option>
	<option value='Other_OS'>Other OS</option>
	<option value='Unknown_OS'>Unknown OS</option>
	</select>
</td>
<td>
	<select id='gggInput' name='agent'>
	<option value='' selected></option>
	<option value='IE'>IE</option>
	<option value='Chrome'>Chrome</option>
	<option value='Chromium'>Chromium</option>
	<option value='Firefox'>Firefox</option>
	<option value='Mozilla'>Mozilla</option>
	<option value='Opera'>Opera</option>
	<option value='Netscape_Navigator'>Netscape</option>
	<option value='Android_Browser'>Android Browser</option>
	<option value='BlackBerry_Browser'>BlackBerry Browser</option>
	<option value='Chrome_Mobile'>Chrome Mobile</option>
	<option value='IE_Mobile'>IE Mobile</option>
	<option value='Mobile_Firefox'>Mobile Firefox</option>
	<option value='Mobile_Safari'>Mobile Safari</option>
	<option value='Nokia_Web_Browser'>Nokia Web Browser</option>
	<option value='Opera_Mini'>Opera Mini</option>
	<option value='Opera_Mobile'>Opera Mobile</option>
	<option value='Other_Mobile_Browser'>Other Mobile Browser</option>
	<option value='Other_Browser'>Other Browser</option>
	<option value='Unknown_Browser'>Unknown Browser</option>
	<option value='Seach_Bot'>Seach Bot</option>
	</select>
</td>
<td>
	<select id='gggInput' name='devices'>
	<option value='' selected></option>
	<option value='Personal_computer'>Personal computer</option>
	<option value='Tablet'>Tablet</option>
	<option value='Smartphone'>Smartphone</option>
	<option value='Game_console'>Game console</option>
	<option value='Smart_TV'>Smart TV</option>
	<option value='Wearable_computer'>Wearable computer</option>
	<option value='PDA'>PDA</option>
	<option value='Other'>Other</option>
	<option value='Unknown_Devices'>Unknown Devices</option>
	</select>
</td>
<td><input type="text" id='gggInput' name="cookie" size="33"></td>
<td><select id='gggInput' name='prox'><option value='' selected></option><option value='yes'>yes</option><option value='no'>no</option></select></td>
<td><select id='gggInput' name='unic'><option value='' selected></option><option value='yes'>yes</option><option value='no'>no</option></select></td>
<td bgcolor="#f7efdf"><input type="text" id='gggInput' name="target" size="33"></td>
<td bgcolor="#f7efdf"><button type="button" class="btn btn-primary btn-xs" onClick='document.addfrm.submit();'>Add</button></td>
<input type="hidden" name="act" value="add">
</tr>
</table>
</form>
</td>
</tr>
</table>

<br>
<br>
<fieldset>
  <legend style="font-size:14px;color:#808080;">ИНСТРУКЦИИ</legend>
  <p style="font-size:13px;color:#808080;"><img src="<?=SITE_URL;?>/theme/img/ico_swon.gif" align="absmiddle" border="0"> <b>IP</b> - Можно вводить как полностью IP так и только подсеть, можно несколько, разделяя их через | (пример, 85.18.98.155|86.17.98.|120.4.98.4|195.74.60.0-195.74.61.255)</p>
  <p style="font-size:13px;color:#808080;"><img src="<?=SITE_URL;?>/theme/img/ico_swon.gif" align="absmiddle" border="0"> <b>ДОМЕН</b> - Можно вводить как полностью URL так и только имя домена, можно несколько, разделяя их через | (пример, domen.com|domen.net|domen.ru)</p>
  <p style="font-size:13px;color:#808080;"><img src="<?=SITE_URL;?>/theme/img/ico_swon.gif" align="absmiddle" border="0"> <b>ГЕО</b> - Можно вводить как один гео код так и несколько, разделяя их через |, при вводе юзаем заглавные буквы (пример, RU|KZ|KG)</p>
  <p style="font-size:13px;color:#808080;"><img src="<?=SITE_URL;?>/theme/img/ico_swon.gif" align="absmiddle" border="0"> <b>Параметры</b> - Можно вводить как одино слово или параметр так и несколько, разделяя их через | (пример, weight-loss|2653)</p>
</fieldset>
<br>
</div>