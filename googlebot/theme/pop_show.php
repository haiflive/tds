<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=SCRIPT_NAME;?> v<?=SCRIPT_VERSION;?></title>
    <link rel="icon" href="<?=SITE_URL;?>favicon.ico">

    <!-- Bootstrap -->
    <link href="<?=SITE_URL;?>/theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=SITE_URL;?>/theme/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="<?=SITE_URL;?>/theme/css/datepicker3.css" rel="stylesheet">
    <style>
    .col-md-6 { width:100% }
    tr.hl:hover td.content,
    tr.hl:hover td.content2,
    table.hl tr:hover td.content,
    table.hl tr:hover td.content2 {
    background-color:#f7efdf;
    }
    #gggInput {
	width:100%; /* вот незадача, FF не хочет задавать ширину, для этого зададим size */
	left:0;
	top:0;
    }
    #logInput {
	width:100%; /* вот незадача, FF не хочет задавать ширину, для этого зададим size */
	height:100%; /* вот незадача, FF не хочет задавать ширину, для этого зададим size */
	left:0;
	top:0;
    }
    tr.hl:hover td.content,
    tr.hl:hover td.content2,
    table.hl tr:hover td.content,
    table.hl tr:hover td.content2 {
    background-color:	#e6ecf8;
    }
    .textbox {
    color: #000000;
    background-color: #e7e7e7;
    border: 0;
    text-align: left;
    width: 100%;
    height: 80%;
    }
    .but { 
    color: #000000; 
    background-color: #e7e7e7;
    width: 100%;
    border: 0;
    }
    .textbox { 
    color: #000000; 
    background-color: #e7e7e7;
    border: 0;
    text-align: left;
    width: 100%;
    height: 80%;
    }
    .but { 
    color: #000000; 
    background-color: #e7e7e7;
    width: 100%;
    border: 0;
    }
    .save {
    background: url(<?=SITE_URL;?>/theme/img/save.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .saveall {
    background: url(<?=SITE_URL;?>/theme/img/save_all.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .save {
    background: url(<?=SITE_URL;?>/theme/img/save.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .saveall {
    background: url(<?=SITE_URL;?>/theme/img/save_all.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .input-group-addon { font-size:12px; }
    .row { font-size:12px; }
    td { font-size:12px; }
    th { font-size:12px;text-transform: uppercase; }
    .well_ {
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    }
    .well_ {
    background-image: -webkit-linear-gradient(top, #e8e8e8 0%, #e8e8e8 100%);
    background-image: -o-linear-gradient(top, #e8e8e8 0%, #e8e8e8 100%);
    background-image: linear-gradient(to bottom, #e8e8e8 0%, #e8e8e8 100%);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffe8e8e8', endColorstr='#ffe8e8e8', GradientType=0);
    border-color: #dcdcdc;
    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.05), 0 1px 0 rgba(255, 255, 255, 0.1);
    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.05), 0 1px 0 rgba(255, 255, 255, 0.1);
    }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=SITE_URL;?>/theme/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript">
    function his_del(id,varname) {
    if(confirm('Вы действительно хотите удалить эту историю?')){
    $(".his_"+id).attr('src','<?=SITE_URL;?>/theme/img/ico_swon.gif');
    $.post('<?=SITE_URL;?>/pop_show.php', { action: "save_del", ids:  id, name:  varname },
    function(data) { if (data) {
    $(".his_"+id).attr('src','<?=SITE_URL;?>/theme/img/ico_swof.gif');
    }}
    );
    }
    }
    </script>
  </head>
 <body>

<div class="modal fade" id="myModal1000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">
      <div class="jumbotron" style="padding:25px;margin-bottom:0px;text-align:center;"><div class="row">Идет загрузка ...</div></div>
      </div>
      </div>
</div>

 <div class="container">
      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header" style="width: 100%;">
            <a target="_parent" class="navbar-brand"><os-p key="7adea0d9c77aabccd8bb67ae0a832d59">ID: <?php $ccexp = explode('-',$file); ?> <?=(!empty($_GET["pcid"]) ? $_GET["pcid"] : ($showcash == 1 ? $ccexp[0] : 'Empty!'));?></os-p></a>
            <div style="clear:both;"></div>
            <?php
            if (empty($showcash) || $showcash != 1) {
            $rrrarray = array(); $his = 0;
     	 foreach ($search_ as $akey_ => $_search_) { $his ++;
     	 $_search_exp = explode('-',$_search_);
     	 if ($pcid == trim($_search_exp[0])) {
     	 $rrrarray[] = '<div style="font-size:12px;color:#808080;padding-left:15px;"><a href="'.SITE_URL.'/pop_show.php?showcash=1&file='.$_search_.'">Есть история от: '.str_replace('_','-',$_search_exp[1]).' и до: '.str_replace('_','-',str_replace('.txt','',$_search_exp[2])).'</a> - <span style="color:#AAAAAA;">размер '.formatSizeUnits(filesize(LOGS."id_cashe/".$_search_)).', добавлен: '.date("d.m.Y H:i:s",$akey_).'</span> - <img align="absmiddle" src="'.SITE_URL.'/theme/img/b_cancel.gif" style="cursor:pointer;" onclick="his_del('.$his.',\''.$_search_.'\')" class="his_'.$his.'"></div><div style="padding:3px;"></div>';
     	 }
     	 }
     	 if (!empty($rrrarray)) {
     	 echo '<div style="height:'.(count($rrrarray) > 1 ? '53px' : '30px').';overflow:auto;">';
     	 echo implode("<div style=\"padding:1px;\"></div>\n",$rrrarray);
     	 echo '</div><div style="padding:3px;"></div>'; 
     	 }
            }
            ?>
            <p style="font-size:11px;color:#808080;padding-left:15px;">Максимально можно отобразить данные за последные <?=CLERT_TABLR_DEY;?> дней.</p>
          </div>
      </div>
      </div>
      <div class="jumbotron" style="padding:1px;margin-bottom:5px;">
      
    	<div class="row">

    	<div class="col-md-6"><div class="well" style="padding:10px;">

    <?php if (!empty($_stat_array)) {?>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Action</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
          <th>Всего (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Action</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    
        	</div></div>
    	<div style="clear:both;"></div>
    
    	</div>
    	</div>
    </div> <!-- /container -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=SITE_URL;?>/theme/js/bootstrap.min.js"></script>
  </body>
</html>