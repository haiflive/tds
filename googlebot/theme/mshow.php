      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      
      <form class="form-inline" role="form" method="get" name="form">
      <input type="hidden" name="datesearch" value="1">
      <?=(isset($_GET['showpage']) ? '<input type="hidden" name="showpage" value="'.$_GET['showpage'].'">' : '');?>
      <div class="form-group">
      <div id="sandbox-container">
    	<div class="input-daterange input-group" id="datepicker" data-date="<?=$date;?>" data-date-format="d-m-yyyy">
      <span class="input-group-addon" style="border-left:1px solid #CCCCCC;">Начало</span>
      <input type="text" class="input-sm form-control" name="start_date" value="<?=str_replace('_','-',$start_date);?>" />
      <span class="input-group-addon">Конец</span>
      <input type="text" class="input-sm form-control" name="date" value="<?=str_replace('_','-',$date);?>" />
      </div>
      </div>
      </div>
      <button type="button" class="btn btn-primary btn-sm" onClick='document.form.submit();'>Применить</button>
      <a style="float:right;" class="btn btn-default btn-sm" data-toggle="modal" href="<?=SITE_URL;?>/<?=ADMIN_FILE_NAME;?>?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showalldeystat=1&start_date=<?=$start_date;?>&date=<?=$date;?>" data-target="#myModal100">Обзор общей статистики по дням</a>
      </form>
      
      <div class="modal fade" id="myModal100" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">
      <div class="jumbotron" style="padding:25px;margin-bottom:0px;text-align:center;"><div class="row">Идет загрузка ...</div></div>
      </div>
      </div>
      </div>
      
      <div class="modal fade" id="myModal1000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">
      <div class="jumbotron" style="padding:25px;margin-bottom:0px;text-align:center;"><div class="row">Идет загрузка ...</div></div>
      </div>
      </div>
      </div>
      
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      
    	<div class="row">

    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php
    if (isset($_GET['showpage']) && $_GET['showpage'] == 'logs') {
    if (!empty($_logs_stat_array)) {
    ?>
    <table class="table table-striped table-condensed">
      <thead>
        <tr>
         <th>Время</th>
         <th>IP</th>
         <th>Лендинг</th>
         <th>Рефер</th>
         <th>Домен</th>
         <th>Гео</th>
         <th>ОС</th>
         <th>Агент</th>
         <th>Девайс</th>
         <th>Параметр</th>
         <th>Прокс</th>
         <th>EsdID</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$_logs_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Логи</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    <?php
    } else {
    $get_array = array(
    	'os'=>'OS',
    	'agent'=>'Браузеры',
    	'devices'=>'Устройства',
    	'geo'=>'Страны',
    	'esdid'=>'ESDID',
    	'target'=>'Лендинг (Куда отправился)',
    	'referer'=>'Рефер (Откуда пришел)',
    	'domain'=>'Домен сервера ЕСД',
    	'ip'=>'IP адреса посетителей',
    	'cookie'=>'Параметры (Передается к есд через cookie и get)'
    );
    if (!empty($_stat_array)) {?>
    <table class="table table-hover">
      <thead>
        <tr>
          <th><?=$get_array[trim($_GET["showpage"])];?></th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
          <th>Всего (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>'.$get_array[trim($_GET["showpage"])].'</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	<?php } ?>
    
    	</div>
    	<?php
          // Вывод списка страниц
          if ($num_pages > 1) {
          echo '<ul class="pagination pagination-sm">';
          for ($page = 1; $page <= $num_pages; $page++) {
          if ($page == $current_page) { echo '<li class="active"><a href="#">'.$page.' <span class="sr-only">(current)</span></a></li>'; }
          else { echo '<li><a href="'.SITE_URL.'/mshow.php?page='.$page.'&'.(isset($_GET['datesearch']) ? 'datesearch=1&' : '').''.(isset($_GET['showpage']) ? 'showpage='.$_GET['showpage'].'&' : '').'start_date='.$start_date.'&date='.$date.'">'.$page.'</a></li>'; }
          }
          echo '</ul>';
          }
      ?>
    	</div>