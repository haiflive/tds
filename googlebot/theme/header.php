<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=SCRIPT_NAME;?> v<?=SCRIPT_VERSION;?></title>

    <!-- Bootstrap -->
    <link href="<?=SITE_URL;?>/theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=SITE_URL;?>/theme/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="<?=SITE_URL;?>/theme/css/datepicker3.css" rel="stylesheet">
    <link href="<?=SITE_URL;?>/theme/js/source/jquery.fancybox.css?v=2.1.4" rel="stylesheet" type="text/css" media="screen" />
    
    <style>
    .col-md-6 { width:100% }
    tr.hl:hover td.content,
    tr.hl:hover td.content2,
    table.hl tr:hover td.content,
    table.hl tr:hover td.content2 {
    background-color:#f7efdf;
    }
    #gggInput {
	width:100%; /* вот незадача, FF не хочет задавать ширину, для этого зададим size */
	left:0;
	top:0;
    }
    #logInput {
	width:100%; /* вот незадача, FF не хочет задавать ширину, для этого зададим size */
	height:100%; /* вот незадача, FF не хочет задавать ширину, для этого зададим size */
	left:0;
	top:0;
    }
    tr.hl:hover td.content,
    tr.hl:hover td.content2,
    table.hl tr:hover td.content,
    table.hl tr:hover td.content2 {
    background-color:	#e6ecf8;
    }
    .textbox {
    color: #000000;
    background-color: #e7e7e7;
    border: 0;
    text-align: left;
    width: 100%;
    height: 80%;
    }
    .but { 
    color: #000000; 
    background-color: #e7e7e7;
    width: 100%;
    border: 0;
    }
    .textbox { 
    color: #000000; 
    background-color: #e7e7e7;
    border: 0;
    text-align: left;
    width: 100%;
    height: 80%;
    }
    .but { 
    color: #000000; 
    background-color: #e7e7e7;
    width: 100%;
    border: 0;
    }
    .save {
    background: url(<?=SITE_URL;?>/theme/img/save.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .saveall {
    background: url(<?=SITE_URL;?>/theme/img/save_all.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .save {
    background: url(<?=SITE_URL;?>/theme/img/save.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .saveall {
    background: url(<?=SITE_URL;?>/theme/img/save_all.png) repeat-x;
    height: 16px;
    width: 16px;
    border: 0px;
    }
    .input-group-addon { font-size:12px; }
    .row { font-size:12px; }
    td { font-size:12px; }
    th { font-size:12px;text-transform: uppercase; }
    .well_ {
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.05);
    }
    .well_ {
    background-image: -webkit-linear-gradient(top, #e8e8e8 0%, #e8e8e8 100%);
    background-image: -o-linear-gradient(top, #e8e8e8 0%, #e8e8e8 100%);
    background-image: linear-gradient(to bottom, #e8e8e8 0%, #e8e8e8 100%);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffe8e8e8', endColorstr='#ffe8e8e8', GradientType=0);
    border-color: #dcdcdc;
    -webkit-box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.05), 0 1px 0 rgba(255, 255, 255, 0.1);
    box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.05), 0 1px 0 rgba(255, 255, 255, 0.1);
    }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=SITE_URL;?>/theme/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?=SITE_URL;?>/theme/js/source/jquery.fancybox.js?v=2.1.4"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    $(".various_p").fancybox({
		maxWidth	: 670,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		preload		: true,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'elastic',
		openSpeed	: 'normal',
		closeSpeed	: 'fast'
    });
    $(".various_m").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '80%',
		height		: '40%',
		preload		: true,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'elastic',
		openSpeed	: 'normal',
		closeSpeed	: 'fast'
    });
    $(".various_lg").fancybox({
		maxWidth	: 1000,
		maxHeight	: 800,
		fitToView	: false,
		width		: '97%',
		height		: '60%',
		preload		: true,
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'fade',
		closeEffect	: 'elastic',
		openSpeed	: 'normal',
		closeSpeed	: 'fast'
    });
    });
    </script>
  </head>
 <body>
 <div class="container">
      <!-- Static navbar -->
      <div class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only" data-replace-tmp-key="6c7195551da0802a39b5e2bc7187df54"><os-p key="6c7195551da0802a39b5e2bc7187df54">Включить навигацию</os-p></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a target="_parent" class="navbar-brand" href="<?=SITE_URL;?>/<?=ADMIN_FILE_NAME;?>" data-replace-tmp-key="7adea0d9c77aabccd8bb67ae0a832d59"><os-p key="7adea0d9c77aabccd8bb67ae0a832d59"><?=SCRIPT_NAME;?> v<?=SCRIPT_VERSION;?></os-p></a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li<?=(empty($file_name) || $file_name == ADMIN_FILE_NAME ? ' class="active"' :'');?>><a target="_parent" href="<?=SITE_URL;?>/<?=ADMIN_FILE_NAME;?>?start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="97e7c9a7d06eac006a28bf05467fcc8b"><os-p key="97e7c9a7d06eac006a28bf05467fcc8b">Статистика</os-p></a></li>
              <li<?=($file_name == 'mshow.php' ? ' class="active dropdown"' :' class="dropdown"');?>>
                <a target="_parent" href="#" class="dropdown-toggle" data-toggle="dropdown" data-replace-tmp-key="6be2458a7786d2dfdb6b72d7583e8104"><os-p key="6be2458a7786d2dfdb6b72d7583e8104">Подробная статистика <span class="caret"></span></os-p></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=ip&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="004bf6c9a40003140292e97330236c53"><os-p key="004bf6c9a40003140292e97330236c53">IP</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=target&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="c396653502927eea9f0fc260a1465518"><os-p key="c396653502927eea9f0fc260a1465518">Лендинг</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=referer&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="71e7f655e2bbd0d97e8c376a4ed5af03"><os-p key="71e7f655e2bbd0d97e8c376a4ed5af03">Реферер</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=domain&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="112cd6710bd31b37dd39719c2d37191e"><os-p key="112cd6710bd31b37dd39719c2d37191e">Домен</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=geo&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="112cd6710bd31b31dd32719c2d37191e"><os-p key="112cd6710bd31b31dd32719c2d37191e">Страны</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=os&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="0006e15256c2d7dba40f61bd99566c60"><os-p key="0006e15256c2d7dba40f61bd99566c60">ОС</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=agent&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="0006e15256c2d7d1a40f61bd9956ec60"><os-p key="0006e15256c2d7d1a40f61bd9956ec60">Браузеры</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=devices&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="0006e15256c2d7d1a40f61bd9956ec60"><os-p key="0006e15256c2d7d1a40f61bd9956ec60">Устройства</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=esdid&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="0006e15256c2d7dba40f68bd9956ec60"><os-p key="0006e15256c2d7dba40f68bd9956ec60">Esd id</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=cookie&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="0006e15256c2d7dba40f67bd9956ec60"><os-p key="0006e15256c2d7dba40f67bd9956ec60">Параметры</os-p></a></li>
                  <li class="divider"></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=logs&start_date=<?=$start_date;?>&date=<?=$date;?>" data-replace-tmp-key="0006e15256c2d7dba40f67bd9956ec60"><os-p key="0006e15256c2d7dba40f67bd9956ec60">Логи</os-p></a></li>
                </ul>
              </li>
              <li<?=($file_name == 'schemes.php' ? ' class="active"' :'');?>><a target="_parent" href="<?=SITE_URL;?>/schemes.php" data-replace-tmp-key="7a1920d61156abc05a60135aefe8bc67"><os-p key="7a1920d61156abc05a60135aefe8bc67">Схемы</os-p></a></li>
              <li<?=($file_name == 'gen_esd.php' ? ' class="active"' :'');?>><a target="_parent" href="<?=SITE_URL;?>/gen_esd.php" data-replace-tmp-key="7a1920d61176abc05a60135aefe8bc67"><os-p key="7a1920d61176abc05a60135aefe8bc67">Генерирование ЕСД</os-p></a></li>
              <!--<li<?=($file_name == 'macros.php' ? ' class="active"' :'');?>><a target="_parent" href="<?=SITE_URL;?>/macros.php" data-replace-tmp-key="7a1920d61156abc05a64135aefe8bc67"><os-p key="7a1920d61156abc05a64135aefe8bc67">Редактор макросов</os-p></a></li> -->
              <!--<li<?=($file_name == 'dects.php' ? ' class="active"' :'');?>><a target="_parent" href="<?=SITE_URL;?>/dects.php" data-replace-tmp-key="7a1920d61176abc05a60135aefe8bc67"><os-p key="7a1920d61176abc05a60135aefe8bc67">Авто ссылки</os-p></a></li> -->
              <li<?=($file_name == 'redirect.esd.php' || $file_name == 'domedit.php' || $file_name == 'auto_domedit.php' || $file_name == 'ajax.esd_table.php' || $file_name == 'ajax.esd.php' ? ' class="active"' :'');?>><a target="_parent" href="#" class="dropdown-toggle" data-toggle="dropdown" data-replace-tmp-key="7a1920d61156abc05a60135aefe8bc57"><os-p key="7a1920d61156abc05a60135aefe8bc57">Редакторы ЕСД <span class="caret"></span></os-p></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a target="_parent" href="<?=SITE_URL;?>/redirect.esd.php" data-replace-tmp-key="7a2920d61156abc05a60135a7fe8bc69"><os-p key="7a2920d61156abc05a60135a7fe8bc69">Редактирование ЕСД редирект доменов</os-p></a></li>
                  <li class="divider"></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/ajax.esd.php" data-replace-tmp-key="7a1920d61156abc05a60135a7fe8bc67"><os-p key="7a1920d61156abc05a60135a7fe8bc67">Редактирование ЕСД серверов</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/auto_domedit.php" data-replace-tmp-key="c396653502927eea9f0fc260a1465518"><os-p key="c396653502927eea9f0fc260a1465518">Групповое редактирование ЕСД</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/domedit.php" data-replace-tmp-key="004bf6c9a40003140292e97330236c53"><os-p key="004bf6c9a40003140292e97330236c53">Аналог</os-p></a></li>
                </ul>
              </li>
              <li<?=($file_name == 'dbedit.php' ? ' class="active"' :'');?>><a target="_parent" href="<?=SITE_URL;?>/dbedit.php" data-replace-tmp-key="6729423b836250c914cb106fcb9cf68f"><os-p key="6729423b836250c914cb106fcb9cf68f">Чистка базы</os-p></a></li>
              <li<?=($file_name == 'services.php' || $file_name == 'bots.php' ? ' class="active"' :'');?>><a target="_parent" href="#" class="dropdown-toggle" data-toggle="dropdown" data-replace-tmp-key="7a1920d61156abc05a60135aefe8bc51"><os-p key="7a1920d61156abc05a60135aefe8bc51">Сервис <span class="caret"></span></os-p></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a target="_parent" href="<?=SITE_URL;?>/services.php" data-replace-tmp-key="7a1920d61156abc05a60135a7fe8bc67"><os-p key="7a1920d61156abc05a60135a7fe8bc67">Обслуживание</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/utilite.php" data-replace-tmp-key="c396653502927eea9f0fc260a1465518"><os-p key="c396653502927eea9f0fc260a1465518">Парсер DOM,MX,ISP</os-p></a></li>
                  <li><a target="_parent" href="<?=SITE_URL;?>/bots.php" data-replace-tmp-key="c396653502927eea9f0fc260a1465518"><os-p key="c396653502927eea9f0fc260a1465518">Ловец ботов</os-p></a></li>
                </ul>
              </li>
              <li<?=($file_name == 'exit.php' ? ' class="active"' :'');?>><a target="_parent" href="<?=SITE_URL;?>/exit.php"  title="Выход" data-replace-tmp-key="7a0f3895a832d63a15567b9a96f22931"><os-p key="7a0f3895a832d63a15567b9a96f22931"><img src="<?=SITE_URL;?>/theme/img/logout-16.png" alt="Выход"></os-p></a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </div>