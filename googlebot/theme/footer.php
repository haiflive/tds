    </div> <!-- /container -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=SITE_URL;?>/theme/js/bootstrap.min.js"></script>
    <script src="<?=SITE_URL;?>/theme/js/bootstrap-datepicker.js"></script>
    <script src="<?=SITE_URL;?>/theme/js/locales/bootstrap-datepicker.ru.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('#sandbox-container .input-daterange').datepicker({
    todayBtn: "linked",
    format: "d-m-yyyy",
    language: "ru",
    todayHighlight: true
    });
    });
    </script>
  </body>
</html>