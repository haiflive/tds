<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
	
	<form class="form-inline" role="form" method="post" name="form">
      <input type="hidden" name="get_mx_isp" value="1">
      <textarea style="width: 99%; height: 200px;" spellcheck="false" name="domain"><?=(!empty($_POST['domain']) ? $_POST['domain'] : '');?></textarea><br>
      <button type="button" class="btn btn-primary btn-sm" onClick='document.form.submit();'>Поиск MX по домену</button>
    </form>
    
</div>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
	
    <?php if (!empty($domain_array)) { ?>
    <textarea style="width: 95%; height: 200px;" spellcheck="false"><?=implode("\n",$domain_array);?></textarea>
    <?php } else { echo '<strong>Нет данных по MX</strong>'; }?>
</div>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
	
	<form class="form-inline" role="form2" method="post" name="form2">
      <input type="hidden" name="get_mx_isp" value="1">
      <textarea style="width: 99%; height: 200px;" spellcheck="false" name="smtp"><?=(!empty($_POST['smtp']) ? $_POST['smtp'] : '');?></textarea><br>
      <button type="button" class="btn btn-primary btn-sm" onClick='document.form2.submit();'>Поиск ISP по домену или SMTP серверу</button>
    </form>
    
</div>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
	
    <?php if (!empty($smtp_array)) { ?>
    <textarea style="width: 95%; height: 200px;" spellcheck="false"><?=implode("\n",$smtp_array);?></textarea>
    <?php } else { echo '<strong>Нет данных по ISP</strong>'; }?>
</div>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      Серверное время: <?php echo date("d.m.Y H:i:s"); ?>
</div>
<? //print_r(timezone_abbreviations_list()); ?> 