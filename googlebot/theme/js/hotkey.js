// ������� �������, ���� hotkeys.js
// ��� ������ ��������� JQuery. �����: ���� ����� ������

$(function(){
	// ������ ���������� ��� ������ ������, ������ ����������� ������������� ������:
	var keyCodes = {D:68, E:69, F:70, M:77, N:78, O:79, U:85, Esc:27, "/":220,
		"0":48, "1":49,  "2":50, "3":51, "4":52, "5":53, "6":54, "7":55, "8":56, "9":57,
		Left:37, Up:38, Right:39, Down:40, Enter:13, Ctrl:17, Alt:18, Space:32
		// ����� ��������, ��� �� ��� ���� ������
	};
	
	// ��������� ���������� ��������� (��������� �������� ��� �������� � ��������)
	var prompt = $("<div class='hotprompt'>-</div>");
	prompt.css("position", "absolute");
	prompt.css("padding", "1px 3px");
	prompt.css("font-size", "8px");
	prompt.css("background-color", "orange");
	prompt.css("color", "black");
	prompt.css("opacity", "0.8"); // ������ ���������������� �� ��������
	prompt.css("border", "1px solid black");
	// �������� � ������ �������, ������� ���� ��� ������:
	prompt.css("border-radius", "7px 7px 0px 7px"); 
	prompt.css("-moz-border-radius", "7px 7px 0px 7px");
	
	// �������� ��������� � ��������
	var showHotPrompts = function(){
		if($(".hotprompt").length > 0) return ;
		// ��� ������� ������ ��� ������, � ��������� hotkey
		$("a[hotkey], input[hotkey]").each(function(a){
			p = prompt.clone(); // ��������� ���������, ������� �� �������
			p.html($(this).attr("hotkey")); //�������� � ��� ������ "Ctrl + .."
			p.insertAfter($(this)); //��������� ��������� ����� ����� ����� ������
			// ��������� � ���, ����� ������ ������ �������� �� ����� ������� ���� ������:
			p.css("left", $(this).position().left - p.width());
			p.css("top", $(this).position().top - p.height());
		});
	}
	
	// ������ ��� ���������
	var hideHotPrompts = function(){
		// ����� �� ������ ��������:
		$("a[hotkey], input[hotkey]").each(function(a){
			$(".hotprompt").remove();
		});
	}
    
    // ������ �������, ����������� ������ �� ������� ������������� ��������
	var in_array = function(needle, haystack){
		for (key in haystack)
			if (haystack[key] == needle) return true;
		return false;
	}
	
	
	// ���� ������ ������� �� ��������
	$("html").keydown(function(e){
		var lastGood = false;
		// �� ���������� ��� ������ � ������, � ������� ���� ������� hotkey
		$("a[hotkey], input[hotkey]").each(function(a){
			var hotkey = $(this).attr("hotkey"); // �������� �������� (�������� Ctrl + E)
			var words = hotkey.split("+"); //��������� �������� ������
			// ��������� ������� � ���� ������� - ��� ���� �������:
			var key = words.pop().replace(/\s/,""); // ����������� � � �������� ��� �������
			var syskeys = new Array();
			// ���������� � ������� ������� ��������� ���������� (Ctrl, Alt, Shift)
			for(var i in words) syskeys.push(words[i].replace(/\s+/g,""));
			if(keyCodes[key] != e.keyCode) return; //��� ������� �� ������� - �����
			if(in_array('Ctrl', syskeys)    && !e.ctrlKey) return; //Ctrl �� ������� - �����
			if(in_array('Alt', syskeys)     && !e.altKey) return;  //Alt �� ������� - �����
			if(in_array('Shift', syskeys)   && !e.shiftKey) return;//Shift �� ������� - �����
			//���� �� �������� ��������� ���������� ������, �� ��������� ������ ���������:
			lastGood = $(this); //���������� ������ �������� ��������� ���������� ��������
		});
		//���� ���������� ��� ������� ���������� ������ (��� �����) ���� �������:
		if(lastGood){ 
			// �� ���� ��� �����, �� ��������:
			if(lastGood.attr("type") == 'submit')
				$(lastGood.context.form).submit();
			else{ // � ���� ������, �� �������
				var href = lastGood.attr("href");
				lastGood.click();
			}
			return false; // � ��������� ��������� �������� ��������
		}
		
		// � ��� ��� ������ �������:
		// ���� ������ ������� CTRL - �� ���������� ��������� ����� ������
		if(e.keyCode == keyCodes.Ctrl){ 
			showHotMap();
			showHotPrompts(); //� ���������
		}
	});
	
	// �� ���������� ����� ������� - �������� ��������� � ����� ������
	$("html").keyup(function(e){
		if(e.keyCode == keyCodes.Ctrl){ 
			hideHotPrompts();
			hideHotMap();
		}
	});

    // ���� ����-������ �������� - �������� ��� ��������� � ����� ������
    $("html").click(function(e){
        hideHotPrompts();
        hideHotMap();
    });
    
    // �������� ��������� ����� ������ (��� ����� ������� ������� �� ������� ������)
    var showHotMap = function(){
        if($(".hotsitemap").length > 0) return ;
        // ������� ���������� �������
        var hotmap = $("<div>");
        $("body").append(hotmap);
        hotmap.addClass("hotsitemap"); 
        hotmap.css('background-color', 'orange');
        hotmap.css('position', 'fixed'); //�� ����� �� �������� �� ���������
        hotmap.css('color', 'black');
        hotmap.css('top', '200px');      //������ ������ �� �������� ��������
        hotmap.css('padding', '20px');
        hotmap.css("border-radius", "10px"); //������ ��������, ��� � �����
        hotmap.css("-moz-border-radius", "10px");//���� � �������
        hotmap.append("<h3>������� �������</h3>");
        // � ��������� ��� ������ ��������� � ����������� �� ������
        $("a[hotkey]").each(function(){
            var hotkey = $(this).attr("hotkey");
            var value = $(this).html(); // ����� ������ <a href=...>��� ����</a>
            var title = $(this).attr("title"); // <a title='��� ����'>...
            // ������ �� ���� ����� ���, ��� �������� (������ ����� �� ����� �������������)
            var display_text = value.length > title.length ? value : title;
            // � ���������� ���� ����� � ������� � �������:
            hotmap.append("<b>"+hotkey+"</b> "+display_text+"<br />"); 
        });
    }
    
    // ������ ��� �����
    var hideHotMap = function(){
        $(".hotsitemap").remove();
    }
});
