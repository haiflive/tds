Colors = function (objectId, inputTextId, styleColor, pallete)
// �����������
{
	
	this._object = document.getElementById(objectId);
	this._objectId = objectId;
	this.inputText = document.getElementById(inputTextId);
	this.inputTextId = inputTextId;
	this.styleColor = styleColor;
	eval('this.oldColor = Colors.rgb2hex(this._object.style.'+styleColor+')');
	this.inputText.value = this.oldColor;
	
	this.pallete = pallete;
	
}

Colors.prototype.show = function ()
// ������ �������
{
	
	var table = '<span id="allcolorselementsselect'+this.inputTextId+'" onclick="document.getElementById(\'allcolorselements'+this.inputTextId+'\').style.display=\'block\'; document.getElementById(\'allcolorselementslink'+this.inputTextId+'\').focus();" style="cursor: pointer;"><img src="'+this.outImage+'" title="Colour" style="position:relative; top:2px;" onmouseover="this.src=\''+this.overImage+'\'" onmouseout="this.src=\''+this.outImage+'\'" /></span>';
	table += '<input id="setc'+this.inputTextId+'" type="hidden" value="0" />';
	table += '<input id="oldColor'+this.inputTextId+'" type="hidden" value="#'+this.oldColor+'" />';
	table += '<a id="allcolorselementslink'+this.inputTextId+'" href="#" onclick="return false;" onblur="document.getElementById(\'allcolorselements'+this.inputTextId+'\').style.display=\'none\'; if (document.getElementById(\'setc'+this.inputTextId+'\').value!=\'1\') Colors.changecolor(document.getElementById(\'oldColor'+this.inputTextId+'\').value, \''+this._objectId+'\', \''+this.inputTextId+'\', \''+this.styleColor+'\'); document.getElementById(\'setc'+this.inputTextId+'\').value=\'0\'; ">';
	table += '<div id="allcolorselements'+this.inputTextId+'" style="width: '+this.allwidth+'px; cursor: pointer; display: none; position: absolute; z-index:1000; background-color:#bbb; border:solid 1px #bbb;">';
	
	for (var i=1, c=0; i<=this.rows; i++)
	{
		//table += '<tr height="20">';
		for (var j=1; j<=this.cols; j++)
		{
			table += '<div style="background:'+this.pallete[c++]+'; float:left; width:'+this.width+'px; height:'+this.width+'px; margin:'+this.border+'px; border:solid '+this.border+'px #000;" onmousedown="document.getElementById(\'setc'+this.inputTextId+'\').value=\'1\'; document.getElementById(\'oldColor'+this.inputTextId+'\').value=this.style.backgroundColor;" onmousemove="Colors.changecolor(this.style.backgroundColor, \''+this._objectId+'\', \''+this.inputTextId+'\', \''+this.styleColor+'\');" onmouseover="this.style.border=\'solid '+this.border+'px #ffff00\'" onmouseout="this.style.border=\'solid '+this.border+'px #000\';"></div>';
		}
		table += '<div style="clear:both;"></div>';
	}
	table += '</div></a>';
	
	document.writeln(table);
}


Colors.rgb2hex = function (rgb) {
// ��������������� ������� �������������� �� ������� rgb(0,0,0) � hex

  if (!rgb)
  {
    return '0000000';
  }
  if (rgb.substring(0,1)=="#")
  {
    return rgb.substring(1);
  }
  else
  {
	  var s,i,h='', x='0123456789abcdef';
	  var c = rgb.substring(4);
	  c = c.substring(0, c.length-1);
	  
	
	  if(c){
	    s=c.split(',');
	    for (i=0; i < 3; i++){
	      n  = parseInt(s[i]);
	      h += x.charAt(n>>4) + x.charAt(n&15);
	    }
	  return h;
	  }
  }
}

Colors.changecolor = function (thiscolor, _object, inputText, styleColor)
{
	eval('document.getElementById(_object).style.'+styleColor+' = thiscolor');
	document.getElementById(inputText).value = Colors.rgb2hex(thiscolor);
}

Colors.setup = function (params) {
	function param_default(pname, def) { if (typeof params[pname] == "undefined") { params[pname] = def; } };
	param_default("rows",			5);
	param_default("cols",			6);
	param_default("width",			20);
	param_default("border",			1);
	param_default("styleColor",		"color");
	param_default("inputTextId",	null);
	param_default("objectId",		null);
	param_default("pallete",        new Array (
	'#FFFFCC',	'#FFFF66',	'#FFCC66',	'#F2984C',	'#E1771E',	'#B47B10',
	'#C3D9FF',	'#99C9FF',	'#66B5FF',	'#3D81EE',	'#0066CC',	'#6C82B5',
	'#CAF99B',	'#80FF00',	'#00FF80',	'#78B749',	'#2BA94F',	'#38B63C',
	'#FFBBE8',	'#E895CC',	'#FF6FCF',	'#C94093',	'#9D1961',	'#800040',
	'#FFFFFF',	'#E6E6E6',	'#CCCCCC',	'#B3B3B3',	'#999999',	'#808080'
	));
	param_default("outImage",		"rgb.gif");
	param_default("overImage", 		"on_rgb.gif");
	
	
	var col = new Colors(params.objectId, params.inputTextId, params.styleColor, params.pallete);

	col.rows = params.rows;
	col.cols = params.cols;
	col.width = params.width;
	col.border = params.border;
	col.allwidth = col.cols*(col.width+col.border*4)+col.border;
	col.outImage = params.outImage
	col.overImage = params.overImage

	col.show();
	
	return col;
}