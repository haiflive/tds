<form action="<?=$_SERVER['SCRIPT_NAME'];?>" method="post" name="form" id="mainform">
<input type="hidden" name="action" value="send">

<?php if (!empty($error)) { ?>
<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
<?=$error;?>
</div>
<?php } ?>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
    <table style="width: 100%;
		max-width: 100%;
		margin-bottom: 5px;
		background-color: transparent;
		border-collapse: collapse;
		border-spacing: 0px;">
        <tr>
          <th>Версия ESD</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;">
        <select name="esd_file_name" class="form-control">
        <option value="code5_test" <?=($esd_file_name == 'code5_test' ? 'selected="selected"' : '');?>>Обычная с Header Redirect (схема run://code5_test)</option>
        <option value="code6_test" <?=($esd_file_name == 'code6_test' ? 'selected="selected"' : '');?>>Версия с JS Redirect (схема run://code6_test)</option>
        </select></td></tr>
        <tr>
          <th>Вставьте последный ESD ID (вставьте NONE если надо отключить интелект. режим)</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><input name="last_esdid" type="text" class="form-control" value="<?php echo $last_esdid; ?>"></td></tr>
        <tr>
          <th>Автоподгрузка доменов от Dom</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><input name="data_auto_dom" type="text" class="form-control" placeholder="Вставьте ссылку автоподгрузки доменов в формате: http://vertonlogin.com/gh747h75m4gT9/domains.txt" value="<?php echo $data_auto_dom; ?>"></td></tr>
        <tr>
          <th>Домены от Dom</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><textarea spellcheck="false" name="data_dom" class="form-control" style="height: 150px;" placeholder="Вставьте ссылки в формате: http://com-xv22.net/space.php?a=277016&c=wl_con"><?php echo $data_dom; ?></textarea></td></tr>
        <tr>
          <th>Автоподгрузка доменов от Bro (genry,tommy)</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><input name="data_auto_bro" type="text" class="form-control" placeholder="Вставьте ссылку автоподгрузки доменов в формате: http://85.143.216.229/lps/go.php?a_aid=55dd455cd0e9a&a_bid=28ef80f4" value="<?php echo $data_auto_bro; ?>"></td></tr>
        <tr>
          <th>Домены от Bro (genry,tommy)</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><textarea spellcheck="false" name="data_bro" class="form-control" style="height: 100px;" placeholder="Вставьте ссылки в формате: http://getmoneyhere.biz/?id=OK&af=9f033dde&ch=1001"><?php echo $data_bro; ?></textarea></td></tr>
        <tr>
          <th>Автоподгрузка доменов от Bro (sanny)</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><input name="data_auto_bro2" type="text" class="form-control" placeholder="Вставьте ссылку автоподгрузки доменов в формате: http://85.143.216.229/lps/go.php?a_aid=55dd455cd0e9a&a_bid=28ef80f4" value="<?php echo $data_auto_bro2; ?>"></td></tr>
        <tr>
          <th>Домены от Bro (sanny)</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><textarea spellcheck="false" name="data_bro2" class="form-control" style="height: 100px;" placeholder="Вставьте ссылки в формате: http://getmoneyhere.biz/?id=OK&af=9f033dde&ch=1001"><?php echo $data_bro2; ?></textarea></td></tr>
        <tr>
          <th>Автоподгрузка доменов от Tosser</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><input name="data_auto_toser" type="text" class="form-control" placeholder="Вставьте ссылку автоподгрузки доменов в формате: http://herba4u.net/land.php?id=b1025ebe8042ba49" value="<?php echo $data_auto_toser; ?>"></td></tr>
        <tr>
          <th>Домены от Tosser</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><textarea spellcheck="false" name="data_toser" class="form-control" style="height: 100px;" placeholder="Вставьте ссылки в формате: fatlossway-b.net"><?php echo $data_toser; ?></textarea></td></tr>
        
        <tr><td style="padding-top: 8px;"><input type="submit" name="Submit" value="Сохранить" class="btn btn-default btn-sm"></td></tr>
    </table>
    
</div>

</form>

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
Серверное время: <?php echo date("d.m.Y H:i:s"); ?><br><br>
<pre>Файл (Header Redirect): <?php if (file_exists(ROOT.'run/code5_test.php')) { echo ROOT.'run/code5_test.php <span style="color:green;">найден!</span> &rsaquo;'; } else { ROOT.'run/code5_test.php <span style="color:red;">не найден!</span> &rsaquo;'; } ?> дата последнего изменения: <?php echo date("d.m.Y H:i:s", filemtime(ROOT.'run/code5_test.php'))." &rsaquo;"; ?><a class="btn btn-xs various_lg" data-fancybox-type="iframe" href="<?=SITE_URL;?>/redirect.esd.php?view=code5_test" target="_blank">обзор файла</a>
Файл (JS Redirect): <?php if (file_exists(ROOT.'run/code6_test.php')) { echo ROOT.'run/code6_test.php <span style="color:green;">найден!</span> &rsaquo;'; } else { ROOT.'run/code6_test.php <span style="color:red;">не найден!</span> &rsaquo;'; } ?> дата последнего изменения: <?php echo date("d.m.Y H:i:s", filemtime(ROOT.'run/code6_test.php')); ?> &rsaquo;<a class="btn btn-xs various_lg" data-fancybox-type="iframe" href="<?=SITE_URL;?>/redirect.esd.php?view=code6_test" target="_blank">обзор файла</a></pre>

Домены от Dom<br><br>
<?php if (!empty(DATA_DOM_ARRAY)) { ?><pre><?php echo htmlSpecialChars(DATA_DOM_ARRAY); ?></pre><?php } ?>

Домены от Bro (genry)<br><br>
<?php if (!empty(DATA_BRO_ARRAY)) { ?><pre><?php echo htmlSpecialChars(DATA_BRO_ARRAY); ?></pre><?php } ?>

Домены от Bro (sanny)<br><br>
<?php if (!empty(DATA_BRO_ARRAY2)) { ?><pre><?php echo htmlSpecialChars(DATA_BRO_ARRAY2); ?></pre><?php } ?>

Домены от Tosser<br><br>
<?php if (!empty(DATA_TOSSER_ARRAY)) { ?><pre><?php echo htmlSpecialChars(DATA_TOSSER_ARRAY); ?></pre><?php } ?>
</div>

<form action="<?=$_SERVER['SCRIPT_NAME'];?>" method="post" name="form2" id="mainform2">
<input type="hidden" name="action" value="replace">

<div class="jumbotron" style="padding:18px;margin-bottom:20px;">
    <table style="width: 100%;
		max-width: 100%;
		margin-bottom: 5px;
		background-color: transparent;
		border-collapse: collapse;
		border-spacing: 0px;">
        <tr>
          <th>Файл версии ESD (обычная с Header Redirect)</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><textarea spellcheck="false" name="esd_header_redirect" class="form-control" style="height: 200px;" placeholder="Вставьте php код"><?php echo $esd_header_redirect; ?></textarea></td></tr>
        <tr>
          <th>Файл версии (обычная с JS Redirect)</th>
        </tr>
        <tr><td style="padding-top: 8px;padding-bottom: 8px;"><textarea spellcheck="false" name="esd_js_redirect" class="form-control" style="height: 200px;" placeholder="Вставьте php код"><?php echo $esd_js_redirect; ?></textarea></td></tr>
        <tr><td style="padding-top: 8px;"><input type="submit" name="Submit" value="Сохранить файл" class="btn btn-default btn-sm"></td></tr>
    </table>
</div>

</form>

<? //print_r(timezone_abbreviations_list()); ?>