      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      
      <p>Обновлять GeoIP раз в месяц.</p>
      <p style="font-size:14px;">Последнее обновление было: <b><?=(date("d-m-Y H:i:s",filemtime(LIBS.'geoip/GeoIP.dat')));?></b></p>
      <p style="font-size:14px;">Скачать базу можно тут: <a target="_blank" href="http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz">http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz</a></p>
      <p style="font-size:14px;">Больше информации: <a target="_blank" href="http://dev.maxmind.com/geoip/legacy/geolite/">MaxMind</a>, <a target="_blank" href="https://github.com/maxmind">MaxMind GitHub</a></p>
      <p style="font-size:14px;">Скачать базу, разпаковать архив и файл GeoIP.dat загрузить в директорию /lib/geoip/</p>
      
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <form name="update" method="post" action="services.php">
      <input type="hidden" name="update" value="ok">
      </form>
      <p>Обновлять базу юзер агентов желательно раз в 10 дней. <!-- <button type="button" class="btn btn-default btn-xs" onclick="document.update.submit();">Обновить автоматичесски</button></p> -->
      <?=$results;?>
      <p style="font-size:14px;">Последнее обновление было: <b><?=(date("d-m-Y H:i:s",filemtime(LOGS.'uascache/uasdata.ini')));?></b></p>
      <!-- <p style="font-size:14px;">Версия файла: <?php
        $cacheIni = array();
        if (file_exists(LOGS . 'uascache/uasdata.ini')) {
            $cacheIni = file_get_contents(LOGS . 'uascache/uasdata.ini');
            preg_match_all("/\; Version\: (.*); Checksum/is", $cacheIni, $vers);
            echo (!empty($vers[1][0]) ? '<b>'.trim($vers[1][0]).'</b> (<a target="_blank" href="http://user-agent-string.info/rpc/get_data.php?key=free&format=ini&ver=y">показать последную версию</a>)' : 'Версия не известна!');
        }
        else { echo "Файл ".LOGS . 'uascache/uasdata.ini'." не найден!"; }
      ?></p>
      <p style="font-size:14px;">Обновить в ручную, скачать: <a target="_blank" href="http://user-agent-string.info/rpc/get_data.php?key=free&format=ini">http://user-agent-string.info/rpc/get_data.php?key=free&format=ini</a></p>
      <p style="font-size:14px;">Скачать базу, переименовать в uasdata.ini и файл загрузить в директорию /logs/uascache/</p> -->
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Очистка MySQL таблиц старше <?=CLERT_TABLR_DEY;?> дней. <button type="button" class="btn btn-primary btn-xs" onclick="document.cleardb.submit();">Очистить</button></p>
      <form name="cleardb" method="post" action="services.php">
      <input type="hidden" name="cleardb" value="ok">
      <p style="font-size:14px;">Всего таблиц: <b><?=count($table_list);?></b> <?=(count($table_list) > 0 ? '(<a target="_blank" href="#" data-toggle="modal" data-target="#myModal1">показать</a>)' : '');?></p>
      <p style="font-size:14px;">Всего найдено таблиц для очитки: <b><?=count($table_name_array);?></b> <?=(count($table_name_array) > 0 ? '(<a target="_blank" href="#" data-toggle="modal" data-target="#myModal2">показать</a>)' : '');?></p>
      <p style="font-size:14px;">Размер MySQL DB: <b><?=$dbsize;?></b></p>
      </form>
      <!-- Modal -->
      <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">Список всех таблиц</h4>
            </div>
            <div class="modal-body">
            <?=d($table_list);?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">Список таблиц для очистки</h4>
            </div>
            <div class="modal-body">
            <?=d($table_name_array);?>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
	<!-- Modal -->
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Информация о MySQL базе ID.</p>
      <p style="font-size:14px;">Всего записей во временной таблице: <b><?=$id_temp;?></b></p>
      <p style="font-size:14px;">Размер временной таблицы MySQL DB: <b><?=$id_temp_dbsize;?></b></p>
      <br>
      <p style="font-size:14px;">Всего записей в таблице историй: <b><?=$id_results;?></b></p>
      <p style="font-size:14px;">Размер таблицы историй MySQL DB: <b><?=$id_results_dbsize;?></b></p>
      <p style="font-size:14px;">До следующего обновление истории осталось: <b><?=get_save_history_time();?></b></p>
      </div>
      
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Информация о файловом DB кеше.</p>
      <p style="font-size:14px;">Всего файлов: <b><?=count($cash_files);?></b></p>
      <p style="font-size:14px;">Размер всего кеша: <b><?=$cashsize;?></b></p>
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Информация о кеше линк чекера.</p>
      <p style="font-size:14px;">Всего файлов: <b><?=count($check_cash_files);?></b></p>
      <p style="font-size:14px;">Размер всего кеша: <b><?=$check_cashsize;?></b></p>
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Информация о кеше непринятого трафика.</p>
      <p style="font-size:14px;">Всего файлов: <b><?=count($break_cash_files);?></b></p>
      <p style="font-size:14px;">Размер всего кеша: <b><?=$break_cashsize;?></b></p>
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Информация о кеше трафика с заблоченых агентов.</p>
      <p style="font-size:14px;">Всего файлов: <b><?=count($bots_cash_files);?></b></p>
      <p style="font-size:14px;">Размер всего кеша: <b><?=$bots_cashsize;?></b></p>
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Информация о кеше принятого трафика.</p>
      <p style="font-size:14px;">Всего файлов: <b><?=count($accept_cash_files);?></b></p>
      <p style="font-size:14px;">Размер всего кеша: <b><?=$accept_cashsize;?></b></p>
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <p>Информация о кеше подозрительного трафика.</p>
      <p style="font-size:14px;">Всего файлов: <b><?=count($unknow_cash_files);?></b></p>
      <p style="font-size:14px;">Размер всего кеша: <b><?=$unknow_cashsize;?></b></p>
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      <form name="archive" method="post" action="services.php">
      <input type="hidden" name="archive" value="ok">
      <p>Архивировать проект: <button type="button" class="btn btn-primary btn-xs" onclick="document.archive.submit();">Создать дамп</button></p>
      <p style="font-size:14px;">Информация о найденном дампе: <?=$file_download;?></p>
      </form>
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      Серверное время: <?php echo date("d.m.Y H:i:s"); ?>
      </div>
      <? //print_r(timezone_abbreviations_list()); ?> 