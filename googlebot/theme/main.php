      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      
      <form class="form-inline" role="form" method="get" name="form">
      <input type="hidden" name="datesearch" value="1">
      <div class="form-group">
      <div id="sandbox-container">
    	<div class="input-daterange input-group" id="datepicker" data-date="<?=$date;?>" data-date-format="d-m-yyyy">
      <span class="input-group-addon" style="border-left:1px solid #CCCCCC;">Начало</span>
      <input type="text" class="input-sm form-control" name="start_date" value="<?=str_replace('_','-',$start_date);?>" />
      <span class="input-group-addon">Конец</span>
      <input type="text" class="input-sm form-control" name="date" value="<?=str_replace('_','-',$date);?>" />
      </div>
      </div>
      </div>
      <button type="button" class="btn btn-primary btn-sm" onClick='document.form.submit();'>Применить</button>
      <a style="float:right;margin-left:5px;" class="btn btn-default btn-sm" data-toggle="modal" href="<?=SITE_URL;?>/<?=ADMIN_FILE_NAME;?>?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showalldeystat=1&start_date=<?=$start_date;?>&date=<?=$date;?>" data-target="#myModal100">Обзор общей статистики по дням</a>
      <a style="float:right;" class="btn btn-default btn-sm various_m" data-fancybox-type="iframe" href="<?=SITE_URL;?>/<?=ADMIN_FILE_NAME;?>?showid=1" target="_blank">Статистика по ID</a>
      </form>
      
      <div class="modal fade" id="myModal100" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">
      <div class="jumbotron" style="padding:25px;margin-bottom:0px;text-align:center;"><div class="row">Идет загрузка ...</div></div>
      </div>
      </div>
      </div>
      
      </div>
      
      <div class="jumbotron" style="padding:18px;margin-bottom:20px;">
      
    	<div class="row">
      
      <div style="float:left;width:560px;">
      
    	<div class="col-md-6"><div class="well_" style="padding:10px;">
    	<p style="font-size:12px;color:#808080;">Статистика за период с <strong><?=str_replace('_','-',$start_date);?></strong> по <strong><?=str_replace('_','-',$date);?></strong></p>
      <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
        </tr>
      </thead>
      <tbody>
        <tr style="background-color: #ffffff;">
          <td><?=$all_dey_unique;?></td>
          <td><?=$all_dey_hits;?></td>
          <td><?=($all_dey_unique > 0 ? (number_format(($all_dey_unique / $all_dey_hits), 4, '.', '') * 100) : '0');?>%</td>
        </tr>
      </tbody>
      </table>
    	 
    	 <p style="font-size:12px;color:#808080;">Статистика за сегодня</p>
      <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
        </tr>
      </thead>
      <tbody>
        <tr style="background-color: #ffffff;">
          <td><?=$dey_unique[0];?></td>
          <td><?=$dey_hits[0];?></td>
          <td><?=($dey_unique[0] > 0 ? (number_format(($dey_unique[0] / $dey_hits[0]), 4, '.', '') * 100) : '0');?>%</td>
        </tr>
      </tbody>
      </table>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($agent_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Браузеры</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
          <th>Всего (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$agent_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Браузеры</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($os_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">ОС</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
          <th>Всего (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$os_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Операционки</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($breack_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Функция</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$breack_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Не принятый траффик</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<!--<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($domain_stat_array)) { ?>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Домен сервера ЕСД <a href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=domain&start_date=<?=$start_date;?>&date=<?=$date;?>" target="_parent"><span class="glyphicon glyphicon-share-alt"></span></a></th>
          <th>Уников</th>
          <th>Хитов</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$domain_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Домен сервера ЕСД</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>-->
      
    	<!--<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($target_stat_array)) { ?>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Лендинг (Куда отправился) <a href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=target&start_date=<?=$start_date;?>&date=<?=$date;?>" target="_parent"><span class="glyphicon glyphicon-share-alt"></span></a></th>
          <th>Уников</th>
          <th>Хитов</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$target_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Лендинг (Куда отправился)</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>-->
      
      </div>
      
      <div style="float:right;width:560px;">
      
    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($geo_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Страны</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
          <th>Всего (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$geo_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Страны</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($devices_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Устройства</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
          <th>Всего (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$devices_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Устройства</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($esdid_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th>ЕСД</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
          <th>Всего (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$esdid_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>ЕСД</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($check_stat_array)) { ?>
    <table class="table table-hover table-condensed">
      <thead>
        <tr>
          <th colspan="2">Функция</th>
          <th>Уников</th>
          <th>Хитов</th>
          <th>У/Х (%)</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$check_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Чекер</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>
    	
    	<!--<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($ip_stat_array)) { ?>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>IP адреса посетителей <a href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=ip&start_date=<?=$start_date;?>&date=<?=$date;?>" target="_parent"><span class="glyphicon glyphicon-share-alt"></span></a></th>
          <th>Уников</th>
          <th>Хитов</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$ip_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>IP адреса посетителей</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>-->
    	
    	<!--<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($referer_stat_array)) { ?>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Рефер (Откуда пришел) <a href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=referer&start_date=<?=$start_date;?>&date=<?=$date;?>" target="_parent"><span class="glyphicon glyphicon-share-alt"></span></a></th>
          <th>Уников</th>
          <th>Хитов</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$referer_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Рефер (Откуда пришел)</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>-->
    	
    	<!--<div class="col-md-6"><div class="well" style="padding:10px;">
    <?php if (!empty($cookie_stat_array)) { ?>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Параметры (Передаем к есд через cookie и get) <a href="<?=SITE_URL;?>/mshow.php?<?=(isset($_GET['datesearch']) ? 'datesearch=1&' : '');?>showpage=cookie&start_date=<?=$start_date;?>&date=<?=$date;?>" target="_parent"><span class="glyphicon glyphicon-share-alt"></span></a></th>
          <th>Уников</th>
          <th>Хитов</th>
        </tr>
      </thead>
      <tbody>
        <?=implode("\n",$cookie_stat_array);?>
      </tbody>
    </table>
    	<?php } else { echo '<strong>Параметры (Передается к есд через cookie и get)</strong>'; }?>
    	</div></div>
    	<div style="clear:both;"></div>-->
      
      </div>
      
      
    	</div>
    	
    	</div>