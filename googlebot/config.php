<?php

// Имя и версия скрипта
define('SCRIPT_NAME', 'TDS Pro');
define('SCRIPT_VERSION', '5.0');

// Ключ доступа к слугам скрипта, к примеру: FDSSFG54tg3DFSTyt45t
define('DOMAINS_KEY', 'rHL0du8N79Y7brj5zR2n5mJDcXKfxDK7');

// Ключ для приватных ссылок
define('PRIVATE_KEY', 'kf8FYoO9C9Pg6Nc2zHU4FrgXpAcBzoy7');

// Ключ для доступа к API ESD генератора
define('ESD_API_KEY', 'OB9ab1N91FRpJZpAo7Xt5gsJJro4hp9H');

// Админ доступ
define('PANEL_LOGIN', 'admin');
define('PANEL_PASSW', 'admin');

// Количество записей на главной
define('STAT_DATA_COUNT', '10');

// Количество записей на подробных страницах
define('STAT_ALLDATA_COUNT', '100');

// Имя файла страницы входа
define('ADMIN_FILE_NAME', 'admin.php');

// Доступ к MySQL
define('DB_HOST_SET', 'docker_db');
define('DB_USER_SET', 'db01');
define('DB_PASS_SET', 'Bz14KyLCGbxcc2aunPYb');
define('DB_NAME_SET', 'db01');
define('DB_PREFIX_SET', '');

// Зона времени, важно не менять после установки
//date_default_timezone_set("UTC");
date_default_timezone_set("EST5EDT"); // Domenic
//date_default_timezone_set("EST"); // Domenic
//date_default_timezone_set("Europe/Moscow"); // Tosser // Зона опережает на час
//date_default_timezone_set("Europe/Kaliningrad"); // Tosser

// Ячейки для кеширования
$get_array = array(
    	'os',
    	'agent',
    	'devices',
    	'geo',
    	'esdid',
    	'target',
    	'referer',
    	'domain',
    	'ip',
    	'cookie'
);

// Используем только цифровые ID и пишем в бд cookie то же только цифры
// Из параметра линка выбираем только цифры, остальное обрезаем
define('SELECT_ONLY_DIGIT_ID', true);

// Минимальное количество символов в cookie
define('SELECT_COUNTS_DIGIT_ID', '4');

// Не трогать те параметры в которых есть указанные слова
// Можно указать несколько, разделаяя с помощбю |
define('NO_SELECT_PARAM_DIGIT_ID', 'img1|img2|img3|img4|img5|img6|img7|img8');

// Включить режим дебага редиректа, рез. в файле logs/DEBUG.txt
define('REDIRECT_DEBUG', false);

// Очищать базы старше чем указано дней
define('CLERT_TABLR_DEY', '30');

// Локнутые агенты (полное совпадение)
$bot_agent = array(
	'Mozilla/5.0 (iPhone; CPU iPhone OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B411 Safari/600.1.4 (compatible; YandexMobileBot/3.0; +http://yandex.com/bots)',
	'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5 (Applebot/0.1; +http://www.apple.com/go/applebot)',
	'Mozilla/5.0 (Linux; U; Android 2.2; en-us;) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1',
	'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36',
	'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9b5) Gecko/2008032619 Firefox/3.0b5',
	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727)',
	'Mozilla/5.0 (compatible; BuzzSumo; +http://www.buzzsumo.com/bot.html)',
	'Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)',
	'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727)',
	'Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)',
	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)',
	'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)',
	'mozilla/4.0 (compatible; msie 6.0; windows nt 5.0)',
	'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)',
	'Google-HTTP-Java-Client/1.17.0-rc (gzip)',
	'Mozilla/3.0 (compatible; Indy Library)',
	'Mozilla/4.03 [en] (Win95; I ;Nav)',
	'Mozilla/5.0 (compatible; Kraken/0.1; http://linkfluence.net/; bot@linkfluence.net)',
	'DirBuster-0.12 (http://www.owasp.org/index.php/Category:OWASP_DirBuster_Project)',
	'facebookexternalhit/1.1 (+http://www.facebook.com/externalhit_uatext.php)',
	'LivelapBot/0.2 (http://site.livelap.com/crawler)',
	'grokkit-crawler (pdsupport@purediscovery.com)',
	'MetaURI API/2.0 +metauri.com',
	'facebookexternalhit/1.1',
	'Mozilla/5.0 Jorgee',
	'URLRedirect.pm',
	'Java/1.8.0_45',
	'Java/1.7.0_71',
	'PHP/5.4.36',
	'facebookexternalhit/1.1;kakaotalk-scrap/1.0;',
	'GG PeekBot 2.0 ( http://gg.pl/ http://info.gadu-gadu.pl/praca )',
	'msnbot-Products/1.0 (+http://search.msn.com/msnbot.htm)',
	'Googlebot/2.1 ( http://www.googlebot.com/bot.html)',
	'Apache-HttpClient/4.5.1 (Java/1.8.0_45)',
	'Ruby',
	'Curebot/1.0',
	'SeitTestBot',
	'Mozilla/5.0 (Windows; U; Windows NT 5.1; en; rv:1.9.0.13) Gecko/2009073022 Firefox/3.5.2 (.NET CLR 3.5.30729) SurveyBot/2.3 (DomainTools)',
	'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36 TwengaBot',
	'Twiends Crawler 1.1 created by twiends.com. See http://twiends.com/msg/crawler for more information.',
	'Uzbl (Webkit 1.8) (Linux x86_64 [x86_64])',
	'finbot',
	'Mozilla/5.0 (compatible; spbot/5.0.1; +http://OpenLinkProfiler.org/bot )',
	'Mozilla/5.0 (compatible; DotBot/1.1; http://www.opensiteexplorer.org/dotbot, help@moz.com)',
	'Mozilla/5.0 (compatible; Konqueror/3.5; Linux) KHTML/3.5.5 (like Gecko) (Exabot-Thumbnails)',
	'Mozilla/5.0 (Windows NT 6.1; Win64; x64) KomodiaBot/1.0',
	'mfibot/1.1 (http://www.mfisoft.ru/analyst/; <admin@mfisoft.ru>; en-RU)',
	'Mozilla/5.0 (compatible; NewShareCounts.com/1.0; +http://newsharecounts.com/crawler)',
	'LinkedInBot/1.0 (compatible; Mozilla/5.0; Jakarta Commons-HttpClient/3.1 +http://www.linkedin.com)',
	'python-requests/2.9.1',
	'python-requests/2.2.1 CPython/2.7.6 Linux/3.13.0-83-generic',
	'python-requests/2.7.0 CPython/3.4.3 Linux/3.14.32-xxxx-grs-ipv6-64',
	'python-requests/2.7.0 CPython/2.7.10 Linux/3.13.0-36-generic',
	'python-requests/2.2.1 CPython/2.7.6 Linux/3.13.0-77-generic',
	'Mozilla/5.0 (TweetmemeBot/4.0; +http://datasift.com/bot.html) Gecko/20100101 Firefox/31.0',
	'Java/1.6.0_22',
	'Java/1.7.0_65',
	'Java/1.4.1_04',
	'Java/1.6.0_04',
	'Java/1.7.0_79',
	'Python-urllib/1.17',
	'Python-urllib/2.7',
	'Python-urllib/2.5',
	'Python-urllib/3.4',
	'Mozilla/5.0 (compatible; oBot/2.3.1; +http://filterdb.iss.net/crawler/)',
	'Mozilla/5.0 (compatible; PaperLiBot/2.1; http://support.paper.li/entries/20023257-what-is-paper-li)',
	'Apache-HttpClient/UNAVAILABLE (java 1.4)',
	'Apache-HttpClient/UNAVAILABLE (Java/1.8.0_45)',
	'Apache-HttpClient/4.3.5 (java 1.5)',
	'\'Mozilla/4.0\'',
	'1',
	'HTTP-Tiny/0.054',
	'Apache-HttpClient/4.5.1 (Java/1.8.0_92)',
	'Microsoft-WebDAV-MiniRedir/6.1.7601',
	'Wget/1.16.1 (linux-gnu)',
	'HTTP-Tiny/0.049',
	'HTTP-Tiny/0.051',
	'curl/7.22.0 (x86_64-pc-linux-gnu) libcurl/7.22.0 OpenSSL/1.0.1 zlib/1.2.3.4 libidn/1.23 librtmp/2.3',
	'curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.13.1.0 zlib/1.2.3 libidn/1.18 libssh2/1.2.2',
	'curl/7.43.0',
	'curl/7.21.0 (i486-pc-linux-gnu) libcurl/7.21.0 OpenSSL/0.9.8o zlib/1.2.3.4 libidn/1.15 libssh2/1.2.6',
	'SynHttpClient-Built/5819',
	'WebClient/1.0',
	'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.16) Gecko/20080702 Firefox/2.0.0.16',
	'Python-urllib/2.6',
	'?Mozilla/4.0?',
	'Mozilla/5.0 (compatible; seo-audit-check-bot/1.0)',
	'Mozilla/4.0 (compatible;)',
	'WHR'
);

# Banned Bot UserAgent
$http_user_agent = "Unknown|PhantomJS|http|masscan|Puffin|CFNetwork|WebDAV|iOpus|CUAMatch|scanner|Scanner|Susie|A6-Indexer|ASProxy|Abonti|Aboundex|Accoona|Ack|Action Comm User|Alexa|AnyEvent-HTTP|Apercite|AportWorm|AppEngine-Google|Arachnophilia|Ask|AutoPager|BIDUBrowser|BOT|BUbiNG|Bad-Neighborhood|Bada|BegunAdvertising|Bender|Bitacle|BlogSwarm|Bloglines|Bot|Butterfly|CJNetworkQuality|CMNTDF|CPython|CRAWLER|Charlotte|CheckSite|Checker|Chilkat|Chimu|CitiStreet|CloudFlare|CollapsarWEB|Confluence|Contiki|CoralWebPrx/|Costinesti|CrawlFire|Crawler|Crowsnest|Curl|DAUMOA|DFTBA|Dalvik|DataFountains|DataparkSearch|Dataprovider|Daumoa|Detector|DeuSu|DevRuter|DirBuster|Django|DoCoMo|DomainAppender|DomainMonitor|Dossier|Download-Tipp|Drupal|DuckDuckPreview|EARTHCOM|Easy-Thumb|EasyBib|Editor|EmbeddedWB|Embedly|EmeraldShield|Exploratodo|FairShare|Faveeo|Feed Parser|FeedBucket|FeedBurner|Feedfetcher-Google|FeedlyApp|Feedspot|Felix|Filangy|Filecrop|Firebat|FlipboardBrowserProxy|FlipboardProxy|Francis|FreeWebMonitoring|Froute|GC3pro+dir+CLD|GTB7|Gecko Miro|Generator|Genieo|GentleSource|Globel|Godzilla|Google AppsViewer|Google Desktop|Google-Structured-Data-Testing-Tool|GoogleDocs|GoogleProducer|Grammarly|Greetings|GroupHigh|HTTPRequest|Hatena|HeartRails|Hermit Search|HostTracker|HttpConduitDownloader|HubPages|HubSpot|ICI Argentina|IDwhois|INFOMINE|IconSurf|IlTrovatore-Setaccio|InAGist|InboundScore|Infohelfer|Instapaper|JS-Kit|JetBrains|JoBo|Jobboerse|JyxoToolbar|K-Meleon|Kevin|Kml-Google|Kohana|Kurwoskrypt|LAIESKEN|LSSRocketCrawler|LYCOSA|LYT.SR|LibertyW|Liferea|LinkChecker|LinkMa|LinkMan|LinkStash|LinkWalke|Linkee|Links|Lipperhey|LoadImpactPageAnalyzer|LoadImpactRload|Lovel|MAXTHON|MC6800's|MRSPUTNIK|MS-RTC LM|MXT/Nutch|MagpieRSS|Mammoth|Maxthon|McAfee|Mechanize|MegaIndex|Meneame|MergeFlow|MessengerShare|MobileSurf|Moreover|Mozilla/4.6|Mozilla/4.7|Mozilla/4.72|Munozilla/5.0|NetLyzer|NetResearchServer|NetcraftSurveyAgent|Netvibes|NewsRob|Nintendo|Nmap Scripting|OfficeLiveConnector|OnetSzukaj|OppO|OptimizedIE8|Owlin Feedfinder|PROJECT HI!|Page2RSS|PagePeeker|PagesInventory|PathDefender|Pattern|PhantomJS|PhoneFavs|Phonifier|Pinterest|Playstation|Plukkie|Pr-Navi-Innovation|Prlog|Python-urllib|QQDownload|Qirina Hurdler|Qwantify|R&D project|RSS API|RadioClicker|Readability|Reaper|RebelMouse|RedirectChecker|RevIP|ReverseGet|Riddler|RoboCrawl|RockmeltEmbedService|RockmeltEmbedder|SBIder|SBSearch|SEO-Visuals|SEOSoft|SJN . Die|SafariBookmarkChecker|Scrapy|Screenshoot|Screenshot|ScribdReader|Scrubby|SeaMonkey|Semantics|Sens.a|Seznam|SimpleBrowser|Site-Shot|Site-Visualizer|SiteCheck|SiteExplorer|SixXS|Slurp|SlySearch|Snacktory|Sonic|Sopel|Source Viewer|Spider|Spinn3r|Spyder|SquallNN|Sscreenshot-generator|Steeler|StudioFACA|Summify|Talkro Web-Shot|TargetSeek|Tesseract|Test.Buzzz|Testing|Textual|ThumbSniper|Thumbshot|Timewe|TinEye|TipTop|TopHatenar|Topodia|Trove|TulipChain|Twiceler|Twikle|Tycoon|Typhoeus|UniversalFeedParser|Unknown|UnwindFetchor|Vagabondo|Validator|Visited|W3C|W3C-mobile|WPDASH-UpChronic|WSE|Wazzup|WbSrch|WeSEE|Web-sniffer|WebCookies|WebMasterAid|WebSearch|WebThumbnail|Webauskunft|WebmasterCoffee|Webscout|WeeChat|Wget|Whois365|WikiCrawl|Willie|Win 9x 4.90|Windows Services|WordPress|X-CAD-SE|X6-00.1|XBMC|XMB-eXtreme-Message-Board|Xbox|YOURLS|Yahoo|Yandex|Yeti|Zao|ZipCommander|ZyBorg|abby|aff-kingsoft-ciba|alexa|android-async-http|annotategoogl|archiver|betasearch|boitho|bot|complexnetworkgroup|cosmos|crawler|curl|dpdev|egothor|endo/1.0|facebook|facebookexternalhit|findlinks|gonzo2P|gooblog|gromoteur|grub-client|heritrix|http://|iThemes|ichiro|indonesiancoder|iskanie|kulturarw3|ltx71|masscan|mfc2|mobilizer|monitoring service|mowser|mozilla/1.0|page scorer|pageverifier|parsijoo|proximic|qingdao|sc-downloader|scan4mail|specialarchiver|sqlmap|vBSEO|viettel|vkShare|webnumbrFetcher|wget|wmtips|wscheck|wsrising|xpcomviewer|xxxxxxxx|bitlybot";

# Banned Unix UserAgent
$unix_user_agent = "Ubuntu|ubuntu|OpenBSD|FreeBSD|NetBSD|Linux x86_64";

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////// Ниже желательно не трогать
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////// Тут желательно не трогать
////////////////////////////////////////////////////////////////////////////////////////////

// Включить режим отладки
define('DEV_MODE', true);
define('DEV_MODE_LOGING', false);

// Константы
define('ROOT', rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/');
define('LOGS', ROOT. 'logs/');
define('LIBS', ROOT. 'lib/');

// Определяем полный урл к скрипту
foreach (explode('/',str_replace('\\', '/', dirname(__FILE__))) as $_v) { $_dir = $_v; }
define('SITE_URL', 'http://'.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : gethostbyname(gethostname())).'/'.str_replace('/', '', $_dir));
define('SITE_URL_SSL', 'https://'.(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : gethostbyname(gethostname())).'/'.str_replace('/', '', $_dir));

// Определяем директорию скрипта
define('SITE_DIR', str_replace('/', '', $_dir));

/*
if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {
header('Location: '.SITE_URL.'/'); die();
}
*/

if (DEV_MODE) {
if (DEV_MODE_LOGING) {
function err_handler($errno, $errmsg, $filename, $linenum) {
    if (!in_array($errno, Array(E_STRICT,E_DEPRECATED,E_USER_DEPRECATED))) {
        $date = date('Y-m-d H:i:s (T)');
        $f = fopen(LOGS.'errors.log', 'a+');
        if (!empty($f)) {
            $err  = "<error>";
            $err .= "<date>$date</date> ";
            $err .= "<errno>$errno</errno> ";
            $err .= "<errmsg>$errmsg</errmsg> ";
            $err .= "<filename>$filename</filename> ";
            $err .= "<linenum>$linenum</linenum>";
            $err .= "</error>\n";
            flock($f, LOCK_EX);
            fwrite($f, $err);
            flock($f, LOCK_UN);
            fclose($f);
        }
    }
}
set_error_handler("err_handler");
}
else {
error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('ignore_repeated_errors', false);
ini_set('error_reporting', E_ALL);
if(version_compare(PHP_VERSION, '5.3.0', '>=')) {
error_reporting(E_ALL ^ (E_DEPRECATED|E_USER_DEPRECATED));
ini_set('error_reporting', E_ALL ^ (E_DEPRECATED|E_USER_DEPRECATED));
}
}
} else { error_reporting(0); ini_set('display_errors', false); }

set_time_limit(0);
ini_set('max_execution_time', 0);
ini_set('upload_max_filesize',"528M");
ini_set('post_max_size',"1024M");
ini_set('memory_limit',"1024M");
ini_set('magic_quotes_runtime', false);
ini_set('magic_quotes_gpc', false);
ini_set("pcre.recursion_limit",700000);
ini_set("pcre.backtrack_limit",7000000);
//ignore_user_abort(true);

//session_start();

////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////// Тут желательно не трогать
////////////////////////////////////////////////////////////////////////////////////////////
