<?php
session_start();

require(rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/config.php');

// LOGIN 1
if ((isset($_SESSION['PANEL_LOGIN']) && !empty($_SESSION['PANEL_LOGIN']) && $_SESSION['PANEL_LOGIN'] == PANEL_LOGIN) 
&& (isset($_SESSION['PANEL_PASSW']) && !empty($_SESSION['PANEL_PASSW']) && $_SESSION['PANEL_PASSW'] == PANEL_PASSW)) {

include(LIBS.'functions.php');
include(LIBS.'database.php');

$db = new database();
$db->connect();

/*
die;
*/

$results = '';

if (isset($_POST['update']) && $_POST['update'] == 'ok') {

define('DEBUG', false);

define('TIMEOUT', 20);

define('MD5_URL', 'http://user-agent-string.info/rpc/get_data.php?format=ini&md5=y');

define('VER_URL', 'http://user-agent-string.info/rpc/get_data.php?key=free&format=ini&ver=y');

define('INI_URL', 'http://user-agent-string.info/rpc/get_data.php?key=free&format=ini');

function writeCacheIni($ver, $status)
{
        // Build a new cache file and store it
        $cacheIni = "; cache info for class UASparser - http://user-agent-string.info/download/UASparser\n";
        $cacheIni .= "[main]\n";
        $cacheIni .= "localversion = \"$ver\"\n";
        $cacheIni .= 'lastupdate = "' . time() . "\"\n";
        $cacheIni .= "lastupdatestatus = \"$status\"\n";
        $written = @file_put_contents(LOGS . 'uascache/cache.ini', $cacheIni, LOCK_EX);
        if ($written === false) {
            debug('Failed to write cache file to ' . LOGS . 'uascache/cache.ini');
            return false;
        }
        return true;
}

function debug($msg)
{
        if (DEBUG) {
            echo gmdate('Y-m-d H:i:s') . "\t$msg\n";
        }
}

function getContents($url, $timeout = 300)
{
        $data = '';
        $starttime = microtime(true);
        // use fopen
        if (ini_get('allow_url_fopen')) {
            $fp = @fopen(
                $url,
                'rb',
                false,
                stream_context_create(
                    array(
                        'http' => array(
                            'timeout' => $timeout,
                            'header' => "Accept-Encoding: gzip\r\n"
                        )
                    )
                )
            );
            if (is_resource($fp)) {
                $data = stream_get_contents($fp);
                $res = stream_get_meta_data($fp);
                if (array_key_exists('wrapper_data', $res)) {
                    foreach ($res['wrapper_data'] as $d) {
                        if ($d == 'Content-Encoding: gzip') { //Data was compressed
                            $data = gzinflate(substr($data, 10, -8)); //Uncompress data
                            debug('Successfully uncompressed data');
                            break;
                        }
                    }
                }
                fclose($fp);
                if (empty($data)) {
                    if (DEBUG) {
                        if ($res['timed_out']) {
                            debug('Fetching URL failed due to timeout: ' . $url);
                        } else {
                            debug('Fetching URL failed: ' . $url);
                        }
                    }
                    $data = '';
                } else {
                    debug(
                        'Fetching URL with fopen succeeded: ' . $url . '. ' . strlen($data) . ' bytes in ' . (microtime(
                                true
                            ) - $starttime) . ' sec.'
                    );
                }
            } else {
                debug('Opening URL failed: ' . $url);
            }
        } elseif (function_exists('curl_init')) {
            // Fall back to curl
            $ch = curl_init($url);
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_TIMEOUT => $timeout,
                    CURLOPT_CONNECTTIMEOUT => $timeout,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => 'gzip'
                )
            );
            $data = curl_exec($ch);
            if ($data !== false and curl_errno($ch) == 0) {
                debug(
                    'Fetching URL with curl succeeded: ' . $url . '. ' . strlen($data) . ' bytes in ' . (microtime(
                            true
                        ) - $starttime) . ' sec.'
                );
            } else {
                debug('Opening URL with curl failed: ' . $url . ' ' . curl_error($ch));
                $data = '';
            }
            curl_close($ch);
        } else {
            trigger_error('Could not fetch UAS data; neither fopen nor curl are available.', E_USER_ERROR);
        }
        return $data;
}

function downloadData($force = false)
{
        // by default status is failed
        $status = false;
        // support for one of curl or fopen wrappers is needed
        if (!ini_get('allow_url_fopen') && !function_exists('curl_init')) {
            debug('Fopen wrappers and curl unavailable, cannot continue');
            trigger_error(
                'ERROR: function file_get_contents not allowed URL open. Update the datafile (uasdata.ini in Cache Dir) manually.'
            );
            return $status;
        }

        $cacheIni = array();
        if (file_exists(LOGS . 'uascache/cache.ini')) {
            $cacheIni = parse_ini_file(LOGS . 'uascache/cache.ini');
        }
        

        // Check the version on the server
        // If we are current, don't download again
        $ver = getContents(VER_URL, TIMEOUT);
        if (preg_match('/^[0-9]{8}-[0-9]{2}$/', $ver)) { //Should be a date and version string like '20130529-01'
            if (array_key_exists('localversion', $cacheIni)) {
                if ($ver <= $cacheIni['localversion']) { //Version on server is same as or older than what we already have
                    if ($force) {
                        debug('Existing file is current, but forcing a download anyway.');
                    } else {
                        debug('Download skipped, existing file is current.');
                        $status = true;
                        writeCacheIni($ver, $status);
                        return $status;
                    }
                }
            }
        } else {
            debug('Version string format mismatch.');
            $ver = 'none'; //Server gave us something unexpected
        }

        // Download the ini file
        $ini = getContents(INI_URL, TIMEOUT);
        echo $ini;
        if (!empty($ini)) {
            // Download the hash file
            $md5hash = getContents(MD5_URL, TIMEOUT);
            if (!empty($md5hash)) {
                // Validate the hash, if okay store the new ini file
                if (md5($ini) == $md5hash) {
                    $written = @file_put_contents(LOGS . 'uascache/uasdata.ini', $ini, LOCK_EX);
                    if ($written === false) {
                        debug('Failed to write data file to ' . LOGS . 'uascache/uasdata.ini');
                    } else {
                        $status = true;
                    }
                } else {
                    debug('Data file hash mismatch.');
                }
            } else {
                debug('Failed to fetch hash file.');
            }
        } else {
            debug('Failed to fetch data file.');
        }
        writeCacheIni($ver, $status);
        return $status; //Return true on success
}

if (downloadData()) {
$results = '<p>Успешно обновлено!</p>';
}
else { $results = '<p>Ошибка!!!</p>'; }

}

function db_size($name='')
{
    if($name == '') $res = mysql_query("SHOW TABLE STATUS");
    else $res = mysql_query("SHOW TABLE STATUS FROM ".$name."");
    $size = 0;
    while($row = mysql_fetch_array($res))
    {
        $size += $row["Data_length"] + $row["Index_length"];
    }
    if( $size < 1024 ) return $size . " B";
    else if( $size < 1048576 ) return round(($size / 1024), 1) . " Kb";
    else return round(($size / 1048576), 1) . " Mb";
}

// Тут и так понятно думаю
$date = (isset($_GET['date']) ? str_replace('-','_',$_GET['date']) : ''); if (empty($date)) { $date=date("j_n_Y"); }
$datesearch = (isset($_GET['datesearch']) ? $_GET['datesearch'] : '');
$start_date = date("j_n_Y",strtotime("last Year"));
$today = date("j_n_Y",strtotime("-".CLERT_TABLR_DEY." day"));

// Выводим именя всех таблиц начинающийся с traffic_
$table_list = array();
$res_list = mysql_query("SHOW TABLES");
if(mysql_num_rows($res_list)) {
while($result_list = mysql_fetch_array($res_list)) {
if (preg_match('/'.DB_PREFIX_SET.'traffic_(\d+)_(\d+)_(\d\d\d\d)/i',$result_list[0])) {
$table_list[] = trim($result_list[0]);
}}} mysql_free_result($res_list);

// Выводим список дат по введенному периоду
$date_array = getInterval($start_date, $today, 'j_n_Y');

// Выводим имена таблиц которые совпали в указанном периоде
$table_name_array = array();
foreach ($table_list as $table_name) { if (in_array($table_name, $date_array)) {
$_handle_ = mysql_query("select count(1) from `".DB_PREFIX_SET."".$table_name."` WHERE `timestamp`<>'0'");
$_counts_ = mysql_fetch_array($_handle_); mysql_free_result($_handle_);
if ($_counts_[0] > 0) { $table_name_array[] = $table_name; }
}}

$dbsize = db_size();

if (isset($_POST['cleardb']) && $_POST['cleardb'] == 'ok') {
foreach ($table_name_array as $tablenamearray) { mysql_query("TRUNCATE TABLE `".DB_PREFIX_SET."".$tablenamearray."`"); }
header("Location: ".SITE_URL."/services.php"); die();
}

function cash_size($cash_files=array())
{
    $size = 0;
    foreach ($cash_files as $cashfiles)
    {
        $size += filesize($cashfiles);
    }
    
    if( $size < 1024 ) return $size . " B";
    else if( $size < 1048576 ) return round(($size / 1024), 1) . " Kb";
    else return round(($size / 1048576), 1) . " Mb";
}

$cash_files = glob(LOGS."cashe/traffic_*");

$cashsize = cash_size($cash_files);

$check_cash_files = glob(LOGS."check_cashe/check_*");

$check_cashsize = cash_size($check_cash_files);

$break_cash_files = glob(LOGS."break_cashe/traffic_*");

$break_cashsize = cash_size($break_cash_files);

$bots_cash_files = glob(LOGS."bots_cashe/traffic_*");

$bots_cashsize = cash_size($bots_cash_files);

$accept_cash_files = glob(LOGS."accept_cashe/traffic_*");

$accept_cashsize = cash_size($accept_cash_files);

$unknow_cash_files = glob(LOGS."unknow_traffic/traffic_*");

$unknow_cashsize = cash_size($unknow_cash_files);

if (isset($_POST['archive']) && $_POST['archive'] == 'ok') {

include(LIBS.'tar.php');

if (file_exists(ROOT.'dump/dump.tar')) { unlink(ROOT.'dump/dump.tar'); }
if (file_exists(ROOT.'dump/dump.tar.gz')) { unlink(ROOT.'dump/dump.tar.gz'); }

$my_tar = new tar;
$my_tar->patchname = ROOT;
$my_tar->tarmode = "tar"; // Закомментим строку если надо архивировать
$my_tar->tarname = 'dump/dump.'.$my_tar->tarmode;
$my_tar->tarArch(''); // путь к директории для архивации, например logs/cashe (без слешов в начале и конце)
//echo $my_tar->error.'<br />';
$my_tar->tarClose();

header("Location: ".SITE_URL."/services.php");
die();
}
else {
if (file_exists(ROOT.'dump/dump.tar')) {
$size = filesize(ROOT.'dump/dump.tar');
if($size < 1024) { $fsize = $size . " B"; }
else if($size < 1048576) { $fsize = round(($size / 1024), 1) . " Kb"; }
else { $fsize = round(($size / 1048576), 1) . " Mb"; }
$file_download = 'Файл <b>dump.tar</b> размером <b>'.$fsize.'</b> (<a target="_blank" href="'.SITE_URL.'/dump/dump.tar">скачать</a>)';
}
elseif (file_exists(ROOT.'dump/dump.tar.gz')) {
$size = filesize(ROOT.'dump/dump.tar.gz');
if($size < 1024) { $fsize = $size . " B"; }
else if($size < 1048576) { $fsize = round(($size / 1024), 1) . " Kb"; }
else { $fsize = round(($size / 1048576), 1) . " Mb"; }
$file_download = 'Файл <b>dump.tar.gz</b> размером <b>'.$fsize.'</b> (<a target="_blank" href="'.SITE_URL.'/dump/dump.tar.gz">скачать</a>)';
}
else { $file_download = 'Дамп отсутствует, создайте!!!'; }
}

$id_temp_handle = mysql_query("select count(1) from `".DB_PREFIX_SET."id_temp` WHERE `cookie` != '' order by cookie");
$idtemp = mysql_fetch_array($id_temp_handle); mysql_free_result($id_temp_handle);
if ($idtemp[0] > 0) { $id_temp = $idtemp[0]; } else { $id_temp = '0'; }

$id_results_handle = mysql_query("select count(1) from `".DB_PREFIX_SET."id_results` WHERE `cookie_id` != '' order by cookie_id");
$idresults = mysql_fetch_array($id_results_handle); mysql_free_result($id_results_handle);
if ($idresults[0] > 0) { $id_results = $idresults[0]; } else { $id_results = '0'; }

$id_temp_dbsize = db_size("`".DB_NAME_SET."` WHERE Name = '".DB_PREFIX_SET."id_temp'");
$id_results_dbsize = db_size("`".DB_NAME_SET."` WHERE Name = '".DB_PREFIX_SET."id_results'");

function get_save_history_time()
{
        $sec = (6*60*60 - (time() - @filemtime(LOGS."collect_results_interval.txt")));
        return $sec." сек. = ".round($sec / 60)." мин. = ".round($sec / 60 / 60)." час.";
}

// Получаем имя открытого файла
$explpname = explode("/", $_SERVER['SCRIPT_NAME']);
foreach ($explpname as $i => $value) {
$file_name=$explpname[$i];
}

include(ROOT.'theme/header.php');
include(ROOT.'theme/services.php');
include(ROOT.'theme/footer.php');

unset($table_name_array);
unset($date_array);
unset($table_list);

$db->close();

// LOGIN 2
} else {

if ((isset($_POST['login']) && !empty($_POST['login']) && $_POST['login'] == PANEL_LOGIN) 
&& (isset($_POST['pass']) && !empty($_POST['pass']) && $_POST['pass'] == PANEL_PASSW)) {
$_SESSION['PANEL_LOGIN'] = PANEL_LOGIN;
$_SESSION['PANEL_PASSW'] = PANEL_PASSW;
echo '<meta http-equiv="refresh" content="0">';
die();
}

include(ROOT.'theme/login.php');
}
?>