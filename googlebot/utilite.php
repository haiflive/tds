<?php
session_start();

require(rtrim(str_replace('\\', '/', dirname(__FILE__)), '/'). '/config.php');

require_once '../vendor/autoload.php';
use GeoIp2\Database\Reader;

$reader = new Reader(LIBS.'geoip2/GeoIP2-ISP.mmdb');

// LOGIN 1
if ((isset($_SESSION['PANEL_LOGIN']) && !empty($_SESSION['PANEL_LOGIN']) && $_SESSION['PANEL_LOGIN'] == PANEL_LOGIN) 
&& (isset($_SESSION['PANEL_PASSW']) && !empty($_SESSION['PANEL_PASSW']) && $_SESSION['PANEL_PASSW'] == PANEL_PASSW)) {

include(LIBS.'functions.php');
include(LIBS.'database.php');

$db = new database();
$db->connect();

// Тут и так понятно думаю
$date = (isset($_GET['date']) ? str_replace('-','_',$_GET['date']) : ''); if (empty($date)) { $date=date("j_n_Y"); }
$start_date = (isset($_GET['start_date']) ? str_replace('-','_',$_GET['start_date']) : '');
$datesearch = (isset($_GET['datesearch']) ? $_GET['datesearch'] : '');

// Расшипляем дату старта и конечную
$explp = explode("_", $date);
$day = $explp[0];
$month = $explp[1];
$year = $explp[2];

if (!empty($start_date)) { 
$start_explp = explode("_", $start_date);
$start_day = $start_explp[0];
$start_month = $start_explp[1];
$start_year = $start_explp[2];
}
else {
$start_date='1_'.$month.'_'.$year;
}

// какой то непонятный код из старого скрипта
$redate=mktime(0,0,0,$month,$day,$year); $redate=strtotime('-1 day', $redate);
$prevday_day=date('j',$redate); $prevday_month=date('n',$redate); $prevday_year=date('Y',$redate);
$redate=mktime(0,0,0,$month,$day,$year); $redate=strtotime('+1 day', $redate);
$futday_day=date('j',$redate); $futday_month=date('n',$redate); $futday_year=date('Y',$redate);
$redate=mktime(0,0,0,$month,$day,$year); $redate=strtotime('-1 month', $redate);
$prevmonth_day=date('j',$redate); $prevmonth_month=date('n',$redate); $prevmonth_year=date('Y',$redate);
$redate=mktime(0,0,0,$month,$day,$year); $redate=strtotime('+1 month', $redate);
$futmonth_day=date('j',$redate); $futmonth_month=date('n',$redate); $futmonth_year=date('Y',$redate);

// Выводим именя всех таблиц начинающийся с traffic_
// Выводим список дат по введенному периоду
$date_array = getInterval($start_date, $date, 'j_n_Y');

if (isset($_REQUEST['get_mx_isp']) && $_REQUEST['get_mx_isp'] == '1') {

if (!empty($_REQUEST['domain'])) {

$domain_array = array();

foreach (explode("\n",trim($_REQUEST['domain'])) as $_domain) {

if (empty($_domain) || trim($_domain) == "") { continue; }

$_domain_exp = explode("\t",trim($_domain));

if (!empty($_domain_exp[1])) {
	
	getmxrr(trim($_domain_exp[1]),$mx_domain);
	
	if (empty($mx_domain[0])) { $mx_domain[0] = "---"; }
	
	$domain_array[] = trim($_domain_exp[0])."\t".$mx_domain[0].'|'.trim($_domain_exp[1]);
}
else {
	
	getmxrr(trim($_domain_exp[0]),$mx_domain);
	
	if (empty($mx_domain[0])) { $mx_domain[0] = "---"; }
	
	$domain_array[] = $mx_domain[0].'|'.trim($_domain_exp[0]);
}
}
natrsort($domain_array);
}

elseif (!empty($_REQUEST['smtp'])) {

$smtp_array = array();

foreach (explode("\n",trim($_REQUEST['smtp'])) as $_smtp) {

if (empty($_smtp) || trim($_smtp) == "") { continue; }

$_smtp_exp = explode("\t",trim($_smtp));

if (!empty($_smtp_exp[1])) {
	
	$_ip = gethostbyname(trim($_smtp_exp[1]));
	
	if ($_ip == trim($_smtp_exp[1])) { $_isp = "---"; }
	
	else {
	$geo = gip(trim($_ip));
	$record = $reader->isp(trim($_ip));
	$_isp = $record->isp.'|'.$record->organization.'|'.$geo.'|'.trim($_ip).'|'.trim($_smtp_exp[1]);
	}
	
	$smtp_array[] = trim($_smtp_exp[0])."\t".$_isp;
}
else {
	
	$_ip = gethostbyname(trim($_smtp_exp[0]));
	
	if ($_ip == trim($_smtp_exp[0])) { $_isp = "---"; }
	
	else {
	$geo = gip(trim($_ip));
	$record = $reader->isp(trim($_ip));
	$_isp = $record->isp.'|'.$record->organization.'|'.$geo.'|'.trim($_ip).'|'.trim($_smtp_exp[0]);
	}
	
	$smtp_array[] = $_isp;
}
}
natrsort($smtp_array);
}
}

// Получаем имя открытого файла
$explpname = explode("/", $_SERVER['SCRIPT_NAME']);
foreach ($explpname as $i => $value) {
$file_name=$explpname[$i];
}

include(ROOT.'theme/header.php');
include(ROOT.'theme/utilite.php');
include(ROOT.'theme/footer.php');

unset($date_array);

$db->close();

// LOGIN 2
} else {

if ((isset($_POST['login']) && !empty($_POST['login']) && $_POST['login'] == PANEL_LOGIN) 
&& (isset($_POST['pass']) && !empty($_POST['pass']) && $_POST['pass'] == PANEL_PASSW)) {
$_SESSION['PANEL_LOGIN'] = PANEL_LOGIN;
$_SESSION['PANEL_PASSW'] = PANEL_PASSW;
echo '<meta http-equiv="refresh" content="0">';
die();
}

include(ROOT.'theme/login.php');
}
?>